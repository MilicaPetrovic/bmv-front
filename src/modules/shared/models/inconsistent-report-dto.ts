export class InconsistentReportDto {
  driverId: number;
  drivingId: number;
  registeredUserId: number;
  registeredUsername: string;

  constructor(driverId: number, drivingId: number, registeredUserId: number, registeredUsername: string) {
    this.driverId = driverId;
    this.drivingId = drivingId;
    this.registeredUserId = registeredUserId;
    this.registeredUsername = registeredUsername;
  }
}
