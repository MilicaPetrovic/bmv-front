export class MapPoint {
  name = 'Novi Sad'
  lat = 45.24545
  lng = 19.83223

  constructor (n: string, lat: number, lon: number) {
    this.name = n
    this.lat = lat
    this.lng = lon
  }
}
