export interface statusDTO {
  successful: boolean
  exceptionMessage: string
  exceptionCode: number
}
