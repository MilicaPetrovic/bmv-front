export interface PaginationDTO<T> {
  totalCount: number
  resultList: T[]
}
