export interface DriverRegisterRequest {
    username: string | null
    name: string | null
    surname: string | null
    email: string | null
    password: string | null
    phoneNumber: string | null
    city: string | null
    postNumber: string | null

    vehicleName: string | null
    vehicleType: string | null
    color: string | null
    label: string | null
    petFriendly: boolean | null
    babyFriendly: boolean | null
    yearOfProduction: number | null
    numberOfSeats: number | null
    price: number | null
  }