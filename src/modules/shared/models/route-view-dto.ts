import { MapPoint } from './map-point'

export class RouteViewDto {
  coordinates: L.LatLng[]
  stations: MapPoint[]

  totalTime: number;
  totalDistance : number;
  price : number;

  constructor (coords: L.LatLng[], stations: MapPoint[], totalTime:number, totalDistance:number, price:number) {
    this.coordinates = coords
    this.stations = stations
    this.totalTime = totalTime
    this.totalDistance = totalDistance
    this.price = price
  }
}
