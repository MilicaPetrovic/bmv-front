export class StatusResponseDto {
  successful: boolean
  exceptionMessage: string
  exceptionCode: number

  constructor (successful: boolean, exceptionMessage: string, exceptionCode: number) {
    this.successful = successful
    this.exceptionMessage = exceptionMessage
    this.exceptionCode = exceptionCode
  }
}
