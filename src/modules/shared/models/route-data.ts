import { DEFAULT_PRICE_PER_KM } from '../../map/app.constants'
import { AlternativeRoute } from '../../map/models/alternative-route'

export class RouteData {
  coordinates: L.LatLng[]
  totalTime: number
  totalDistance: number
  price = 0
  color = ''

  public constructor (coords: L.LatLng[], time: number, distance: number)
  public constructor (alternativeRoutes: AlternativeRoute[])
  public constructor ()

  public constructor (...myarray: any[]) {
    if (myarray.length === 3) {
      this.coordinates = myarray[0]
      this.totalTime = myarray[1]
      this.totalDistance = myarray[2]
      this.price = Number((this.totalDistance / 1000 * DEFAULT_PRICE_PER_KM).toFixed(2))
      return
    }
    if (myarray.length === 1) {
      const alternativeRoutes = myarray[0]
      let calculateTotalDistance = 0
      let calculateTotalTime = 0
      let sumCoordinates: L.LatLng[] = []
      for (const a of alternativeRoutes) {
        console.log('current coorrdinates')
        console.log(a.route.coordinates)
        calculateTotalDistance += a.route.totalDistance
        calculateTotalTime += a.route.totalTime
        if (sumCoordinates.length === 0) {
          sumCoordinates = a.route.coordinates
        } else {
           sumCoordinates.push(...a.route.coordinates);
        }
      }
      console.log('sum coordinates')
      console.log(sumCoordinates)
      console.log('sum time', calculateTotalTime)
      console.log('sum distance', calculateTotalDistance)

      this.coordinates = sumCoordinates
      this.totalTime = calculateTotalTime
      this.totalDistance = calculateTotalDistance
      this.price = Number((this.totalDistance / 1000 * DEFAULT_PRICE_PER_KM).toFixed(2))
      return
    }
    if (myarray.length === 0) {
      this.coordinates = []
      this.totalTime = 0
      this.totalDistance = 0
      this.price = 0
      this.color = ''
    }
  }
}
