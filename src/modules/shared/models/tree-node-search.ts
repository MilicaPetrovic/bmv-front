export class TreeNodeSearch {
  label: string
  collapsedIcon: string
  expandedIcon: string

  constructor (label: string, collapsedIcon: string, expandedIcon: string) {
    this.label = label
    this.collapsedIcon = collapsedIcon
    this.expandedIcon = expandedIcon
  }
}
