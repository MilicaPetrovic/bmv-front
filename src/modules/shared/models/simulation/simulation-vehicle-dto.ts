export class SimulationVehicleDto {
  id: number;
  label : string;
  latitude : number;
  longitude : number;
  driverId : number;

  constructor (id: number, label: string, latitude:number, longitude :number, driverId : number) {
    this.id = id;
    this.label = label;
    this.latitude = latitude;
    this.longitude = longitude;
    this.driverId = driverId;
  }
}
