import {SimulationVehicleDto} from "./simulation-vehicle-dto";

export class SimulationDto {
  id:number;
  coordinates : L.LatLng[];
  simulationVehicleDTO : SimulationVehicleDto;
  drivingState : number;
  isPaid : boolean

  constructor (id: number, coordinates: L.LatLng[], simulationVehicleDTO: SimulationVehicleDto, drivingState:number, isPaid:boolean) {
    this.id = id;
    this.coordinates = coordinates;
    this.simulationVehicleDTO = simulationVehicleDTO;
    this.drivingState = drivingState;
    this.isPaid = isPaid;
  }
}
