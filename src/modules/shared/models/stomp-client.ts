export interface StompClient {
  connect(obj: unknown, callback: arrow): void
  subscribe(url: string, callback: arrowWithBodyType): void
  debug: null
}

export interface BodyData {
  body: string
}

type arrow = () => void
type arrowWithBodyType = (arg: BodyData) => void
