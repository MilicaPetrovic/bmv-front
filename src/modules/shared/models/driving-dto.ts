import { MapPoint } from './map-point'
import { RouteData } from './route-data'

export class DrivingDto {
  stations: MapPoint[]
  chosenRoute: RouteData | null
  linkedUsers: string[]
  vehicleTypeId = 1
  petFriendly = false
  babyFriendly = false
  coordinates: L.LatLng[] | null
  time: number
  distance: number
  price: number
  favouriteRoute: boolean
  future : boolean
  hour: number
  minute: number

  constructor (mapPointStations: MapPoint[], chosenRoute: RouteData, alreadyOrdered:boolean) {
    this.stations = mapPointStations
    this.chosenRoute = chosenRoute
    this.coordinates = this.chosenRoute.coordinates
    this.time = chosenRoute.totalTime
    this.price = chosenRoute.price
    this.distance = chosenRoute.totalDistance
    this.petFriendly = false
    this.babyFriendly = false
    this.favouriteRoute = false
    this.future = false
    this.hour = 0
    this.minute = 0
    if (!alreadyOrdered){
      this.distance = chosenRoute.totalDistance/1000
      this.time = Number((chosenRoute.totalTime/60).toFixed(2))
    }
  }
}
