import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DateArrayToStringPipe } from './pipes/date-array-to-string/date-array-to-string.pipe'
import { DrivingStateToStringPipe } from './pipes/driving-state-to-string/driving-state-to-string.pipe'
import { NotificationsComponent } from '../root/components/notifications/notifications.component'
import { SidebarModule } from 'primeng/sidebar'
import { SingleNotificationComponent } from '../root/components/single-notification/single-notification.component'
import { MatCardModule } from '@angular/material/card'
import { ViewMapComponent } from './components/view-map/view-map.component'
import { LeafletModule } from '@asymmetrik/ngx-leaflet'
import { ToastModule } from 'primeng/toast'
import { NotificationTypeToStringPipe } from './pipes/notification-type-to-string/notification-type-to-string.pipe'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { Interceptor } from './interceptors/interceptor.interceptor'
import {BadgeModule} from "primeng/badge";
import {ProfileModule} from "../profile/profile.module";
import {FormsModule} from "@angular/forms";
import { DrivingDetailedPassengersComponent } from './components/driving-detailed-passengers/driving-detailed-passengers.component';
import { DrivingDetailedMainComponent } from './components/driving-detailed-main/driving-detailed-main.component';
import {NgxStarsModule} from "ngx-stars";
import { SimulationMapComponent } from './components/simulation-map/simulation-map.component';

@NgModule({
  declarations: [
    DateArrayToStringPipe,
    DrivingStateToStringPipe,
    NotificationsComponent,
    ViewMapComponent,
    SingleNotificationComponent,
    NotificationTypeToStringPipe,
    DrivingDetailedPassengersComponent,
    DrivingDetailedMainComponent,
    SimulationMapComponent
  ],
  imports: [
    CommonModule,
    SidebarModule,
    MatCardModule,
    CommonModule,
    LeafletModule,
    ToastModule,
    BadgeModule,
    ProfileModule,
    FormsModule,
    NgxStarsModule
  ],
  exports: [
    DateArrayToStringPipe,
    DrivingStateToStringPipe,
    NotificationsComponent,
    ViewMapComponent,
    NotificationTypeToStringPipe,
    DrivingDetailedPassengersComponent,
    DrivingDetailedMainComponent,
    SimulationMapComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ]
})
export class SharedModule { }
