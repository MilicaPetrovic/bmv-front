import { Component, Input, OnInit } from '@angular/core'
import * as L from 'leaflet'
import { DrivingService } from '../../../registered-user/services/driving/driving.service'
import { icon, latLng, Map, MapOptions, marker, tileLayer } from 'leaflet'
import { DEFAULT_LATITUDE, DEFAULT_LONGITUDE } from '../../../map/app.constants'
import { MapPoint } from '../../models/map-point'

@Component({
  selector: 'app-view-map',
  templateUrl: './view-map.component.html',
  styleUrls: ['./view-map.component.css']
})
export class ViewMapComponent implements OnInit {
  map: Map
  options: MapOptions
  lastLayer: L.Layer[] = []
  @Input() drivingId: number

  constructor (private readonly drivingService: DrivingService) { }

  ngOnInit (): void {
    this.initializeMapOptions()
  }

  initializeMap (map: Map): void {
    console.log('initialize map')
    this.map = map
    this.map.setView([DEFAULT_LATITUDE, DEFAULT_LONGITUDE], this.map.getZoom())
    this.drawRoutePolyline()
  }

  private initializeMapOptions () {
    this.options = {
      zoom: 13,
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'OSM' })
      ]
    }
    this.lastLayer.push(tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: 'OSM'
    }))
  }

  private createMarker (stations: MapPoint[]) {
    this.clearMap()
    this.map.setView([DEFAULT_LATITUDE, DEFAULT_LONGITUDE], this.map.getZoom())

    const mapIcon = this.getDefaultIcon()
    for (const station of stations) {
      const coordinates2 = latLng([station.lat ? station.lat : 45.00,
        station.lng ? station.lng : 19.2331])

      this.lastLayer.push(marker(coordinates2).setIcon(mapIcon).addTo(this.map))
      this.map.setView(coordinates2, this.map.getZoom())
    }
  }

  public drawRoutePolyline (): void { //  id  je driving id za koji se ispisuje ruta
    this.drivingService.getRouteByDrivingId(this.drivingId).subscribe(data => {
      const polyline = L.polyline(data.coordinates, { color: 'red' })
      polyline.addTo(this.map);
      console.log("***************************************************************")
      console.log(data.stations)
      this.createMarker(data.stations)
    }, error => {
      console.log("-------------------------------------------------------------")
      console.log(error)

    })
  }

  private getDefaultIcon () {
    return icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/marker/marker-icon.png'
    })
  }

  public clearMap ():void {
    const numOfLayers = this.lastLayer.length
    for (let i = 1; i < numOfLayers; i++) {
      if (this.map.hasLayer(this.lastLayer[i])) {
        this.map.removeLayer(this.lastLayer[i])
      }
    }
  }
}
