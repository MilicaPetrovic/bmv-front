import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingDetailedPassengersComponent } from './driving-detailed-passengers.component';

describe('DrivingDetailedPassengersComponent', () => {
  let component: DrivingDetailedPassengersComponent;
  let fixture: ComponentFixture<DrivingDetailedPassengersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingDetailedPassengersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DrivingDetailedPassengersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
