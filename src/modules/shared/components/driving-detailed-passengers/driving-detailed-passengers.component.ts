import {Component, Input} from '@angular/core';
import {PassengerDto} from "../../../admin/models/detailed-driving-dto";

@Component({
  selector: 'app-admin-driving-detailed-passengers',
  templateUrl: './driving-detailed-passengers.component.html',
  styleUrls: ['./driving-detailed-passengers.component.css']
})
export class DrivingDetailedPassengersComponent {

  @Input() passengers: PassengerDto[]

}
