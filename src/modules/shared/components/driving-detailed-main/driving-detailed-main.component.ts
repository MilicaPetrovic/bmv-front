import {Component, Input} from '@angular/core';
import {DetailedDrivingDto} from "../../../admin/models/detailed-driving-dto";

@Component({
  selector: 'app-admin-driving-detailed-main',
  templateUrl: './driving-detailed-main.component.html',
  styleUrls: ['./driving-detailed-main.component.css']
})
export class DrivingDetailedMainComponent {

  @Input() driving : DetailedDrivingDto


}
