import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingDetailedMainComponent } from './driving-detailed-main.component';

describe('DrivingDetailedMainComponent', () => {
  let component: DrivingDetailedMainComponent;
  let fixture: ComponentFixture<DrivingDetailedMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingDetailedMainComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DrivingDetailedMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
