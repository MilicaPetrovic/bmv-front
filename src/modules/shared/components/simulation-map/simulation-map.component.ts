import {Component, OnInit} from '@angular/core';
import {icon, latLng, LayerGroup, marker, tileLayer} from "leaflet";
import {SimulationService} from "../../simulation-service/simulation.service";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import * as L from 'leaflet'
import {SimulationDto} from "../../models/simulation/simulation-dto";
import {SimulationVehicleDto} from "../../models/simulation/simulation-vehicle-dto";
import {StompClient} from "../../models/stomp-client";

@Component({
  selector: 'app-simulation-map',
  templateUrl: './simulation-map.component.html',
  styleUrls: ['./simulation-map.component.css']
})
export class SimulationMapComponent implements OnInit {
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '...',
      }),
    ],
    zoom: 14,
    center: latLng(45.253434, 19.831323),
  };
  vehicles: any = {};
  drivings: any = {};
  mainGroup: LayerGroup[] = [];
  private stompClient: StompClient;

  constructor(private simulationService: SimulationService) {
  }


  ngOnInit(): void {
    this.initializeWebSocketConnection();
    this.simulationService.getAllDrivers().subscribe((ret) => {
      this.initialize_route(ret);
    });
    this.simulationService.getDrivingForDraw().subscribe((ret) => {
      this.draw(ret);
    })
  }

  initialize_route(ret: SimulationVehicleDto[]) : void {
    console.log("results from back")
    for (const vehicle of ret) {
      //  let color = Math.floor(Math.random() * 16777215).toString(16);
      const geoLayerRouteGroup: LayerGroup = new LayerGroup();
      console.log(vehicle);

      // let routeLayer = L.polyline(ride.coordinates);
      // routeLayer.setStyle({ color: `#${color}` });
      // routeLayer.addTo(geoLayerRouteGroup);
      // this.drivings[ride.id] = geoLayerRouteGroup;

      console.log("markeri");
      const markerLayer = marker([vehicle.longitude, vehicle.latitude], {
        icon: icon({
          iconUrl: 'assets/marker/car.png',
          iconSize: [35, 45],
          iconAnchor: [18, 45],
        }),
      });
      markerLayer.addTo(geoLayerRouteGroup);
      console.log(" na pocetku " + vehicle.id)
      this.vehicles[vehicle.id] = markerLayer;
      this.mainGroup = [...this.mainGroup, geoLayerRouteGroup];
    }

  }

  initializeWebSocketConnection() : void {
    const ws = new SockJS('https://localhost:8080/uber/socket');
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => {
      this.openGlobalSocket();
    });
  }


  openGlobalSocket() : void{
    this.stompClient.subscribe('/topic/map-updates/update-vehicle-position', (message: { body: string }) => {
      console.log("radi socket")
      console.log(message.body)
      const vehicle: SimulationVehicleDto = JSON.parse(message.body);
      const existingVehicle = this.vehicles[vehicle.id];
      console.log("objekat vehicles")
      console.log(this.vehicles["5"])
      existingVehicle.setLatLng([vehicle.longitude, vehicle.latitude]);
      existingVehicle.update();

    });

    this.stompClient.subscribe('/topic/map-updates/new-ride', (message: { body: string }) => {
      const ride: SimulationDto = JSON.parse(message.body);
      this.draw(ride);
   /*   const geoLayerRouteGroup: LayerGroup = new LayerGroup();
      const color = Math.floor(Math.random() * 16777215).toString(16);

      const routeLayer = L.polyline(ride.coordinates);
      routeLayer.setStyle({color: `#${color}`});
      routeLayer.addTo(geoLayerRouteGroup);
      this.drivings[ride.id] = geoLayerRouteGroup;

      this.mainGroup = [...this.mainGroup, geoLayerRouteGroup];

      const existingVehicle = this.vehicles[ride.simulationVehicleDTO.id];
      console.log("objekat vehicles")
      console.log(this.vehicles["5"])
      existingVehicle.setLatLng([ride.simulationVehicleDTO.longitude, ride.simulationVehicleDTO.latitude]);
      existingVehicle.update();
      */
    });

    this.stompClient.subscribe('/topic/map-updates/ended-ride', (message: { body: string }) => {
      console.log(" end voznje")
      console.log(message.body)
      const ride: number = JSON.parse(message.body);
      this.mainGroup = this.mainGroup.filter((lg: LayerGroup) => lg !== this.drivings[ride]);
      // delete this.vehicles[ride.simulationVehicleDTO.id];
      delete this.drivings[ride];
    });

    this.stompClient.subscribe('/topic/map-updates/delete-all-rides', (message ) => {
      this.vehicles = {};
      this.drivings = {};
      this.mainGroup = [];
    });
  }

  private draw(ride: SimulationDto) {
    console.log("DRAW.......")
    console.log(ride)
    if(ride !== null) {
      const geoLayerRouteGroup: LayerGroup = new LayerGroup();
      const color = Math.floor(Math.random() * 16777215).toString(16);

      const routeLayer = L.polyline(ride.coordinates);
      routeLayer.setStyle({color: `#${color}`});
      routeLayer.addTo(geoLayerRouteGroup);
      this.drivings[ride.id] = geoLayerRouteGroup;
      this.mainGroup = [...this.mainGroup, geoLayerRouteGroup];

      const existingVehicle = this.vehicles[ride.simulationVehicleDTO.id];
      console.log("objekat vehicles")
      console.log(this.vehicles["5"])
      existingVehicle.setLatLng([ride.simulationVehicleDTO.longitude, ride.simulationVehicleDTO.latitude]);
      existingVehicle.update();
    }
  }
}
