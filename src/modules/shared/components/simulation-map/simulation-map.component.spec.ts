import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulationMapComponent } from './simulation-map.component';

describe('SimulationMapComponent', () => {
  let component: SimulationMapComponent;
  let fixture: ComponentFixture<SimulationMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimulationMapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SimulationMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
