import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'notificationTypeToString'
})
export class NotificationTypeToStringPipe implements PipeTransform {
  transform (value: string): string {
    switch (value) {
      case 'SPLIT_FARE_APPROVAL':
        return 'Neko hoće da te doda u njihovu vožnju'
      case 'APPROVED_DRIVING':
        return 'Našli smo vožača'
      case 'ARRIVED_DRIVER':
        return 'Vožač je stigao'
      case 'DRIVING_REMINDER':
        return 'Imate zakazanu vožnju'
      case 'RATE_DRIVING':
        return 'Ocenite vašu vožnju'
      case 'ADDED_DRIVING':
        return 'Dodeljeni ste sledećem vožnju'
      case 'REFUSED_DRIVING':
        return 'Vožnja je bila odbijana'
    }
    return 'notifikcaija'
  }
}
