import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'drivingStateToString'
})
export class DrivingStateToStringPipe implements PipeTransform {
  transform (value: string): string {
    switch (value) {
      case 'MADE':
        return 'napravljen'
      case 'ACCEPTED':
        return 'prihvaćen'
      case 'CURRENT':
        return 'trenutni'
      case 'REJECTED':
        return 'odbijen'
      case 'SUCCESSFUL':
        return 'uspešan'
      case 'REFUSED':
        return 'neuspešno'
      case 'PAID':
        return 'plaćeno'
    }
    return value
  }
}
