import {HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {SimulationDto} from "../models/simulation/simulation-dto";
import {environment} from "../../../environments/environment";
import {SimulationVehicleDto} from "../models/simulation/simulation-vehicle-dto";

@Injectable({
  providedIn: 'root'
})
export class SimulationService {
  private readonly SIMULATION_PATH = environment.api_path + 'simulation'
  private readonly VEHICLE_TYPE_PATH = environment.api_path + 'vehicle/type'

  constructor(private http: HttpClient) {}

  getAllActiveDrivings(): Observable<SimulationDto[]> {
    return this.http.get<SimulationDto[]>(`${this.SIMULATION_PATH}`)
  }

  getAllCurrentDrivings(): Observable<SimulationDto[]> {
    return this.http.get<SimulationDto[]>(`${this.SIMULATION_PATH}`+"/current");
  }

  getAllDrivers():Observable<SimulationVehicleDto[]>{
    return this.http.get<SimulationVehicleDto[]>(`${this.VEHICLE_TYPE_PATH}` +"/vehicle/all");
  }

  getDrivingForDraw():Observable<SimulationDto>{
    return this.http.get<SimulationDto>(`${this.SIMULATION_PATH}`+"/draw");
  }
}
