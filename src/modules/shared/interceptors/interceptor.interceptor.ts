import { Injectable } from '@angular/core'
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders
} from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept (
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const item = localStorage.getItem('token')
    if (item) {

      const cloned = req.clone({
        headers: new HttpHeaders()
          .set('Authorization', 'Bearer ' + item)
          //.set('Content-Type', 'application/json')
      })

      return next.handle(cloned)
    } else {
      console.log('No item')
      return next.handle(req)
    }
  }
}
