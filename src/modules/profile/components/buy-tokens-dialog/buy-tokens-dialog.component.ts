import { Component, ElementRef, OnInit, ViewChild, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { RegisteredUserService } from '../../services/registered-user/registered-user.service';
import { statusDTO } from 'src/modules/shared/models/status-dto'

declare let paypal: any;

@Component({
  selector: 'app-buy-tokens-dialog',
  templateUrl: './buy-tokens-dialog.component.html',
  styleUrls: ['./buy-tokens-dialog.component.css']
})
export class BuyTokensDialogComponent implements OnInit {

  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;

  selectedPackage: number;
  packages: number[] = [500, 1000, 2000, 5000];
  value: number;

  constructor(private userService: RegisteredUserService,
    public dialogRef: MatDialogRef<BuyTokensDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public statusDTO: statusDTO) { 
    this.selectedPackage = this.packages[1];
  }

  ngOnInit(): void {
    paypal
      .Buttons({
        style: {
          layout: 'horizontal',
          color: 'blue',
        },

        createOrder: (data: any, actions: any) => {
          return actions.order.create({
            purchase_units: [
              {
                description: `Paket od ${this.selectedPackage} tokena`,
                amount: {
                  currency_code: 'USD',
                  value: (this.selectedPackage / 110.2).toFixed(2)
                }
              }
            ]
          })
        },

        onApprove: (data: any, actions: any) => {
          return actions.order.capture().then( () => {
            this.userService.buyTokens(this.selectedPackage).subscribe(
              res => {
                this.dialogRef.close([res, this.selectedPackage])
              })
          })
        }
      })
      .render(this.paypalElement.nativeElement);
  }

}
