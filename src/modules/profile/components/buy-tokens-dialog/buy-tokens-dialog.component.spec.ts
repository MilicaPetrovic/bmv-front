import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyTokensDialogComponent } from './buy-tokens-dialog.component';

describe('BuyTokensDialogComponent', () => {
  let component: BuyTokensDialogComponent;
  let fixture: ComponentFixture<BuyTokensDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyTokensDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyTokensDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
