import { City } from "src/modules/shared/models/city";

export interface User {
    email: string,
    name: string,
    surname: string,
    phone: string,
    username: string,
    city: City,
    tokens: number,
    provider: string
}
