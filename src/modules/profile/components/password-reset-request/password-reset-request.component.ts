import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PasswordResetService } from '../../services/password-reset/password-reset.service'
import { MessageService } from 'primeng/api'
import { AuthService } from '../../../auth/services/auth/auth.service'

@Component({
  selector: 'app-password-reset-request',
  templateUrl: './password-reset-request.component.html',
  styleUrls: ['./password-reset-request.component.css'],
  providers: [MessageService]
})
export class PasswordResetRequestComponent implements OnInit {
  passwordResetForm: FormGroup

  constructor (
    private readonly fb: FormBuilder,
    private readonly messageService: MessageService,
    private readonly passwordResetService: PasswordResetService,
    private readonly userService: AuthService
  ) {
    userService.checkIfSomeoneIsLoggedIn()
  }

  ngOnInit ():void {
    this.passwordResetForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]]
    })

    this.passwordResetForm.valueChanges.subscribe()
  }

  submit ():void {
    if (!this.passwordResetForm.valid) { return }
    this.passwordResetService.requestPasswordReset(this.passwordResetForm.value.email).subscribe(data => {
      if (data.successful) {
        this.messageService.add({
          key: 'password-reset-request-message',
          severity: 'success',
          summary: 'Uspešno poslat mejl',
          detail: 'U Vama poslatom mejlu imate dalje istrukcije.',
          life: 10000
        })
      } else if (data.exceptionCode == 6) {
        this.messageService.add({
          key: 'password-reset-request-message',
          severity: 'error',
          summary: 'Nspešna reset',
          detail: 'Koriniku nije dozvoljen pristup. Proverite da li ste već verifikovali vašu imejl adresu',
          life: 5000
        })
      } else if (data.exceptionCode == 1) {
        this.messageService.add({
          key: 'password-reset-request-message',
          severity: 'error',
          summary: 'Nspešna reset',
          detail: 'Korinikčko ime nije korišćen',
          life: 5000
        })
      } else {
        this.messageService.add({
          key: 'password-reset-request-message',
          severity: 'error',
          summary: 'Nspešna reset',
          detail: 'Nešto se desilo',
          life: 5000
        })
      }
    })
  }
}
