import { Component, OnInit } from '@angular/core'
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective, NgForm,
  ValidatorFn,
  Validators
} from '@angular/forms'
import { MessageService } from 'primeng/api'
import { PasswordResetService } from '../../services/password-reset/password-reset.service'
import { ResetPasswordDTO } from '../../model/reset-password-dto'
import { ActivatedRoute, Router } from '@angular/router'
import { ErrorStateMatcher } from '@angular/material/core'
import { AuthService } from '../../../auth/services/auth/auth.service'

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css'],
  providers: [MessageService]
})
export class PasswordResetComponent implements OnInit {
  resetPasswordDto: ResetPasswordDTO
  passwordResetForm: FormGroup
  passwordFormControl: FormControl = new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-8])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')])
  passwordAgainFormControl: FormControl
  matcher = new MyErrorStateMatcher()

  constructor (
    private readonly route: ActivatedRoute,
    private readonly _router: Router,
    private readonly fb: FormBuilder,
    private readonly messageService: MessageService,
    private readonly passwordResetService: PasswordResetService,
    private readonly userService: AuthService
  ) {
    userService.checkIfSomeoneIsLoggedIn()
    this.route.params.subscribe(
      params => {
        this.resetPasswordDto = {
          username: String(params['username']),
          resetCode: String(params['code']),
          newPassword: ''
        }
      })
  }

  ngOnInit (): void {
    this.passwordAgainFormControl = new FormControl('', [Validators.required, this.createPasswordMatchingValidator(this.passwordFormControl)])
    this.passwordResetForm = this.fb.group({
      password: this.passwordFormControl,
      passwordAgain: this.passwordAgainFormControl
    })
    this.passwordResetForm.valueChanges.subscribe()
  }

  submit () : void {
    if (this.passwordResetForm.valid) { // this.passwordAgainFormControl.valid && this.passwordFormControl.valid){
      this.resetPasswordDto.newPassword = this.passwordFormControl.value
      this.passwordResetService.resetPassword(this.resetPasswordDto).subscribe(data => {
        if (data.successful) {
          this.messageService.add({
            key: 'password-reset-message',
            severity: 'success',
            summary: 'Uspešno poslat mejl',
            detail: 'Uspešno ste promenili lozinku.\nMolimo Vas ulogujte se opet.',
            life: 5000
          })
          //const that = this
          setTimeout(() => { this._router.navigate(['auth/login']) }, 4000)
        } else if (data.exceptionCode == 8) {
          this.messageService.add({
            key: 'password-reset-message',
            severity: 'error',
            summary: 'Nspešna reset',
            detail: 'Reset kod nije ispravan',
            life: 5000
          })
        } else if (data.exceptionCode == 1) {
          this.messageService.add({
            key: 'password-reset-message',
            severity: 'error',
            summary: 'Nspešna reset',
            detail: 'Korinikčko ime nije korišćen',
            life: 5000
          })
        } else if (data.exceptionCode == 5) {
          this.messageService.add({
            key: 'password-reset-message',
            severity: 'error',
            summary: 'Nspešna reset',
            detail: 'Reset kod više nije validan',
            life: 5000
          })
        } else {
          this.messageService.add({
            key: 'password-reset-message',
            severity: 'error',
            summary: 'Nspešna reset',
            detail: 'Nešto se desilo',
            life: 5000
          })
        }
      })
    }
  }

  createPasswordMatchingValidator (passwordFormControl: FormControl): ValidatorFn {
    {
      return (control: AbstractControl): Record<string, string> | null =>
        control.value === passwordFormControl.value
          ? null
          : { wrongValue: control.value }
    }
  }
}

class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = (form != null) && form.submitted
    return !!((control != null) && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}
