import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../../services/user/user-service.service';
import { SocialPhotoDto } from '../model/social-photo-dto';
import { User } from '../model/user';

@Component({
  selector: 'app-profile-photo',
  templateUrl: './profile-photo.component.html',
  styleUrls: ['./profile-photo.component.css']
})
export class ProfilePhotoComponent implements OnChanges {

  @Input() style: string;

  @Input() username: string;

  @Input() profilePhoto: any;

  constructor(private userService: UserService,
              private readonly sanitizer: DomSanitizer) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['username']?.previousValue === changes['username']?.currentValue)
      return;

    if (this.username === undefined)
      return;

    this.userService.getUserByUsername(this.username).subscribe({
      next: (res: User) => {
        if (res.provider === 'NO_PROVIDER' || res.provider === null) {
          this.userService.getProfilePhoto(this.username).subscribe({
            next: (res: Blob) => {
              const objectURL = URL.createObjectURL(res)
              this.profilePhoto = this.sanitizer.bypassSecurityTrustUrl(objectURL)
            },
            error: () => {
              this.profilePhoto = 'https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg'
            }
          })
        } else {
          this.userService.getSocailProfilePhoto(this.username).subscribe(
            (res: SocialPhotoDto) => {
              this.profilePhoto = res.path;
            }
          )
        }
      }
    })
  }

}
