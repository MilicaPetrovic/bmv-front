export interface ResetPasswordDTO {
  username: string
  resetCode: string
  newPassword: string
}
