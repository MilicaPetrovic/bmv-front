import { Component, OnInit } from '@angular/core'
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import { DomSanitizer } from '@angular/platform-browser'
import { MessageService } from 'primeng/api'
import { AuthService } from 'src/modules/auth/services/auth/auth.service'
import { UserService } from 'src/modules/profile/services/user/user-service.service'
import { BuyTokensDialogComponent } from '../../components/buy-tokens-dialog/buy-tokens-dialog.component'
import { ChangePasswordDialogComponent } from '../../components/change-password-dialog/change-password-dialog.component'
import { User } from '../../components/model/user'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [MessageService]
})
export class UserProfileComponent implements OnInit {
  form = new FormGroup({
    email: new FormControl({ value: '', disabled: true }, [Validators.required, Validators.email]),
    name: new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+'), Validators.minLength(3), Validators.maxLength(20)]),
    surname: new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+'), Validators.minLength(3), Validators.maxLength(20)]),
    phone: new FormControl('', [Validators.required, Validators.pattern('[\+][0-9]{6,13}'), Validators.minLength(6), Validators.maxLength(13)]),
    username: new FormControl({ value: '', disabled: true }, [Validators.required]),
    city: new FormControl('', [Validators.pattern('[A-Za-z ]*'), Validators.minLength(2), Validators.maxLength(40)]),
    zip: new FormControl(0, [Validators.pattern('[1-9]+[0-9]*'), Validators.minLength(2), Validators.maxLength(10)])
  })

  profilePhoto: any
  username: string;
  tokens: number;
  provider: string;

  passwordData = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])
  })

  constructor (private readonly userService: UserService,
    private readonly sanitizer: DomSanitizer,
    public passwordDialog: MatDialog,
    public tokensDialog: MatDialog,
    private readonly messageService: MessageService,
    private authService: AuthService) { }

  ngOnInit (): void {
    this.userService.getUser().subscribe({
      next: (res: User) => {
        this.form.setValue({
          email: res.email,
          name: res.name,
          surname: res.surname,
          phone: res.phone || '',
          username: res.username,
          city: res.city?.name || '',
          zip: res.city?.postNumber || 0
        })
        this.username = res.username;
        this.tokens = res.tokens;
        this.provider = res.provider;
      }
    })
  }

  selectPhoto (event: any): void {
    if (this.provider === 'GOOGLE' || this.provider === 'FACEBOOK') {
      this.messageService.add({key: 'update-profile-message', severity: 'info', summary: 'Obaveštenje', detail: 'Ne možete promeniti profilnu sliku jer ste ulogovani preko socijalne mreže.'})
      return;
    }
    const data = new FormData()
    data.append('files', event.target.files[0])
    this.userService.changeProfilePhoto(data).subscribe({
      next: (res: Blob) => {
        const objectURL = URL.createObjectURL(res)
        this.profilePhoto = this.sanitizer.bypassSecurityTrustUrl(objectURL)
        this.messageService.add({ key: 'update-profile-message', severity: 'success', summary: 'Uspešna promena slike', detail: 'Profilna slika je uspešno ažurirana.' })
      }
    })
  }

  updateUser (): void {
    if (!this.form.valid) { return }
    this.userService.updateUser(this.form).subscribe(
      (result) => {
        if (result.successful) { this.messageService.add({ key: 'update-profile-message', severity: 'success', summary: 'Uspešna promena podataka', detail: result.exceptionMessage }) } else { this.messageService.add({ key: 'update-profile-message', severity: 'error', summary: 'Neuspešna promena podataka', detail: 'Desila se greška prilikom ažuriranja podataka. Molimo pokušajte ponovo kasnije.', life: 5000 }) }
      })
  }

  openPasswordDialog (): void {
    const dialogRef = this.passwordDialog.open(ChangePasswordDialogComponent)

    dialogRef.afterClosed().subscribe(res => {
      if (res.successful === null || res.successful === undefined) { return }
      if (res.successful) {
        this.messageService.add({ key: 'change-password-message', severity: 'success', summary: 'Uspešna promena lozinke', detail: 'Lozinka je uspešno ažurirana i bićete preusmereni na stranicu za prijavu za 3 sekunde.' })
        setTimeout(() => {
          this.authService.logout();
        }, 3000);
      }
      else { 
        this.messageService.add({ key: 'change-password-message', severity: 'error', summary: 'Neuspešna promena lozinke', detail: res.exceptionMessage, life: 5000 }) 
      }
    })
  }

  openTokensDialog(): void {
    const dialogRef = this.tokensDialog.open(BuyTokensDialogComponent);

    dialogRef.afterClosed().subscribe(res => {

      if (res[0].successful === null || res[0].successful === undefined) { return }
      if (res[0].successful) {
        this.messageService.add({ key: 'buy-tokens-message', severity: 'success', summary: 'Uspešna kupovina tokena', detail: 'Tokeni su uspešno kupljeni.' })
        this.tokens += res[1]
      }
      else { 
        this.messageService.add({ key: 'buy-tokens-message', severity: 'error', summary: 'Neuspešna kupovina tokena', detail: res[0].exceptionMessage, life: 5000 }) 
      }
    })
  }

}
