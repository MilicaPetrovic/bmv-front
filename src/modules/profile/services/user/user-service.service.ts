import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { environment } from '../../../../environments/environment'
import { FormGroup } from '@angular/forms'
import { statusDTO } from 'src/modules/shared/models/status-dto'
import { Observable } from 'rxjs'
import { User } from '../../components/model/user'
import { SocialPhotoDto } from '../../components/model/social-photo-dto'


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly API_PATH = environment.api_path + 'user'
  authHeader: HttpHeaders

  constructor (private readonly httpClient: HttpClient,
    private readonly authService: AuthService) {
    this.authHeader = authService.getAuthHeader()
  }

  public getUser (): Observable<User> {
    return this.httpClient.get<User>(this.API_PATH + '/get', { headers: this.authHeader })
  }

  public changeProfilePhoto (data: FormData) {
    return this.httpClient.put(this.API_PATH + '/change/photo', data,
      {
        responseType: 'blob',
        headers: this.authHeader
      })
  }

  public updateUser (data: FormGroup): Observable<statusDTO> {
    const dto = this.createDTO(data)
    return this.httpClient.put<statusDTO>(this.API_PATH + '/update', dto,
      {
        headers: this.authHeader
      })
  }

  public changePassword (data: any): Observable<statusDTO> {
    return this.httpClient.put<statusDTO>(this.API_PATH + '/change/password', data,
      {
        headers: this.authHeader
      })
  }

  public getUserByUsername(username: string): Observable<User> {
    return this.httpClient.get<User>(`${this.API_PATH}/get/${username}`,
      {
        headers: this.authHeader
      })
  }

  public getProfilePhoto(username: string): Observable<Blob> {
    return this.httpClient.get(`${this.API_PATH}/get/photo/${username}`,
      {
        responseType: 'blob',
        headers: this.authHeader
      })
  }

  public getSocailProfilePhoto(username: string): Observable<SocialPhotoDto> {
    return this.httpClient.get<SocialPhotoDto>(`${this.API_PATH}/get/social/photo/${username}`,
      {
        headers: this.authHeader
      })
  }

  private createDTO (data: FormGroup) {
    const dto = {
      email: data.value.email,
      name: data.value.name,
      surname: data.value.surname,
      phone: data.value.phone,
      username: data.value.username,
      city: {
        name: data.value.city,
        zip: data.value.zip
      }
    }
    return dto
  }
}
