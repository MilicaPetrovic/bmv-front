import { Injectable } from '@angular/core'
import { statusDTO } from '../../../shared/models/status-dto'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from 'rxjs'
import { ResetPasswordDTO } from '../../model/reset-password-dto'

@Injectable({
  providedIn: 'root'
})
export class PasswordResetService {
  private readonly path = environment.api_path + 'reset/password'

  constructor (private readonly httpClient: HttpClient) { }

  requestPasswordReset (username: string): Observable<statusDTO> {
    const email = {
      text: username
    }
    return this.httpClient.post<statusDTO>(`${this.path}/request`, email)
  }

  resetPassword (resetPasswordDto: ResetPasswordDTO): Observable<statusDTO> {
    return this.httpClient.post<statusDTO>(`${this.path}`, resetPasswordDto)
  }
}
