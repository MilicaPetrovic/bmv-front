import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from 'src/modules/auth/services/auth/auth.service';
import { statusDTO } from 'src/modules/shared/models/status-dto';
import { environment } from '../../../../environments/environment'
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RegisteredUserService {

  private readonly API_PATH = environment.api_path + 'client'
  authHeader: HttpHeaders

  constructor (private readonly httpClient: HttpClient,
    private readonly authService: AuthService) {
    this.authHeader = authService.getAuthHeader()
  }


  public buyTokens(tokens: number): Observable<statusDTO> {
    return this.httpClient.put<statusDTO>(`${this.API_PATH}/buy/tokens`, tokens,
      {
        headers: this.authHeader
      })
  }
}
