import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ChangePasswordDialogComponent } from './components/change-password-dialog/change-password-dialog.component'
import { PasswordResetComponent } from './components/password-reset/password-reset.component'
import { PasswordResetRequestComponent } from './components/password-reset-request/password-reset-request.component'
import { UserProfileComponent } from './pages/user-profile/user-profile.component'
import { PasswordResetService } from './services/password-reset/password-reset.service'
import { UserService } from './services/user/user-service.service'
import { RouterModule } from '@angular/router'
import { ProfileRoutes } from './profile.routes'
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule } from '@angular/forms'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { MatInputModule } from '@angular/material/input'
import { ToastModule } from 'primeng/toast'
import { MatCardModule } from '@angular/material/card'
import { MatDialogModule } from '@angular/material/dialog';
import { ProfilePhotoComponent } from './components/profile-photo/profile-photo.component';
import { BuyTokensDialogComponent } from './components/buy-tokens-dialog/buy-tokens-dialog.component'
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ChangePasswordDialogComponent,
        PasswordResetComponent,
        PasswordResetRequestComponent,
        UserProfileComponent,
        ProfilePhotoComponent,
        BuyTokensDialogComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(ProfileRoutes),
        MatFormFieldModule,
        ReactiveFormsModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        ToastModule,
        MatCardModule,
        MatDialogModule,
        MatRadioModule,
        FormsModule
    ],
    exports: [
        ProfilePhotoComponent
    ],
    providers: [
        PasswordResetService,
        UserService
    ]
})
export class ProfileModule { }
