import { Routes } from '@angular/router'
import { RoleGuard } from '../auth/guards/role/role.guard'
import { UserProfileComponent } from './pages/user-profile/user-profile.component'
import { PasswordResetRequestComponent } from './components/password-reset-request/password-reset-request.component'
import { PasswordResetComponent } from './components/password-reset/password-reset.component'
import { LoginGuard } from '../auth/guards/login/login.guard'

export const ProfileRoutes: Routes = [
  {
    path: 'user',
    pathMatch: 'full',
    component: UserProfileComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'admin|driver|registeredUser' }
  },
  {
    path: 'reset/password/request',
    pathMatch: 'full',
    component: PasswordResetRequestComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'reset/password/:username/:code',
    pathMatch: 'full',
    component: PasswordResetComponent,
    canActivate: [LoginGuard]
  }
]
