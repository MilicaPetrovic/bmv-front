import { Injectable } from '@angular/core'
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable} from "rxjs";
import {UserTokenState} from "../../../auth/model/user-token-state";

@Injectable({
  providedIn: 'root'
})
export class AccountVerificationService {
  private readonly API_PATH = environment.api_path

  requestHeader = new HttpHeaders(
    { 'No-Auth': 'True' }
  )

  constructor (private readonly httpClient: HttpClient) { }

  public verify (username: string, code: string) : Observable<UserTokenState>{
    const url = this.API_PATH + 'verify'
    return this.httpClient.get<UserTokenState>(`${url}/${username}/${code}`, {
    })
  }
}
