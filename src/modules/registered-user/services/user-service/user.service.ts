import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../../environments/environment'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  private readonly API_PATH = environment.api_path + 'user'
  constructor (private readonly http: HttpClient) { }

  getAllUsernamesForSplitFair (): Observable<string[]> {
    return this.http.get<string[]>(`${this.API_PATH}` + '/username/all/registered')
  }
}
