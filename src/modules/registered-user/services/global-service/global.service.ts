import { Injectable } from '@angular/core';
import {DrivingDto} from "../../../shared/models/driving-dto";
import {MapPoint} from "../../../shared/models/map-point";

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  drivingDTO: DrivingDto
  mapPointStations: MapPoint[]


  public getDrivingDto(): DrivingDto {
    return this.drivingDTO;
  }

  public setDrivingDto(drivingDto: DrivingDto): void {
    this.drivingDTO = drivingDto
  }

  public getMapPointStations(): MapPoint[] {
    return this.mapPointStations
  }

  public setMapPointStations(mapPointStations:MapPoint[]): void {
    this.mapPointStations = mapPointStations;
  }


}
