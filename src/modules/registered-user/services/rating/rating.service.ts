import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {ClientDrivingRatingDto} from "../../model/client-driving-rating-dto";

@Injectable({
  providedIn: 'root'
})
export class RatingService {
  private readonly API_PATH = environment.api_path + 'client/driving'

  constructor (private readonly http: HttpClient) { }

  saveRating (ratingDto: ClientDrivingRatingDto): Observable<StatusResponseDto> {
    return this.http.post<StatusResponseDto>(`${this.API_PATH}/rating`, ratingDto);
  }
}
