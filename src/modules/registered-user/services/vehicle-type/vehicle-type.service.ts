import { Injectable } from '@angular/core'
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { VehicleTypeDto } from '../../model/vehicle-type-dto'

@Injectable({
  providedIn: 'root'
})
export class VehicleTypeService {
  private readonly API_PATH = environment.api_path + 'vehicle/type'
  requestHeader = new HttpHeaders(
    { 'No-Auth': 'True' }
  )

  constructor (private readonly http: HttpClient) { }

  getAllVehicleTypes (): Observable<VehicleTypeDto[]> {
    return this.http.get<VehicleTypeDto[]>(`${this.API_PATH}` + '/all')
  }
}
