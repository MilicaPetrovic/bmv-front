import { Injectable } from '@angular/core'
import { DrivingDto } from '../../../shared/models/driving-dto'
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { RouteViewDto } from '../../../shared/models/route-view-dto'
import {PaginationDTO} from "../../../shared/models/pagination-dto";
import {ClientDrivingHistoryDto} from "../../model/client-driving-history-dto";
import {ClientDetailedDrivingDto} from "../../model/client-detailed-driving-dto";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import { PurchaseDrivingDTO } from '../../model/purchase-driving-dto'

@Injectable({
  providedIn: 'root'
})
export class DrivingService {

  private readonly API_PATH = environment.api_path + ''
  authHeader: HttpHeaders

  private readonly ROUTE_PATH = environment.api_path + 'driving/history/route/'

  constructor (private readonly http: HttpClient, private readonly authService: AuthService) {
    this.authHeader = authService.getAuthHeader()
  }

  saveDriving (drivingDto: DrivingDto): Observable<StatusResponseDto> {
    console.log("futureeee   : ")
    console.log(drivingDto.future)
    return this.http.post<StatusResponseDto>(`${this.API_PATH}make/driving`, drivingDto, {
      headers: this.authHeader
    })
  }

  getRouteByDrivingId (id: number): Observable<RouteViewDto> {
    return this.http.get<RouteViewDto>(`${this.ROUTE_PATH}` + id, {
      headers: this.authHeader
    })
  }

  /*getDrivings (from: number, until: number): Observable<PaginationDTO<ClientDrivingHistoryDto>> {
    return this.http.get<PaginationDTO<ClientDrivingHistoryDto>>(`${this.API_PATH}driving/history/all/${from}/${until}`,
    )
  }*/
  getDrivings (from: number, until: number, sortBy: string): Observable<PaginationDTO<ClientDrivingHistoryDto>> {
    return this.http.get<PaginationDTO<ClientDrivingHistoryDto>>(`${this.API_PATH}driving/history/all/${from}/${until}/${sortBy}`,
    )
  }

  getDriving (drivingId: number): Observable<ClientDetailedDrivingDto> {
    return this.http.get<ClientDetailedDrivingDto>(`${this.API_PATH}driving/history/${drivingId}`)
  }

  toggleRouteAsFavouriteByDrivingId(drivingId: number) : Observable<StatusResponseDto>{
    return this.http.get<StatusResponseDto>(`${this.API_PATH}client/toggle/route/${drivingId}`)
  }

  getAvailableDriver() : Observable<StatusResponseDto> {
    return this.http.get<StatusResponseDto>(`${this.API_PATH}get/driver`, {
      headers: this.authHeader
    })
  }

  purchaseDriving(dto: PurchaseDrivingDTO): Observable<StatusResponseDto> {
    return this.http.post<StatusResponseDto>(`${this.API_PATH}purchase/driving`, dto,
    {headers: this.authHeader})
  }

  getFavouriteRoutes():Observable<RouteViewDto[]>{
    return this.http.get<RouteViewDto[]>(`${this.API_PATH}driving/history/favourite`)
  }

}
