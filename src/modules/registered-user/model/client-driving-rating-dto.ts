export interface ClientDrivingRatingDto {
  drivingId: number
  driverRating: number
  vehicleRating: number
  text: string
}
