import {DetailedDrivingDto} from "../../admin/models/detailed-driving-dto";

export interface ClientDetailedDrivingDto extends DetailedDrivingDto {
  clientCanLeaveRating: boolean
  favourite: boolean
}
