import {RouteViewDto} from "../../shared/models/route-view-dto";

export class FavouriteRoutes {

  value: RouteViewDto;
  viewValue: string;

  constructor(value:RouteViewDto, viewValue:string){
    this.value = value
    this.viewValue = viewValue
  }

}
