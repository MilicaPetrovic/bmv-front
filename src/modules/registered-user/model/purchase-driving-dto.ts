export interface PurchaseDrivingDTO {
    drivingId: number
    passengersUsernames: string[]
    driver: string
  }