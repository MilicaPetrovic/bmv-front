export interface RejectDrivingDTO {
    drivingId: number
    reason: string
}