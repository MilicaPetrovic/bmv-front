export class VehicleTypeDto {
  id: number
  name: string
  price: number
}
