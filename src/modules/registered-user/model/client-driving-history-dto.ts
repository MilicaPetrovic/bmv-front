export interface ClientDrivingHistoryDto {
  drivingId: number
  departure: string
  arrival: string
  price: number
  startDateTime: number[]
  endDateTime: number[]
  orderTime: number[]
}
