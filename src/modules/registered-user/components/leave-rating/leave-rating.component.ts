import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ClientDrivingRatingDto} from "../../model/client-driving-rating-dto";
import {RatingService} from "../../services/rating/rating.service";

@Component({
  selector: 'app-leave-rating',
  templateUrl: './leave-rating.component.html',
  styleUrls: ['./leave-rating.component.css']
})
export class LeaveRatingComponent {

  rating : ClientDrivingRatingDto = {
    driverRating: 0,
    drivingId: this.drivingId,
    text: '',
    vehicleRating:0
  }
  constructor(
    private ratingService: RatingService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LeaveRatingComponent>,
    @Inject(MAT_DIALOG_DATA) public drivingId: number) { }


  setDriverRating(rating: number): void {
    this.rating.driverRating = rating;
  }
  setVehicleRating(rating: number): void {
    this.rating.vehicleRating = rating;
  }

  leaveRating(): void{
    this.ratingService.saveRating(this.rating).subscribe(
      res =>{
        console.log("**************************************");
        console.log(res);
        this.dialogRef.close(res);
      }
    )
  }

}
