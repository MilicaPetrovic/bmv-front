import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DrivingService} from "../../services/driving/driving.service";
import {ClientDetailedDrivingDto} from "../../model/client-detailed-driving-dto";
import {LeaveRatingComponent} from "../leave-rating/leave-rating.component";
import {MessageService} from "primeng/api";
import {DrivingDto} from "../../../shared/models/driving-dto";
import {RouteData} from "../../../shared/models/route-data";
import {GlobalService} from "../../services/global-service/global.service";
import { Router} from '@angular/router'

@Component({
  selector: 'app-client-driving-detailed',
  templateUrl: './client-driving-detailed.component.html',
  styleUrls: ['./client-driving-detailed.component.css'],
  providers: [MessageService]
})
export class ClientDrivingDetailedComponent implements OnInit {

  driving: ClientDetailedDrivingDto

  constructor(
    private readonly drivingService: DrivingService,
    private readonly messageService: MessageService,
    private readonly globalService : GlobalService,
    private readonly router: Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ClientDrivingDetailedComponent>,
    @Inject(MAT_DIALOG_DATA) public drivingId: number) {
  }

  ngOnInit(): void {
    this.setDriving();
  }

  leaveRating(): void {
    const dialogRef = this.dialog.open(LeaveRatingComponent, {
      width: '40%',
      height: '65%',
      data: this.drivingId
    })
    dialogRef.afterClosed().subscribe(res => {

      if (res.successful) {
        this.messageService.add({key: 'rating-message', severity: 'success', summary: 'Uspešno sačuvana recenzija'})
        this.setDriving();
      } else {
        switch (res.exceptionCode) {
          case 1: {//NoUserFoundException
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije pronađen korisnik'
            })
            break;
          }
          case 5: {//NotInTimeException
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Ups niste dali recenziju na vreme'
            })
            break;
          }
          case 9: {//CostumNotFoundException
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije pronađen'
            })
            break;
          }
          case 7: {//NotValidException
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije validna recenzija'
            })
            break;
          }
          case 4: {//AlreadyInUse
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Korišćen'
            })
            break;
          }
          case 8: {//NoMatchingException
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Morate da date i ocene da bismo sačuvali recenziju'
            })
            break;
          }
          default: {
            this.messageService.add({
              key: 'rating-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nešsto se desilo'
            })
          }
        }
      }

    })
  }

  toggleFavourite() : void {
    this.drivingService.toggleRouteAsFavouriteByDrivingId(this.drivingId).subscribe(
      res => {
        console.log(res);
        if (res.successful) {
          this.messageService.add({
            key: 'rating-message',
            severity: 'success',
            summary: 'Uspešno',
            detail: 'Uspešno promenjeno'
          })
          this.driving.favourite = !this.driving.favourite
        } else {
          this.messageService.add({
            key: 'rating-message',
            severity: 'error',
            summary: 'Nespešno',
            detail: 'Nismo uspeli da promenimo'
          })

        }
      },
      err => {
        console.log(err);
      }
    )
  }

  private setDriving() {

    this.drivingService.getDriving(this.drivingId).subscribe(
      res => {
        this.driving = res
      },
      err => {
        console.log('FAIL')
        console.log(err)
      }
    )
  }

  orderDriving(): void {
    this.drivingService.getRouteByDrivingId(this.drivingId).subscribe(res=>{
      const routeData = new RouteData(res.coordinates, res.totalTime, res.totalDistance)
      routeData.price = res.price
      const drivingDto = new DrivingDto(res.stations, routeData, true)
      this.globalService.setDrivingDto(drivingDto);
      this.router.navigate(['/registered/step/additional'])
    });
  }
}
