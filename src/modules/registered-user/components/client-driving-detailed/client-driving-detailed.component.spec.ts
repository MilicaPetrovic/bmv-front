import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDrivingDetailedComponent } from './client-driving-detailed.component';

describe('ClientDrivingDetailedComponent', () => {
  let component: ClientDrivingDetailedComponent;
  let fixture: ComponentFixture<ClientDrivingDetailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientDrivingDetailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientDrivingDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
