import { Component, OnInit, OnDestroy } from '@angular/core'
import {MenuItem, MessageService} from 'primeng/api'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-steps-demo',
  templateUrl: './steps-demo.component.html',
  styleUrls: ['./steps-demo.component.css'],
  providers: [MessageService]
})
export class StepsDemoComponent implements OnInit, OnDestroy {
  items: MenuItem[]
  subscription: Subscription
  index = 1;

  constructor (private messageService :MessageService) {}

  ngOnInit (): void {
    this.items = [{
      label: 'Izbor putanje',
      command: (event: any) => {
        this.index = 1;
     //   this.messageService.add({severity:'info', summary:'First Step', detail: event.item.label});
      },
      routerLink: '/registered/step/order',
    },
    {
      label: 'Dodavanje saputnika i dodatnih usluga',
      command: (event: any) => {
        this.index = 2;
  //      this.messageService.add({severity:'info', summary:'Second Step', detail: event.item.label});
      },
      routerLink: '/registered/step/additional',

    },
    {
      label: 'Plaćanje',
      command: (event: any) => {
        this.index = 3;
 //       this.messageService.add({severity:'info', summary:'Third Step', detail: event.item.label});
      },
      routerLink: '/registered/step/payment'
    }
    ]

    //  this.subscription = this.ticketService.paymentComplete$.subscribe((personalInformation) =>{
    //  this.messageService.add({severity:'success', summary:'Order submitted', detail: 'Dear, ' + personalInformation.firstname + ' ' + personalInformation.lastname + ' your order completed.'});
  }

  ngOnDestroy () : void {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }
}
