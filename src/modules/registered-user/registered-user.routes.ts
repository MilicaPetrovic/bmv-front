import {ClientDrivingHistoryComponent} from "./pages/client-driving-history/client-driving-history.component";
import {Routes} from '@angular/router'
import {RoleGuard} from '../auth/guards/role/role.guard'
import {RegisteredUserComponent} from './pages/registered-user/registered-user.component'
import {AccountVerificationComponent} from './pages/account-verification/account-verification.component'
import {
  ChoseFriendsAdditionalServicesComponent
} from "./pages/chose-friends-additional-services/chose-friends-additional-services.component";
import {StepsDemoComponent} from "./components/stap-order/steps-demo.component";
import {DrivingPaymentComponent} from "./pages/driving-payment/driving-payment.component";
import {UserSimulationComponent} from "./pages/user-simulation/user-simulation.component";


export const RegisteredUserRoutes: Routes = [
  /*{
    path: 'order/driving',
    pathMatch: 'full',
    component: RegisteredUserComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'registeredUser' }
  },*/
  {
    path: 'verify/:username/:code',
    component: AccountVerificationComponent
  },
  {
    path: 'driving/history',
    component: ClientDrivingHistoryComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'registeredUser'}
  },
  {
    path: 'step',
    //pathMatch: 'full',
    component: StepsDemoComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'registeredUser'},
    children: [
      {path: '', pathMatch: 'full', redirectTo:'order'},
      {
        path: 'order',
        component :RegisteredUserComponent
      },
      {
        path: 'additional',
        component: ChoseFriendsAdditionalServicesComponent
      },
      {
        path: 'payment/:drivingId',
        component: DrivingPaymentComponent
      }
    ]
  },
  {
    path: 'simulation',
    component: UserSimulationComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'registeredUser'}
  }
]
