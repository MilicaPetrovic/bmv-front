import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AccountVerificationComponent } from './pages/account-verification/account-verification.component'
import { RegisteredUserComponent } from './pages/registered-user/registered-user.component'
import { AccountVerificationService } from './services/account-verification/account-verification.service'
import { DrivingService } from './services/driving/driving.service'
import { VehicleTypeService } from './services/vehicle-type/vehicle-type.service'
import { UserDataService } from './services/user-service/user.service'
import { RouterModule} from '@angular/router'
import { RegisteredUserRoutes } from './registered-user.routes'
import { ToastModule } from 'primeng/toast'
import { TreeSelectModule } from 'primeng/treeselect'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { MapModule } from '../map/map.module'
import { AutoCompleteModule } from 'primeng/autocomplete'
import { CheckboxModule } from 'primeng/checkbox'
import { RadioButtonModule } from 'primeng/radiobutton'
import { ButtonModule } from 'primeng/button'
import { StepsDemoComponent } from './components/stap-order/steps-demo.component'
import { StepsModule } from 'primeng/steps'
import { CardModule } from 'primeng/card';
import { ClientDrivingHistoryComponent } from './pages/client-driving-history/client-driving-history.component'
import {TableModule} from "primeng/table";
import {SharedModule} from "../shared/shared.module";
import { ClientDrivingDetailedComponent } from './components/client-driving-detailed/client-driving-detailed.component';
import { LeaveRatingComponent } from './components/leave-rating/leave-rating.component';
import {InputTextareaModule} from "primeng/inputtextarea";
import {MatDialogModule} from "@angular/material/dialog";
import { ChoseFriendsAdditionalServicesComponent } from './pages/chose-friends-additional-services/chose-friends-additional-services.component'
import {GlobalService} from "./services/global-service/global.service";
import { DrivingPaymentComponent } from './pages/driving-payment/driving-payment.component';
import {BadgeModule} from "primeng/badge";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatChipsModule} from "@angular/material/chips";
import {NgxStarsModule} from "ngx-stars";
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from "@angular/material/input";
import { UserSimulationComponent } from './pages/user-simulation/user-simulation.component';
import {MatSelectModule} from "@angular/material/select";
import {NgbTimepickerModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    AccountVerificationComponent,
    RegisteredUserComponent,
    StepsDemoComponent,
    ClientDrivingHistoryComponent,
    ClientDrivingDetailedComponent,
    LeaveRatingComponent,
    ChoseFriendsAdditionalServicesComponent,
    DrivingPaymentComponent,
    UserSimulationComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(RegisteredUserRoutes),
        ToastModule,
        TreeSelectModule,
        FormsModule,
        MapModule,
        AutoCompleteModule,
        CheckboxModule,
        RadioButtonModule,
        ButtonModule,
        StepsModule,
        CardModule,
        TableModule,
        SharedModule,
        InputTextareaModule,
        MatDialogModule,
        BadgeModule,
        MatIconModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatFormFieldModule,
        NgxStarsModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        NgbTimepickerModule
    ],
  providers: [
    AccountVerificationService,
    DrivingService,
    VehicleTypeService,
    UserDataService,
    GlobalService
  ],
  exports:[RouterModule]
})
export class RegisteredUserModule { }
