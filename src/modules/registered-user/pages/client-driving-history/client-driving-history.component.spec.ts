import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDrivingHistoryComponent } from './client-driving-history.component';

describe('ClientDrivingHistoryComponent', () => {
  let component: ClientDrivingHistoryComponent;
  let fixture: ComponentFixture<ClientDrivingHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientDrivingHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientDrivingHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
