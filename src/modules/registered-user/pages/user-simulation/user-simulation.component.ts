import {Component, OnInit} from '@angular/core';
import {DrivingService} from "../../../driver/services/driving/driving.service";
import {MessageService} from "primeng/api";
import {AuthService} from "../../../auth/services/auth/auth.service";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import {ReportService} from "../../../reports/services/report.service";
import {InconsistentReportDto} from "../../../shared/models/inconsistent-report-dto";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {StompClient} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-user-simulation',
  templateUrl: './user-simulation.component.html',
  styleUrls: ['./user-simulation.component.css'],
  providers: [MessageService]
})
export class UserSimulationComponent implements OnInit {

  isButtonReportDisabled = true
  private stompClient: StompClient;
  report: InconsistentReportDto;
  timeToCome:number;
  timeText = ""
  isReported = false

  constructor(private drivingService: DrivingService,
              private messageService: MessageService,
              private authService: AuthService,
              private reportService: ReportService) {
  }

  ngOnInit(): void {
    this.initializeWebSocketConnection();
  }


  initializeWebSocketConnection():void {
    const ws = new SockJS('https://localhost:8080/uber/socket');
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => {
      this.openGlobalSocket();
    });
  }

  private openGlobalSocket():void {
    this.stompClient.subscribe('/topic/map-updates/enable-report-driving/' + this.authService.getUsername(), (message: { body: string }) => {
      console.log("prijava za nekonzistentnost u nastavki driver Id: ")
      console.log(message.body)
      this.report = JSON.parse(message.body)
      if (!this.isReported) {
        this.isButtonReportDisabled = false
      }
    });

    this.stompClient.subscribe('/topic/map-updates/update-time/'+this.authService.getUsername(), (message: { body: string }) => {
      const newTime = Number(message.body);
      if(newTime === 0){
        this.timeText = "Vozilo će uskoro stići."
      }
      else if(this.timeToCome - newTime < 1){
        this.timeText = "Vozilo će stići za " + this.timeToCome + " minuta.";
      }else{
        this.timeToCome = Number(message.body);
        this.timeText = "Vozilo će stići za " + this.timeToCome + " minuta.";
      }
    });
  }

  reportInconsistency():void {
    console.log("report&&&&&&&&")
    console.log(this.report);
    this.reportService.saveInconsistencyReport(this.report).subscribe(res => {
      this.showReportResponse(res)
      this.isButtonReportDisabled = true
      this.isReported = true
    });
  }

  showReportResponse(res: StatusResponseDto):void {
    if (res.successful) {
      this.messageService.add({
        key: 'user-report-inconsistency',
        severity: 'success',
        summary: 'Uspešno',
        detail: 'Prijavili ste nekonzistentnu vožnju'
      })
    } else if (res.exceptionCode === 1) {
      this.messageService.add({
        key: 'user-report-inconsistency',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Nismo uspeli da prijavimo vožnju'
      })
    }
  }
}
