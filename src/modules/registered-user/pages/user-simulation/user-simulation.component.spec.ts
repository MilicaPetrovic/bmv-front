import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSimulationComponent } from './user-simulation.component';

describe('UserSimulationComponent', () => {
  let component: UserSimulationComponent;
  let fixture: ComponentFixture<UserSimulationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSimulationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserSimulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
