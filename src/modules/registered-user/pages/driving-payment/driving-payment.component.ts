import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { PassengerDto } from 'src/modules/admin/models/detailed-driving-dto';
import { PurchaseDrivingDTO } from '../../model/purchase-driving-dto';
import { DrivingService } from '../../services/driving/driving.service';


@Component({
  selector: 'app-driving-payment',
  templateUrl: './driving-payment.component.html',
  styleUrls: ['./driving-payment.component.css'],
  providers: [MessageService]
})
export class DrivingPaymentComponent implements OnInit {

  drivingId: string | null;
  price: number;
  passengers: PassengerDto[]
  driver: string;

  constructor(private drivingService: DrivingService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.drivingId = this.route.snapshot.paramMap.get('drivingId');
    if (this.drivingId !== null)
      this.drivingService.getDriving(parseInt(this.drivingId)).subscribe(
        res => {
          this.price = res.price
          this.passengers = res.passengers
          this.driver = res.driver.username
        }
      )
  }

  purchaseDriving(): void {
    const passengersUsernames = this.getPassengersUsernames();
    if (this.drivingId !== null) {
      const dto = this.getPurchaseDTO(this.drivingId, passengersUsernames);
      this.drivingService.purchaseDriving(dto).subscribe(
        res => {
          if (res.successful) {
            this.messageService.add({ key: 'payment-message', severity: 'success', summary: 'Uspešno plaćanje vožnje', detail: 'Vožnja je uspešno plaćena.', life: 10000 })
          }else{
            if (res.exceptionCode === 1) { this.messageService.add({ key: 'payment-message', severity: 'error', summary: 'Neuspešna narudžbina vožnje', detail: 'Nismo uspeli da naručimo vožnju za Vas.\nVaš nalog ne postoji u sistemu.', life: 5000 }) }
            else if (res.exceptionCode === 15) { this.messageService.add({ key: 'payment-message', severity: 'error', summary: 'Neuspešna narudžbina vožnje', detail: res.exceptionMessage, life: 5000 }) }
            else if (res.exceptionCode === 16) { this.messageService.add({ key: 'payment-message', severity: 'error', summary: 'Neuspešna narudžbina vožnje', detail: res.exceptionMessage, life: 5000 }); return; }
            else { this.messageService.add({ key: 'payment-message', severity: 'error', summary: 'Neuspešno', detail: 'Nismo uspeli da naručimo vožnju za Vas.\nMolimo Vas da se prijavite pa potom poručite vožnju.', life: 5000 }) }
          }
          setTimeout(() => {
            this.router.navigate(['/registered/step/order']);
          }, 2000);
        }
      )
    }
  }

  private getPurchaseDTO(drivingId: string, passengersUsernames: string[]) {
    const dto: PurchaseDrivingDTO = {
      drivingId: parseInt(drivingId),
      passengersUsernames: passengersUsernames,
      driver: this.driver
    }
    return dto;
  }

  private getPassengersUsernames(): string[] {
    const list: string[] = []
    for (const passenger of this.passengers) {
      list.push(passenger.username)
    }
    return list;
  }

}
