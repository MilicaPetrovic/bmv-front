import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingPaymentComponent } from './driving-payment.component';

describe('DrivingPaymentComponent', () => {
  let component: DrivingPaymentComponent;
  let fixture: ComponentFixture<DrivingPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingPaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DrivingPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
