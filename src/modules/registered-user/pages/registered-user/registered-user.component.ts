import {Component, OnInit} from '@angular/core'
import { DrivingDto } from '../../../shared/models/driving-dto'
import { TreeNodeSearch } from '../../../shared/models/tree-node-search'
import { DrivingService } from '../../services/driving/driving.service'
import { MessageService } from 'primeng/api'
import { Router} from '@angular/router'
import {GlobalService} from "../../services/global-service/global.service";
import {COLORS, COLORS_SERBIAN} from "../../../map/app.constants";
import {RouteViewDto} from "../../../shared/models/route-view-dto";
import {FavouriteRoutes} from "../../model/favourite-routes";
import {RouteData} from "../../../shared/models/route-data";
import {LatLng} from "leaflet";

@Component({
  selector: 'app-registered-user',
  templateUrl: './registered-user.component.html',
  providers: [MessageService],
  styleUrls: ['./registered-user.component.css']
})

export class RegisteredUserComponent implements OnInit {
  nodes = [new TreeNodeSearch('po vremenu', 'pi pi-clock', 'pi pi-folder-open'),
    new TreeNodeSearch('po razdaljini', 'pi pi-compass', 'pi pi-folder-open'),
    new TreeNodeSearch('po ceni', 'pi pi-wallet', 'pi pi-folder-open'),
    new TreeNodeSearch('ručno', 'pi pi-thumbs-up', 'pi pi-folder-open')]

  selectedOptimal: TreeNodeSearch
  chosenDriving: DrivingDto
  submitted = false
  chosenRouteColor:string=""
  isFavouriteRoute: boolean = false;

  selectedValue: string;
  selectedCar: string;

  favouriteRoutes: FavouriteRoutes[] = [];
  nodesFavourite :TreeNodeSearch[] = []
  selectedFavourite: TreeNodeSearch;
  favouriteRoute : RouteViewDto


  constructor (
    private readonly globalService: GlobalService,
    private readonly drivingService: DrivingService,
    private readonly messageService:MessageService,

    private readonly router: Router) {
    console.log('PHU')
  }

  ngOnInit(): void {
    this.drivingService.getFavouriteRoutes().subscribe(data=>{
      console.log(data)
      for(const route of data){
        const r = new FavouriteRoutes(route, this.getNameForRoute(route));
        this.favouriteRoutes.push(r)
      }
      for(const route of this.favouriteRoutes){
        this.nodesFavourite.push(new TreeNodeSearch(route.viewValue, "", ""))
      }
    });
  }

  public getOptimalRoute (drivingDto: DrivingDto): void {
    this.chosenDriving = drivingDto
    this.setChosenRouteColor();
  }

  private setChosenRouteColor(){
    const index = COLORS.indexOf(<string>this.chosenDriving.chosenRoute?.color)
    console.log(index)
    if(index >= 0){
      this.chosenRouteColor = COLORS_SERBIAN[index];
    }
    console.log("bojaaa: ", this.chosenRouteColor);
  }

  nextPage(): void {
    if(this.selectedFavourite !== undefined){
      this.setFavouriteRouteAsNewDriving()
      this.router.navigate(['/registered/step/additional'])
      this.submitted = true
    }
    else if(this.chosenDriving===undefined){
      this.messageService.add({ key: 'driving-order-message', severity: 'warn', summary: 'Niste izabrali putanju', detail: 'Prvo izaberite putanju da biste prešli na sledeći korak.', life: 5000 });
    }
    else {
      this.chosenDriving.favouriteRoute = this.isFavouriteRoute
      this.globalService.setDrivingDto(this.chosenDriving);
      this.router.navigate(['/registered/step/additional'])
      this.submitted = true
    }
  }

  resetColor(): void {
    this.chosenRouteColor='';
    this.router.navigateByUrl('/RefreshComponent', {skipLocationChange: true}).then(() => {
      this.router.navigate(['registered/step/order']);
    });
  }

  private getNameForRoute(route: RouteViewDto):string {
    let name = "";
    for(let i = 0 ; i < route.stations.length; i++){
      if( route.stations.length - i === 1){
        name += route.stations[i].name
      }else {
        name += route.stations[i].name + " - "
      }
    }
    return name;
  }

  private setFavouriteRouteAsNewDriving() {

    const route = this.findSelectedFavourite()
    const routeData = new RouteData(route.value.coordinates, route.value.totalTime, route.value.totalDistance)
    routeData.price = route.value.price
    const drivingDto = new DrivingDto(route.value.stations, routeData, true)
    this.globalService.setDrivingDto(drivingDto);
  }

  private findSelectedFavourite() :FavouriteRoutes{
    for (const route of this.favouriteRoutes) {
      if (route.viewValue === this.selectedFavourite.label) {
        return route;
      }
    }
    return this.favouriteRoutes[0]   // ovo je kriticnoo
  }

  fovouriteRoutesIsClicked():LatLng[] {
    const route = this.findSelectedFavourite()
    this.favouriteRoute = route.value
    this.chosenRouteColor="crvena"
    return route.value.coordinates
  }
}
