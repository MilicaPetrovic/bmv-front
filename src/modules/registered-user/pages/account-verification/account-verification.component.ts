import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AccountVerificationService } from '../../services/account-verification/account-verification.service'
import { MessageService } from 'primeng/api'
import { AuthService } from '../../../auth/services/auth/auth.service'

@Component({
  selector: 'app-account-verification',
  templateUrl: './account-verification.component.html',
  styleUrls: ['./account-verification.component.css'],
  providers: [MessageService]
})
export class AccountVerificationComponent {
  username = ''
  code = ''

  constructor (
    private readonly route: ActivatedRoute,
    private readonly accountVrificationSerivce: AccountVerificationService,
    private readonly messageService: MessageService,
    private readonly userService: AuthService,
    private readonly router: Router) {
    this.route.params.subscribe(
      params => {
        this.username = params['username']
        this.code = params['code']
        console.log(params)
        accountVrificationSerivce.verify(this.username, this.code).subscribe(
          (res) => {
            this.userService.setUser(res.accessToken)
            this.userService.setExpiration(res.expiresIn)
            setTimeout(() => {
              this.router.navigateByUrl('/registered/step')
            }, 2000)
            this.messageService.add({ key: 'verification-message', severity: 'success', summary: 'Uspešna verifikacija', detail: 'Uživajte u korišćenju naše aplikacije.' })
          },
          err => {
            if (err.status === 404) { this.messageService.add({ key: 'verification-message', severity: 'error', summary: 'Neuspešna verifikacija', detail: 'Nismo uspeli da verifikujemo nalog.\nMolimo Vas preko mejla verifikujte Vaš nalog.', life: 5000 }) } else { this.messageService.add({ key: 'verification-message', severity: 'error', summary: 'Neuspešna registracija', detail: 'Nismo uspeli da verifikujemo nalog.\nGreška se desilo.', life: 5000 }) }
          }
          // this.messageService.add({key: 'verification-message', severity:'error', summary:'Neuspešna verifikacija', detail:'Nismo uspeli da verifikujemo nalog.\nVerifikacioni kod nije validan.', life: 5000});
        )
      }
    )
  }
}
