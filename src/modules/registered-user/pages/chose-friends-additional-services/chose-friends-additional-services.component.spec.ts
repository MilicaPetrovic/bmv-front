import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoseFriendsAdditionalServicesComponent } from './chose-friends-additional-services.component';

describe('ChoseFriendsAdditionalServicesComponent', () => {
  let component: ChoseFriendsAdditionalServicesComponent;
  let fixture: ComponentFixture<ChoseFriendsAdditionalServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoseFriendsAdditionalServicesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChoseFriendsAdditionalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
