import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {VehicleTypeDto} from "../../model/vehicle-type-dto";
import {DrivingDto} from "../../../shared/models/driving-dto";
import {GlobalService} from "../../services/global-service/global.service";
import {VehicleTypeService} from "../../services/vehicle-type/vehicle-type.service";
import {UserDataService} from "../../services/user-service/user.service";
import {Router} from "@angular/router";
import {MessageService} from "primeng/api";
import {DrivingService} from "../../services/driving/driving.service";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-chose-friends-additional-services',
  templateUrl: './chose-friends-additional-services.component.html',
  styleUrls: ['./chose-friends-additional-services.component.css'],
  providers: [MessageService]
})
export class ChoseFriendsAdditionalServicesComponent implements OnInit {
  selectedVehicle: VehicleTypeDto
  vehicleTypes: VehicleTypeDto[]
  chosenDriving: DrivingDto
  babyChecked: [] = []
  petChecked: [] = []
  answer: StatusResponseDto

  separatorKeysCodes: number[] = [ENTER, COMMA];
  currentUsernameForSplitFair = new FormControl('');
  filteredUsernames: Observable<string[]>;
  splitFairUsernames: string[] = [];
  allUsernames: string[] = [];

  @ViewChild('usernameInput') usernameInput: ElementRef<HTMLInputElement>;
  drivingPriceForBadge: number;
  time = {hour: 0, minute: 0};

  constructor(private globalService: GlobalService,
              private vehicleService: VehicleTypeService,
              private userDataService: UserDataService,
              private router: Router,
              private messageService: MessageService,
              private drivingService: DrivingService) {

    this.chosenDriving = globalService.getDrivingDto();
    this.drivingPriceForBadge = this.chosenDriving.price

    this.filteredUsernames = this.currentUsernameForSplitFair.valueChanges.pipe(
      startWith(null),
      map((username: string | null) => (username ? this._filter(username) : this.allUsernames.slice())),
    );
  }

  ngOnInit(): void {
    this.vehicleService.getAllVehicleTypes().subscribe(data => {
      this.vehicleTypes = data
      console.log(this.vehicleTypes)
      this.selectedVehicle = this.vehicleTypes[0]
    })

    this.userDataService.getAllUsernamesForSplitFair().subscribe(data => {
      this.allUsernames = data
    })

  }

  checkValidTimeForFutureDriving(): boolean {
    const now = new Date();
    const hours = now.getHours();
    const minutes = now.getMinutes();

    const futureMinutes = this.time.hour * 60 + this.time.minute
    const currentMinutes = hours * 60 + minutes;

    if (futureMinutes < currentMinutes) {
      return false
    } else if (futureMinutes - currentMinutes > 300) {
      return false
    }else if(futureMinutes - currentMinutes < 30){
      return false;
    }
    else {
      return true
    }
  }

  isFutureDriving(): boolean {
    if (this.time.hour !== 0 || this.time.minute !== 0) {
      return true
    }
    return false
  }

  private setFutureIfFuture(): boolean {
    if (this.isFutureDriving()) {
      if (this.checkValidTimeForFutureDriving()) {
        this.chosenDriving.future = true
        this.chosenDriving.hour = this.time.hour;
        this.chosenDriving.minute = this.time.minute
        return true;
      } else {
        this.showInvalidTimeMessage()
        return false;
      }
    }
    else {
      this.chosenDriving.future = false
      return true
    }
  }

  setChosenData(): void {
    console.log(this.babyChecked)
    if (this.babyChecked.length > 0) {
      this.chosenDriving.babyFriendly = true
    }
    if (this.petChecked.length > 0) {
      this.chosenDriving.petFriendly = true
    }
    this.chosenDriving.vehicleTypeId = this.selectedVehicle.id
    this.chosenDriving.linkedUsers = this.splitFairUsernames;
    this.chosenDriving.price = this.chosenDriving.price + this.selectedVehicle.price;

    if (this.setFutureIfFuture()) {
      this.globalService.setDrivingDto(this.chosenDriving);
      console.log("set chosing data")
      console.log(this.chosenDriving)
      this.saveNewOrder();
    }
  }

  private saveNewOrder() {
    console.log("futureeeeeeeeeeeeeeeeee ")
    console.log(this.chosenDriving.future)
    this.drivingService.saveDriving(this.chosenDriving).subscribe(data => {
      this.answer = data;
      this.checkAnswer();
    })

  }

  private checkAnswer() {
    if (this.answer.successful) {
      this.nextPage(this.answer.exceptionMessage);
    } else {
      if (this.answer.exceptionCode == 1) {
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešna narudžbina vožnje',
          detail: 'Nismo uspeli da naručimo vožnju za Vas.\nVaš nalog ne postoji u sistemu.',
          life: 5000
        })
      } else if (this.answer.exceptionCode == 2) {
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešna narudžbina vožnje XXX',
          detail: 'Nismo uspeli da naručimo vožnju za Vas.\nMolimo Vas da se prijavite pa potom poručite vožnju.',
          life: 5000
        })
      } else if (this.answer.exceptionCode == 14) {
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešna narudžbina vožnje',
          detail: 'Nismo uspeli da naručimo vožnju za Vas.\nVi ili jedan od drugara već ima poručeno ili aktivno vožnje.',
          life: 5000
        })
      } else if (this.answer.exceptionCode == 17) {
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešna narudžbina vožnje',
          detail: 'Nismo uspeli da naručimo vožnju za Vas.\nNemamo vozača na raspolaganju.',
          life: 5000
        })
      }
      else if(this.answer.exceptionCode == 15){
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešna narudžbina vožnje',
          detail: 'Neko od putnika nema dovoljno tokena za ovu vožnju. Molimo kupite tokene.',
          life: 5000
        });
      }
      else {
        this.messageService.add({
          key: 'driving-additional-message',
          severity: 'error',
          summary: 'Neuspešno',
          detail: 'Nismo uspeli da naručimo vožnju za Vas.\nMolimo Vas da se prijavite pa potom poručite vožnju.',
          life: 5000
        })
      }
    }
  }

  private showInvalidTimeMessage(): void {
    this.messageService.add(
      {
        key: 'driving-additional-message',
        severity: 'error',
        summary: 'Neodgovarajuće vreme',
        detail: 'Buduću vožnju je moguće naručiti najviše 5 sati unapred, a najmanje pola sata.',
        life: 5000
      });
  }

  existInAllUsernames(value: string): boolean {
    for (const username of this.allUsernames) {
      if (username === value) {
        return true;
      }
    }
    return false;
  }

  existInSplitFairUsernames(value: string): boolean {
    for (const username of this.splitFairUsernames) {
      if (username === value) {
        return true;
      }
    }
    return false;
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value && this.existInAllUsernames(value) && !this.existInSplitFairUsernames(value)) {
      this.splitFairUsernames.push(value);
      const index = this.allUsernames.indexOf(value);
      this.allUsernames.slice(index);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.currentUsernameForSplitFair.setValue(null);
  }

  remove(username: string): void {
    const index = this.splitFairUsernames.indexOf(username);

    if (index >= 0) {
      this.splitFairUsernames.splice(index, 1);
      this.allUsernames.push(username);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.existInAllUsernames(event.option.viewValue) && !this.existInSplitFairUsernames(event.option.viewValue)) {
      this.splitFairUsernames.push(event.option.viewValue);
      this.usernameInput.nativeElement.value = '';
      this.currentUsernameForSplitFair.setValue(null);
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allUsernames.filter(username => username.toLowerCase().includes(filterValue));
  }


  prevPage(): void {
    //  this.router.navigate(['registered/step/order']);
    this.router.navigateByUrl('/RefreshComponent', {skipLocationChange: true}).then(() => {
      this.router.navigate(['registered/step/order']);
    });
  }

  nextPage(drivingId: string): void {
    this.router.navigate([`registered/step/payment/${drivingId}`])
  }
}



