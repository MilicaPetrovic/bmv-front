import { NgModule } from '@angular/core'

import { AppComponent } from './app.component'
import { NotFoundComponent } from './pages/not-found/not-found.component'

import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module'
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { BrowserModule } from '@angular/platform-browser'
import { SimpleHeaderComponent } from './components/simple-header/simple-header.component'
import { RegisteredHeaderComponent } from './components/registered-header/registered-header.component'
import { DriverHeaderComponent } from './components/driver-header/driver-header.component'

import { ChatComponent } from './components/chat/chat.component'
import { NgChatModule } from 'ng-chat'
import { AdminChatComponent } from './components/admin-chat/admin-chat.component'
import { ChatWebsocketService } from './services/websocket/chat-websocket.service'
import { AdminModule } from '../admin/admin.module'
import { RootComponent } from './pages/root/root.component'
import { AuthModule } from '../auth/auth.module'
import { MapModule } from '../map/map.module'
import { ProfileModule } from '../profile/profile.module'
import { RegisteredUserModule } from '../registered-user/registered-user.module'
import { AdminHeaderComponent } from './components/admin-header/admin-header.component'
import { TooltipModule } from 'primeng/tooltip'
import { SidebarModule } from 'primeng/sidebar'
import { SharedModule } from '../shared/shared.module'
import {DriverModule} from "../driver/driver.module";
import {ToastModule} from "primeng/toast";
import { SplitFareApprovalDialogComponent } from './components/split-fare-approval-dialog/split-fare-approval-dialog.component';
import {ButtonModule} from "primeng/button";
import {MatDialogModule} from "@angular/material/dialog";
import { DriverNotificationsComponent } from './components/driver-notifications/driver-notifications.component';
import { SingleDriverNotificationComponent } from './components/single-driver-notification/single-driver-notification.component';
import { DrivingApprovementDialogComponent } from './components/driving-approvement-dialog/driving-approvement-dialog.component';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {ReportsModule} from "../reports/reports.module";
import {ReactiveFormsModule} from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login'

@NgModule({
  declarations: [ // ovde dodajemo svaku novokreiranu komponentu
    AppComponent,
    NotFoundComponent,
    SimpleHeaderComponent,
    RegisteredHeaderComponent,
    DriverHeaderComponent,
    RootComponent,
    AdminHeaderComponent,
    ChatComponent,
    AdminChatComponent,
    SplitFareApprovalDialogComponent,
    DriverNotificationsComponent,
    SingleDriverNotificationComponent,
    DrivingApprovementDialogComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      NoopAnimationsModule,
      NgChatModule,
      AuthModule,
      MapModule,
      ProfileModule,
      RegisteredUserModule,
      AdminModule,
      MatButtonModule,
      TooltipModule,
      SidebarModule,
      SharedModule,
      DriverModule,
      ToastModule,
      ButtonModule,
      MatDialogModule,
      MatCardModule,
      MatInputModule,
      ReportsModule,
      ReactiveFormsModule,
      NgbModule,
      RouterModule,
      SocialLoginModule
  ],

  providers: [ // ovde navodimo za dependency injection
    ChatWebsocketService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '1046024947775-hl1hc0cupsjr0bg9i2cglftm2kebgm17.apps.googleusercontent.com'
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('538119038197957')
          }
        ],
        onError: (err) => {
          console.error(err)
        }
      } as SocialAuthServiceConfig
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
