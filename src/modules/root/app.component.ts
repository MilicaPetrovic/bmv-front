import { Component, OnInit } from '@angular/core'
import { PrimeNGConfig } from 'primeng/api'

@Component({
  selector: 'app-root', // pogledati index.html - tamo je u body sekciji upravo app-root
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Uber'
  constructor (private readonly primengConfig: PrimeNGConfig) {}

  ngOnInit () : void {
    this.primengConfig.ripple = true
  }
}
