import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { take } from 'rxjs';

declare let google: any;

@Directive({
    // eslint-disable-next-line @angular-eslint/directive-selector
    selector: 'google-signin-button'
})
export class GoogleSigninButtonDirective implements OnInit {

    constructor(private el: ElementRef, private socialAuthService: SocialAuthService) {
    }

    ngOnInit(): void {
        this.socialAuthService.initState.pipe(take(1)).subscribe(() => {
            google.accounts.id.renderButton(this.el.nativeElement, {
                type: 'standard',
                size: 'medium',
                text: 'signin_with',
                theme: 'filled_blue'
            });
        });
    }
}
