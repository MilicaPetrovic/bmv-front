import {
  ChatAdapter,
  ChatParticipantStatus,
  ChatParticipantType,
  IChatParticipant,
  Message,
  ParticipantResponse
} from 'ng-chat'
import { Observable, of } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { FriendsListElement } from '../../model/friends-list-element'
import { environment } from '../../../../environments/environment'
import {DomSanitizer} from "@angular/platform-browser";
import {UserService} from "../../../profile/services/user/user-service.service";

@Injectable({
  providedIn: 'root'
})
export class MyChatAdapter extends ChatAdapter {
  private readonly API_PATH = environment.api_path + 'chat'

  constructor (private readonly httpClient: HttpClient,
          private readonly sanitizer: DomSanitizer,
               private userService: UserService) {
    super()
  }

  mockedParticipants: IChatParticipant[] = []

  listFriends (): Observable<ParticipantResponse[]> {
    this.getFriendsList().subscribe({
      next: (data) => {
        let friendsListChanged = false
        if (this.mockedParticipants.length != data.length) { friendsListChanged = true }
        const res: ParticipantResponse[] = []
        for (const u of data) {
          const user = {
            participantType: ChatParticipantType.User,
            id: u.username,
            displayName: u.displayName,
            avatar: u.profilePhoto,
            status: ChatParticipantStatus.Online
          }
          this.mockedParticipants.push(user)
          res.push(this.convertUserToParticipantResponse(user))
        }
        if (friendsListChanged) { this.onFriendsListChanged(res) }
      }
    })
    return of(this.mockedParticipants.map(user => {
      return this.convertUserToParticipantResponse(user)
    }))
  }

  convertUserToParticipantResponse (user: IChatParticipant): ParticipantResponse {
    const participantResponse = new ParticipantResponse()
    participantResponse.participant = user
    participantResponse.metadata = {
      totalUnreadMessages: 0
    }
    return participantResponse
  }

  getMessageHistory (destinataryId: string): Observable<Message[]> {
    if (destinataryId == 'admin') {
      return (this.httpClient.get<Message[]>(this.API_PATH + '/messages', {
      }))
    } else {
      return (this.httpClient.get<Message[]>(`${this.API_PATH}/messages/${destinataryId}`, {
      }))
    }
  }

  sendMessage (message: Message): void {
    console.log('SEND MESSAGE')
    console.log(message)
    if (message.fromId === 'admin') { // notify{
      console.log('ADMIN')
      this.httpClient.put(this.API_PATH + '/send/message/admin', message, {
        //headers: this.authHeader
      }).subscribe();
    } else { // notify
      this.httpClient.put(this.API_PATH+'/send/message', message, {
      }).subscribe();
      console.log('not admin')
    }
  }

  public getFriendsList (): Observable<FriendsListElement[]> {
    return this.httpClient.get<FriendsListElement[]>(`${this.API_PATH}/friends`, {
    })
  }

}
