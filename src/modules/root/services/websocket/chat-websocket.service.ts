import { Injectable } from '@angular/core'

// var SockJs = require("sockjs-client");
import * as SockJS from 'sockjs-client'
import * as Stomp from 'stompjs'
import {environment} from "../../../../environments/environment";
import {StompClient} from "../../../shared/models/stomp-client";
//const Stomp = require('stompjs')

@Injectable({
  providedIn: 'root'
})
export class ChatWebsocketService {
  private readonly API_PATH = environment.api_path

  public connect (): StompClient {
    const socket = new SockJS(this.API_PATH + 'socket')

    const stompClient = Stomp.over(socket)

    return stompClient
  }
/*
  private connecting: boolean = false;
  private topicQueue: any[] = [];

  socket = new SockJS('https://localhost:8080/uber/socket');
  stompClient = Stomp.over(this.socket);

  subscribe(topic: string, callback: any): void {
    // If stomp client is currently connecting add the topic to the queue
    if (this.connecting) {
      this.topicQueue.push({
        topic,
        callback
      });
      return;
    }

    const connected: boolean = this.stompClient.connected;
    if (connected) {
      // Once we are connected set connecting flag to false
      this.connecting = false;
      this.subscribeToTopic(topic, callback);
      return;
    }

    // If stomp client is not connected connect and subscribe to topic
    this.connecting = true;
    this.stompClient.connect({}, (): any => {
      this.subscribeToTopic(topic, callback);

      // Once we are connected loop the queue and subscribe to remaining topics from it
      this.topicQueue.forEach((item:any) => {
        this.subscribeToTopic(item.topic, item.callback);
      })

      // Once done empty the queue
      this.topicQueue = [];
    });
  }

  private subscribeToTopic(topic: string, callback: any): void {
    this.stompClient.subscribe(topic, (response?:string): any => {
      callback(response);
    });
  } */
}
