import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  getTitle(url: string): string {
    switch (url){
      case '/auth/login': return 'Ulogovanje'
      case '/auth/register': return 'Registracija'
      case '/registered/step/order': return 'Poručivanje vožnje'
      case '/registered/step/additional': return 'Poručivanje vožnje'
      case '/registered/step/payment': return 'Poručivanje vožnje'
      case '/registered/driving/history': return 'Istorija vožnja'
      case '/profile/user': return 'Profil'
      case '/home': return 'Početna stranica'
      case '/admin/users': return 'Korisnici'
      case '/admin/add-driver': return 'Dodavanje vozača'
      case '/admin/driver-requests': return 'Zahtevi vozača'
      case '/driver/driving/history': return 'Istorija vožnja'
      case '/reports/client': return 'Izveštaji'
      case '/driver/simulation': return 'Prikaz vožnje'
      case '/reports/driver': return 'Izveštaji'
      case '/reports/admin': return 'Izveštaji'
    }
    return '';
  }
}
