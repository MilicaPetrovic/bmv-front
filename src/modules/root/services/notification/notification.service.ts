import { Injectable } from '@angular/core'
import { environment } from '../../../../environments/environment'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs'
import { NotificationDto } from '../../model/notification-dto'
import {DetailedDrivingNotificationDto} from "../../model/detailed-driving-notification-dto";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import { DriverNotificationDto } from '../../model/driver-notification-dto'
import {AuthService} from "../../../auth/services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private readonly API_PATH = environment.api_path + 'notification'
  authHeader: HttpHeaders

  constructor (private readonly httpClient: HttpClient, private readonly authService: AuthService) {
    this.authHeader = authService.getAuthHeader()
  }

  getNotifications (): Observable<NotificationDto[]> {
    return this.httpClient.get<NotificationDto[]>(`${this.API_PATH}/all`)
  }

  getDetailedSplitFareNotificationInfo(notificationId: number): Observable<DetailedDrivingNotificationDto> {
    return this.httpClient.get<DetailedDrivingNotificationDto>(`${this.API_PATH}/split/fare/${notificationId}`)
  }

  confirmSplitFare(notificationId: number): Observable<StatusResponseDto>{
    return this.httpClient.get<StatusResponseDto>(`${this.API_PATH}/split/fare/confirm/${notificationId}`);
  }

  getDriverNotifications(): Observable<DriverNotificationDto[]>{
    return this.httpClient.get<DriverNotificationDto[]>(`${this.API_PATH}/driver/all`,
    {headers: this.authHeader});
  }

  getDriverNotificationInfo(notificationId: number): Observable<DriverNotificationDto> {
    return this.httpClient.get<DriverNotificationDto>(`${this.API_PATH}/driver/get/${notificationId}`,
    {headers: this.authHeader})
  }

  // getNewDriverNotification(): Observable<DriverNotificationDto>{
  //   return this.httpClient.get<DriverNotificationDto>(`${this.API_PATH}/split/fare/confirm/${notificationId}`);
  // }
}
