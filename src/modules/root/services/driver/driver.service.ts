import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  private readonly API_PATH = environment.api_path + 'driver'


  constructor(private http: HttpClient) { }

  getIfActive(): Observable<boolean> {
    return this.http.get<boolean>(`${this.API_PATH}/active/driver`);
  }

  changeStatus() : Observable<StatusResponseDto> {
    return this.http.get<StatusResponseDto>(`${this.API_PATH}/change/status`);

  }
}
