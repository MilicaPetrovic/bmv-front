import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import { DriverNotificationDto } from '../../model/driver-notification-dto';
import { NotificationService } from '../../services/notification/notification.service';

@Component({
  selector: 'app-driver-notifications',
  templateUrl: './driver-notifications.component.html',
  styleUrls: ['./driver-notifications.component.css']
})
export class DriverNotificationsComponent implements OnChanges {

  @Input() overlayVisible: boolean
  notifications: DriverNotificationDto[]
  @Output() display = new EventEmitter<DriverNotificationDto>()

  constructor (private readonly notificationService: NotificationService) { }

  ngOnChanges (changes: SimpleChanges) : void {
    if (changes['overlayVisible']) {
      if (this.overlayVisible) {
        this.notificationService.getDriverNotifications().subscribe(
          res => {
            this.notifications = res
          }
        )
      }
    }
  }

  openNotification ($event: DriverNotificationDto) : void {
    this.display.emit($event);
    // umesto otvaranje poruke --> na osnovu tipa notifikacije moze da se otvori neka stranica ili neki dialog
    //this.messageService.add({ key: 'notification-message', severity: 'success', summary: 'Sum', detail: $event.type, life: 5000 })
  }

}
