import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleDriverNotificationComponent } from './single-driver-notification.component';

describe('SingleDriverNotificationComponent', () => {
  let component: SingleDriverNotificationComponent;
  let fixture: ComponentFixture<SingleDriverNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleDriverNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SingleDriverNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
