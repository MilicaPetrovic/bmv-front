import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DriverNotificationDto } from '../../model/driver-notification-dto';

@Component({
  selector: 'app-single-driver-notification',
  templateUrl: './single-driver-notification.component.html',
  styleUrls: ['./single-driver-notification.component.css']
})
export class SingleDriverNotificationComponent{

  @Input() notification: DriverNotificationDto
  @Output() display = new EventEmitter<DriverNotificationDto>()


  displayNotification () : void {
    this.display.emit(this.notification)
  }

}
