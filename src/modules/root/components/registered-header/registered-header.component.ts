import {Component, Input} from '@angular/core'
import {AuthService} from '../../../auth/services/auth/auth.service'
import {ChatWebsocketService} from "../../services/websocket/chat-websocket.service";
import {MessageService} from "primeng/api";
import {LeaveRatingComponent} from "../../../registered-user/components/leave-rating/leave-rating.component";
import {MatDialog} from "@angular/material/dialog";
import {NotificationDto} from "../../model/notification-dto";
import {SplitFareApprovalDialogComponent} from "../split-fare-approval-dialog/split-fare-approval-dialog.component";
import {DetailedDrivingNotificationDto} from "../../model/detailed-driving-notification-dto";
import {DrivingReminderNotification} from "../../model/driving-reminder-notification";
import {BodyData} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-registered-header',
  templateUrl: './registered-header.component.html',
  styleUrls: ['./registered-header.component.css', '../header.component.css'],
  providers: [MessageService]
})
export class RegisteredHeaderComponent {
  @Input() pageTitle: string | undefined
  openLiveChat = false
  overlayVisible = false
  userId: string;
  newNotifications: NotificationDto = {
    canMakeAnAction: false,
    date: [],
    driverAssigned: false,
    driverName: "",
    drivingId: 0,
    id: 0,
    orderTime: [],
    type: ""
  };

  update: boolean = false;
  detailedNotification: DetailedDrivingNotificationDto;

  constructor(private readonly userService: AuthService,
              private readonly webSocketService: ChatWebsocketService,
              public dialog: MatDialog,
              private readonly messageService: MessageService) {
    this.userId = userService.getUsername();
    const stompClient = this.webSocketService.connect()
    stompClient.connect({}, () => {
      stompClient.subscribe('/topic/driving/' + this.userId, (data: BodyData) => {

        switch (JSON.parse(data.body).type) {
          case 'SPLIT_FARE_APPROVAL': {
            this.newNotifications = JSON.parse(data.body)
            this.showSplitFareNotification(this.newNotifications)
            this.update = !this.update
            break
          }
          case 'APPROVED_DRIVING': {
            this.detailedNotification = JSON.parse(data.body)
            this.showApprovedDrivingMessage(this.detailedNotification);
            console.log("*********************************************************")
            console.log(this.detailedNotification);
            break
          }
          case 'REFUSED_DRIVING': {
            this.detailedNotification = JSON.parse(data.body)
            this.showRefusedDrivingMessage(this.detailedNotification);
            console.log("*********************************************************")
            console.log(this.detailedNotification);
            break
          }
          case 'ARRIVED_DRIVER': {
            this.newNotifications = JSON.parse(data.body)
            this.showDriverArrivedNotification(this.newNotifications);
            break
          }
          case 'DRIVING_REMINDER': {
            const drivingReminder: DrivingReminderNotification = JSON.parse(data.body)
            this.showDrivingReminderMessage(drivingReminder);
            break
          }
          case 'RATE_DRIVING': {
            this.newNotifications = JSON.parse(data.body)
            this.showRatingNotification();
          }
        }

      })
    })
  }

  private showApprovedDrivingMessage(detailedNotification: DetailedDrivingNotificationDto) {
    console.log(detailedNotification.drivingId);
    this.messageService.add({
      key: 'notification-message',
      severity: 'success',
      summary: 'Uspešno',
      detail: 'Uspešno smo Vam napravili vožnju'
    })
  }

  private showRefusedDrivingMessage(detailedNotification: DetailedDrivingNotificationDto) {
    console.log(detailedNotification.drivingId);
    this.messageService.add({
      key: 'notification-message',
      severity: 'warn',
      summary: 'Neuspešno',
      detail: 'Nažalost nismo bili u stanju da Vam napravimo vožnju ili vozač više nije u mogućnosti da vozi.',
      life: 100000
    })
  }

  logout(): void {
    this.userService.logout()
  }

  openChat(): void {
    this.openLiveChat = !this.openLiveChat
  }

  toggle(): void {
    console.log("TOGGLE")
    this.overlayVisible = !this.overlayVisible
  }

  triggeredClickOnNotification(): void {

    switch (this.newNotifications.type) {
      case 'SPLIT_FARE_APPROVAL': {
        this.showSplitFareConfirmDialog(this.newNotifications)
        break
      }
      case 'APPROVED_DRIVING':
        break
      case 'ARRIVED_DRIVER':
        break
      case 'DRIVING_REMINDER':
        break
      case 'RATE_DRIVING': {
        this.openRatingDialog(this.newNotifications)
      }
    }
  }

  private openRatingDialog(notification: NotificationDto) {
    if (!notification.canMakeAnAction) {
      this.messageService.add({
        key: 'notification-message',
        severity: 'info',
        summary: 'Oprezno',
        detail: 'Ne možete više da ocenite vožnju'
      })
      return
    }

    const dialogRef = this.dialog.open(LeaveRatingComponent, {
      width: '40%',
      height: '65%',
      data: notification.drivingId
    })
    dialogRef.afterClosed().subscribe(res => {
      console.log("----------------------------------------")
      console.log(res)

      if (res.successful) {
        notification.canMakeAnAction = !notification.canMakeAnAction
        this.messageService.add({
          key: 'notification-message',
          severity: 'success',
          summary: 'Uspešno sačuvana recenzija'
        })
      } else {
        switch (res.exceptionCode) {
          case 1: {//NoUserFoundException
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije pronađen korisnik'
            })
            break;
          }
          case 5: {//NotInTimeException
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Ups niste dali recenziju na vreme'
            })
            break;
          }
          case 9: {//CostumNotFoundException
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije pronađen'
            })
            break;
          }
          case 7: {//NotValidException
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nije validna recenzija'
            })
            break;
          }
          case 4: {//AlreadyInUse
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Korišćen'
            })
            break;
          }
          case 8: {//NoMatchingException
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Morate da date i ocene da bismo sačuvali recenziju'
            })
            break;
          }
          default: {
            this.messageService.add({
              key: 'notification-message',
              severity: 'error',
              summary: 'Neuspešno',
              detail: 'Nešsto se desilo'
            })
          }
        }
      }

    })

  }


  openNotification($event: NotificationDto): void {
    switch ($event.type) {
      case 'SPLIT_FARE_APPROVAL': {
        this.showSplitFareConfirmDialog($event)
        break
      }
      case 'APPROVED_DRIVING':
        break
      case 'ARRIVED_DRIVER':
        break
      case 'DRIVING_REMINDER':
        break
      case 'RATE_DRIVING': {
        this.openRatingDialog($event)
      }
    }
  }

  private showRatingNotification() {
    this.messageService.add({
      key: 'notification-message',
      severity: 'success',
      summary: 'Uspešno ste završili vašu vožnju',
      detail: 'Ako imate vremena procenite vašu vožnju',
      closable: false
    })
  }


  private showSplitFareNotification(notification: NotificationDto) {
    console.log(notification.id)
    this.messageService.add({
      key: 'notification-message',
      severity: 'info',
      summary: 'Neko je vas dodao u vožnju',
      detail: 'Ako ste usaglašeni potvrdite, u slučaju ako niste, samo zanemarite',
      closable: false
    })
  }

  showSplitFareConfirmDialog(notification: NotificationDto): void {
    if (!notification.canMakeAnAction) {
      this.messageService.add({
        key: 'notification-message',
        severity: 'info',
        summary: 'Oprezno',
        detail: 'Već ste potvrdili vožnju'
      })
      return
    }

    const dialogRef = this.dialog.open(SplitFareApprovalDialogComponent, {
      width: '30%',
      height: '65%',
      data: notification.id
    })

    dialogRef.afterClosed().subscribe(res => {
      if (res.successful) {
        notification.canMakeAnAction = false;
        this.messageService.add({
          key: 'notification-message',
          severity: 'success',
          summary: 'Uspešno ste potvrdili vožnju',
          closable: false
        })
      } else {
        console.log(res)
        this.messageService.add({
          key: 'notification-message',
          severity: 'error',
          summary: 'Nismo uspeli da potvrdimo vožnju',
          closable: false
        })
      }
    })
  }

  private showDriverArrivedNotification(newNotifications: NotificationDto) {
    this.messageService.add({
      key: 'notification-message',
      severity: 'success',
      summary: 'Vozač ' + newNotifications.driverName + " je stigao",
      life: 100000
    })
  }

  private showDrivingReminderMessage(drivingReminder: DrivingReminderNotification) {
    let text = 'Podsetnik za ' + drivingReminder.time + "min imate poručenu vožnju\n" +
      "" + drivingReminder.departure + " - " + drivingReminder.arrival + "\n"
    if (drivingReminder.driverAssigned)
      text += '\nSa vozačom: ' + drivingReminder.driverName
    this.messageService.add({
      key: 'notification-message',
      severity: 'success',
      summary: "Voznja",
      detail: text,
      life: 100000
    })
  }
}
