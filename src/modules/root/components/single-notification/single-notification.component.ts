import { Component, EventEmitter, Input, Output } from '@angular/core'
import { NotificationDto } from '../../model/notification-dto'

@Component({
  selector: 'app-single-notification',
  templateUrl: './single-notification.component.html',
  styleUrls: ['./single-notification.component.css']
})
export class SingleNotificationComponent {
  @Input() notification: NotificationDto
  @Output() display = new EventEmitter<NotificationDto>()

  displayNotification () : void {
    this.display.emit(this.notification)
  }
}
