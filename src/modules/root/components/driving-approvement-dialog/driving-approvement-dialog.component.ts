import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RejectDrivingDTO } from 'src/modules/registered-user/model/reject-driving-dto';
import { DriverNotificationDto } from '../../model/driver-notification-dto';
import { NotificationService } from '../../services/notification/notification.service';
import {DrivingService} from "../../../driver/services/driving/driving.service";

@Component({
  selector: 'app-driving-approvement-dialog',
  templateUrl: './driving-approvement-dialog.component.html',
  styleUrls: ['./driving-approvement-dialog.component.css']
})
export class DrivingApprovementDialogComponent implements OnInit {

  notificationData: DriverNotificationDto
  reason: FormControl = new FormControl('', [Validators.required])
  isDisabled: boolean = true

  constructor(private notificationService: NotificationService,
    public dialog: MatDialog,
    private drivingService: DrivingService,
    public dialogRef: MatDialogRef<DrivingApprovementDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public notificationId: number) { }

  ngOnInit(): void {
    this.notificationService.getDriverNotificationInfo(this.notificationId).subscribe(
      res =>{
        console.log("************************************************");
        console.log(res)
        this.notificationData = res;
      }
    )
  }

  confirmDriving(): void {
    this.drivingService.confirmDriving(this.notificationData.drivingId).subscribe(
      res =>{
        this.dialogRef.close(res);
      }
    )
  }

  rejectDriving(): void {
    const dto: RejectDrivingDTO = {
      drivingId: this.notificationData.drivingId,
      reason: this.reason.value
    }
    console.log("rrrrrrrrr easooooooooooooo  nnnnnnnnnn")
    console.log(dto.reason)
    console.log(this.reason.value)
    this.drivingService.rejectDriving(dto).subscribe(
      res =>{
        this.dialogRef.close(res);
      }
    )
  }

  handleInput(event: any): void {
    if (event.target.value !== '')
      this.isDisabled = false
    else
      this.isDisabled = true
  }

}
