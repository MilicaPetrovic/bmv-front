import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingApprovementDialogComponent } from './driving-approvement-dialog.component';

describe('DrivingApprovementDialogComponent', () => {
  let component: DrivingApprovementDialogComponent;
  let fixture: ComponentFixture<DrivingApprovementDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingApprovementDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DrivingApprovementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
