import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core'
import {NotificationService} from '../../services/notification/notification.service'
import {NotificationDto} from '../../model/notification-dto'
import {MessageService} from 'primeng/api'

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
  providers: [MessageService]
})
export class NotificationsComponent implements OnChanges {
  @Input() overlayVisible: boolean
  @Input() update: boolean
  notifications: NotificationDto[]
  @Output() display = new EventEmitter<NotificationDto>()

  constructor(private readonly notificationService: NotificationService, private readonly messageService: MessageService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    console.log("NOTIFY")
    if (changes['overlayVisible']) {
      if (this.overlayVisible) {
        this.notificationService.getNotifications().subscribe(
          res => {
            this.notifications = res
          }
        )
      }
    }

    if (changes['update']) {
      this.notificationService.getNotifications().subscribe(
        res => {
          this.notifications = res
        }
      )
    }
  }

  openNotification($event: NotificationDto): void {
    this.display.emit($event);
    // umesto otvaranje poruke --> na osnovu tipa notifikacije moze da se otvori neka stranica ili neki dialog
    //this.messageService.add({ key: 'notification-message', severity: 'success', summary: 'Sum', detail: $event.type, life: 5000 })
  }
}
