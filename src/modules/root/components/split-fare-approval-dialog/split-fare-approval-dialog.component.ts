import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DetailedDrivingNotificationDto} from "../../model/detailed-driving-notification-dto";
import {NotificationService} from "../../services/notification/notification.service";

@Component({
  selector: 'app-split-fare-approval-dialog',
  templateUrl: './split-fare-approval-dialog.component.html',
  styleUrls: ['./split-fare-approval-dialog.component.css']
})
export class SplitFareApprovalDialogComponent implements OnInit {

  splitFareApprovalData: DetailedDrivingNotificationDto

  constructor(
    private notificationService: NotificationService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SplitFareApprovalDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public notificationId: number) { }

  ngOnInit(): void {
    //call server
    this.notificationService.getDetailedSplitFareNotificationInfo(this.notificationId).subscribe(
      res =>{
        console.log("************************************************");
        console.log(res)
        this.splitFareApprovalData = res;
      }
    )
    /*
    this.splitFareApprovalData = {
      arrival: "NS",
      departure: "SU",
      orderTime: [1,4,2022,14,14],
      orderedFullname: "Violeta Erdelji",
      orderedUsername: "ev",
      partialPrice: 200,
      price: 300
    }*/
  }

  confirmDriving() : void {
    this.notificationService.confirmSplitFare(this.splitFareApprovalData.drivingId).subscribe(
      res =>{
        this.dialogRef.close(res);
      }
    )
  }
}
