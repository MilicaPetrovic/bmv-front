import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitFareApprovalDialogComponent } from './split-fare-approval-dialog.component';

describe('SplitFareApprovalDialogComponent', () => {
  let component: SplitFareApprovalDialogComponent;
  let fixture: ComponentFixture<SplitFareApprovalDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SplitFareApprovalDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SplitFareApprovalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
