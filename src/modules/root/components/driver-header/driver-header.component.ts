import { Component, OnInit, Input } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { MessageService } from 'primeng/api'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { DriverNotificationDto } from '../../model/driver-notification-dto'
import { ChatWebsocketService } from '../../services/websocket/chat-websocket.service'
import { DrivingApprovementDialogComponent } from '../driving-approvement-dialog/driving-approvement-dialog.component'
import {DriverService} from "../../services/driver/driver.service";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {BodyData} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-driver-header',
  templateUrl: './driver-header.component.html',
  styleUrls: ['./driver-header.component.css', '../header.component.css'],
  providers: [MessageService]
})
export class DriverHeaderComponent implements OnInit {
  @Input() pageTitle: string | undefined
  openLiveChat = false
  active: boolean = false
  overlayVisible = false
  userId: string;
  newNotifications: DriverNotificationDto = {
    canMakeAnAction: false,
    date: [],
    drivingId: 0,
    id: 0,
    orderTime: [],
    type: "",
    departure: '',
    arrival: ''
  };

  detailedNotification: DriverNotificationDto

  constructor (private readonly userService: AuthService,
               public dialog: MatDialog,
               private driverService: DriverService,
               private readonly webSocketService: ChatWebsocketService,
               private readonly messageService: MessageService) {
    this.userId = userService.getUsername();
    const stompClient = this.webSocketService.connect()
    stompClient.connect({}, () => {
      stompClient.subscribe('/topic/driving/' + this.userId, (data: BodyData) => {

        switch (JSON.parse(data.body).type) {
          case 'SPLIT_FARE_APPROVAL':break
          case 'APPROVED_DRIVING': break
          case 'REFUSED_DRIVING': break
          case 'ARRIVED_DRIVER':break
          case 'ADDED_DRIVING': {
            this.detailedNotification = JSON.parse(data.body)
            this.showDriverNotification();
            console.log("*********************************************************")
            console.log(this.detailedNotification);
            break
          }
          case 'RATE_DRIVING':break
        }

      })
    })
  }


  ngOnInit (): void {
    if (localStorage.getItem('active') != null || localStorage.getItem('active') != undefined){
      if (localStorage.getItem('active') === 'true')
        this.active = true;
      return;
    }
    this.driverService.getIfActive().subscribe(res=>{
      this.changeActive(res);
      },
      () => {this.changeActive(false)});
  }

  logout () : void {
    this.userService.driverLogout().subscribe(
      (res) => {
        if (!res.successful){
          this.handleRes(res);
        }else{
          this.userService.removeCredentials();
          localStorage.removeItem('active');
        }
      },
      () => {
        this.userService.removeCredentials();
      }
    )
  }

  private handleRes(res: StatusResponseDto) {
    if (res.exceptionCode === 18){
      this.messageService.add({
        key: 'notification-message',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Imate aktivne vožnju ne možete da se izlogujete'
      })
    }
  }

  openChat () : void {
    this.openLiveChat = !this.openLiveChat
  }

  toggle () : void {
    this.overlayVisible = !this.overlayVisible
  }

  changeStatus() : void {
    this.driverService.changeStatus().subscribe(
      res=>{
        if (res.successful) { this.changeActive(!this.active);}
        this.handleRes(res);
        });
  }

  private changeActive(newStatus: boolean) {
    this.active = newStatus;
    localStorage.setItem('active', this.active.toString())
  }

  triggeredClickOnNotification(): void {

    switch (this.newNotifications.type) {
      case 'SPLIT_FARE_APPROVAL':break
      case 'APPROVED_DRIVING':break
      case 'ARRIVED_DRIVER':break
      case 'ADDED_DRIVING': {
        this.showDrivingConfirmDialog(this.newNotifications)
        break
      }
      case 'RATE_DRIVING':break
    }
  }

  openNotification ($event: DriverNotificationDto) : void {
    switch ($event.type) {
      case 'SPLIT_FARE_APPROVAL':break
      case 'APPROVED_DRIVING':break
      case 'ARRIVED_DRIVER':break
      case 'ADDED_DRIVING': {
        this.showDrivingConfirmDialog($event)
        break
      }
      case 'RATE_DRIVING':break
    }
  }

  private showDriverNotification() {
    this.messageService.add({
      key: 'notification-message',
      severity: 'info',
      summary: 'Dodijeljeni ste na novu vožnju',
      detail: 'Ako ste usaglašeni potvrdite, u slučaju ako niste, otkažite uz obrazloženje',
      closable: false
    })
  }

  showDrivingConfirmDialog(notification: DriverNotificationDto): void {
    if (!notification.canMakeAnAction){
      this.messageService.add({
        key: 'notification-message',
        severity: 'info',
        summary: 'Oprezno',
        detail: 'Već ste potvrdili odnosno odbili vožnju'
      })
      return
    }

    const dialogRef = this.dialog.open(DrivingApprovementDialogComponent, {
      width: '40%',
      height: '65%',
      data: notification.id
    })

    dialogRef.afterClosed().subscribe(res => {
      if (res === null || res === undefined || res.successful === undefined || res.successful === null)
        return;
      if (res.successful){
        notification.canMakeAnAction = false;
        this.messageService.add({
          key: 'notification-message',
          severity: 'success',
          summary: 'Uspešno ste započeli vožnju',
          closable: false
        })
      }else {
        console.log(res)
        this.messageService.add({
          key: 'notification-message',
          severity: 'info',
          summary: 'Uspešno ste odbili vožnju',
          closable: false
        })
      }
      notification.canMakeAnAction = false;
    })
  }
}
