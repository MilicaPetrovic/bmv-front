import {Component, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core'
import {
  ChatParticipantStatus,
  ChatParticipantType,
  IChatController,
  IChatParticipant
} from 'ng-chat'
import { MyChatAdapter } from '../../services/chat/my-chat-adapter'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { ChatWebsocketService } from '../../services/websocket/chat-websocket.service'
import {BodyData} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnChanges {
  userId = ''
  @ViewChild('ngChatInstance') protected ngChatInstance: IChatController
  @Input() openChat: boolean

  constructor (public adapter: MyChatAdapter,
    private readonly authService: AuthService,
    private readonly webSocketService: ChatWebsocketService) {
    this.userId = authService.getUsername()

    this.adapter.getFriendsList()
    const stompClient = this.webSocketService.connect()
    stompClient.connect({}, () => {
      stompClient.subscribe('/topic/' + this.userId, (data: BodyData) => {
        const d = JSON.parse(data.body)
        console.log('WEBSOCKET')
        console.log(d)
        const participant: IChatParticipant = {
          participantType: ChatParticipantType.User,
          id: d.user.username,
          displayName: d.user.displayName,
          avatar: 'assets/admin3.png',
          status: ChatParticipantStatus.Online
        }
        this.adapter.onMessageReceived(participant, d.message)
      })
    })
  }

  ngOnChanges (changes: SimpleChanges) : void {
    if (changes['openChat']) {
      if (this.adapter.mockedParticipants.length > 0) {
        const first = this.adapter.mockedParticipants[0]
        this.ngChatInstance.triggerOpenChatWindow(
          {
            participantType: first.participantType,
            id: first.id,
            displayName: first.displayName,
            avatar: "assets/admin3.png",
            status: first.status
          }
        )
      }
    }
  }
}
/* {
          participantType: ChatParticipantType.User,
          id: 1, //vrv zato sto je id uvek 1
          displayName: "Arya Stark",
          avatar: "https://66.media.tumblr.com/avatar_9dd9bb497b75_128.pnj",
          status: ChatParticipantStatus.Online
        }
        */
