import {Component, Input} from '@angular/core'
import { AuthService } from '../../../auth/services/auth/auth.service'

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css', '../header.component.css']
})
export class AdminHeaderComponent {
  @Input() pageTitle: string | undefined
  constructor (private readonly userService: AuthService) { }

  logout () : void {
    this.userService.logout()
  }
}
