import { Component, Input, ViewChild } from '@angular/core'
import { MyChatAdapter } from '../../services/chat/my-chat-adapter'
import { ChatParticipantStatus, ChatParticipantType, IChatController, IChatParticipant } from 'ng-chat'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { ChatWebsocketService } from '../../services/websocket/chat-websocket.service'
import {BodyData} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-admin-chat',
  templateUrl: './admin-chat.component.html',
  styleUrls: ['./admin-chat.component.css']
})
export class AdminChatComponent {
  title = 'Korisnici'
  searchPlaceHolder = 'Pretraži'
  userId = 'admin'
  @ViewChild('ngChatInstance') protected ngChatInstance: IChatController
  @Input() openChat: boolean

  constructor (public adapter: MyChatAdapter,
    private readonly authService: AuthService,
    private readonly webSocketService: ChatWebsocketService) {
    this.adapter.getFriendsList()

    const stompClient = this.webSocketService.connect()
    stompClient.connect({}, () => {
      stompClient.subscribe('/topic/' + this.userId, (data: BodyData) => {
        this.handleNewMessage(data)
      })
    })
  }

  private handleNewMessage (data: BodyData) {
    const d = JSON.parse(data.body)
    const participant: IChatParticipant = {
      participantType: ChatParticipantType.User,
      id: d.user.username,
      displayName: d.user.displayName,
      avatar: 'assets/admin3.png',
      status: ChatParticipantStatus.Online
    }
    this.ngChatInstance.triggerCloseChatWindow(participant.id)
    this.ngChatInstance.triggerOpenChatWindow({
      participantType: ChatParticipantType.User,
      id: d.user.username,
      displayName: d.user.displayName,
      avatar: 'assets/admin3.png',
      status: ChatParticipantStatus.Online
    })
  }
}
