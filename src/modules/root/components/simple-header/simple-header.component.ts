import {Component, Input} from '@angular/core'

@Component({
  selector: 'app-simple-header',
  templateUrl: './simple-header.component.html',
  styleUrls: ['./simple-header.component.css', '../header.component.css']
})
export class SimpleHeaderComponent {
  @Input() pageTitle: string | undefined

}
