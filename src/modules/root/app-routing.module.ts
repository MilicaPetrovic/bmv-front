import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { NotFoundComponent } from './pages/not-found/not-found.component'
import { RootComponent } from './pages/root/root.component'

// admin
// registeredUser
// driver
const routes: Routes = [
  {
    path: 'admin',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../admin/admin.module').then((m) => m.AdminModule)
      }
    ]
  },
  {
    path: 'reports',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../reports/reports.module').then((m) => m.ReportsModule)
      }
    ]
  },
  {
    path: 'auth',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../auth/auth.module').then((m) => m.AuthModule)
      }
    ]
  },

  {
    path: 'profile',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
           import('../profile/profile.module').then((m) => m.ProfileModule)
      }
    ]
  },

  {
    path: 'registered',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../registered-user/registered-user.module').then((m) => m.RegisteredUserModule)
      }
    ]
  },
  {
    path: 'driver',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../driver/driver.module').then((m) => m.DriverModule)
      }
    ]
  },
  {
    path: '',
    children: [
      {
        path: '',
        component: RootComponent,
        loadChildren: () =>
          import('../map/map.module').then((m) => m.MapModule)
      }
    ]
  },

  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
