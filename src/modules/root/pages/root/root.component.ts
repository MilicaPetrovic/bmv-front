import { Component, OnInit } from '@angular/core'
import { JwtHelperService } from '@auth0/angular-jwt'
import {Router} from "@angular/router";
import {TitleService} from "../../services/title.service";

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {
  public role: string

  pageTitle: string | undefined = 'default page title' ;

  constructor (private router: Router, private titleService: TitleService) {
    this.role = ''
  }

  ngOnInit () : void {
    this.pageTitle = this.titleService.getTitle(this.router.url)

    this.router.events.subscribe(() => {
      this.pageTitle = this.titleService.getTitle(this.router.url)
   })
    //console.log(this.router.url);
    /*console.log(this.route.data)
    this.route.data.subscribe(
      (data) => {
        console.log(data)
        this.pageTitle = data['title'];
      }
    );*/
  }

  checkRole () : void {
    const item = localStorage.getItem('token')
    if (item) {
      const jwt: JwtHelperService = new JwtHelperService()
      const info = jwt.decodeToken(item)

      for (const key in info.role[0]) {
        if (info.role[0][key]) {
          console.log('IMAM ROLU: ' + key)
          this.role = key
        }
      }
    } else {
      this.role = ''
    }
  }
}
