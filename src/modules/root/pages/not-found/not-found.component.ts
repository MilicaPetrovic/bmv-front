import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from '../../../auth/services/auth/auth.service'

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent {
  constructor (private readonly router: Router,
    private readonly authService: AuthService) { }

  takeMeHome (): void {
    const userLoggedIn = this.authService.getUser()
    console.log(userLoggedIn)
    if (userLoggedIn === null || userLoggedIn === '') { this.router.navigate(['auth/login']) } else { this.router.navigate(['home']) }
  }
}
