export interface NotificationDto{
  id: number
  drivingId: number
  date: number[]
  type: string
  orderTime: number[]
  driverAssigned: boolean
  driverName: string
  canMakeAnAction: boolean
}
