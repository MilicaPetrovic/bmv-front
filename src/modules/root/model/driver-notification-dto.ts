export interface DriverNotificationDto {
    id: number
    orderTime: number[]
    departure: string
    arrival: string
    drivingId: number
    type: string
    date: number[]
    canMakeAnAction: boolean
  }