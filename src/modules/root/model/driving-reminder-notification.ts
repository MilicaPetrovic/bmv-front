export interface DrivingReminderNotification {
  date: number[];
  type: string;
  orderTime: number[];
  driverAssigned: boolean;
  driverName: string;
  departure: string;
  arrival: string;
  canMakeAnAction: boolean;
  time: number;
}
