export interface DetailedDrivingNotificationDto {
  orderedUsername: string
  orderedFullname: string
  orderTime: number[]
  departure: string
  arrival: string
  price: number
  partialPrice: number
  drivingId: number
  type: string
}
