export interface DrivingStateDto{
  start :boolean;
  finish :boolean;
  drivingId:number
}
