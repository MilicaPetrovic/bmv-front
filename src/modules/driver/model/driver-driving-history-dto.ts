export interface DriverDrivingHistoryDto {
  drivingId: number
  departure: string
  arrival: string
  price: number
  startDateTime: number[]
  endDateTime: number[]
  state: string
}
