import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {DriverRoutes} from "./driver.routes";
import { DriverDrivingHistoryComponent } from './pages/driver-driving-history/driver-driving-history.component';
import {SharedModule} from "../shared/shared.module";
import {TreeSelectModule} from "primeng/treeselect";
import {TableModule} from "primeng/table";
import {FormsModule} from "@angular/forms";
import { DriverDrivingDetailedComponent } from './components/driver-driving-detailed/driver-driving-detailed.component';
import {DriverSimulationComponent} from "./pages/driver-simulation/driver-simulation.component";
import {ToastModule} from "primeng/toast";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [
    DriverDrivingHistoryComponent,
    DriverDrivingDetailedComponent,
    DriverSimulationComponent
  ],
  imports: [
    RouterModule.forChild(DriverRoutes),
    CommonModule,
    SharedModule,
    TreeSelectModule,
    TableModule,
    FormsModule,
    ToastModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class DriverModule { }
