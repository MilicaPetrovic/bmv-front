import { Routes } from '@angular/router'
import {RoleGuard} from "../auth/guards/role/role.guard";
import {DriverDrivingHistoryComponent} from "./pages/driver-driving-history/driver-driving-history.component";
import {DriverSimulationComponent} from "./pages/driver-simulation/driver-simulation.component";

export const DriverRoutes: Routes = [
  {
    path: 'driving/history',
    component: DriverDrivingHistoryComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'driver' }
  },
  {
    path:'simulation',
    component :DriverSimulationComponent,
    canActivate:[RoleGuard],
    data:{expectedRoles: 'driver'}
  },

]
