import {Component, OnInit} from '@angular/core';
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import {MessageService} from "primeng/api";
import {RejectDrivingDTO} from "../../../registered-user/model/reject-driving-dto";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {DrivingService} from "../../services/driving/driving.service"
import {AuthService} from "../../../auth/services/auth/auth.service";
import {DrivingStateDto} from "../../model/driving-state-dto";
import {StompClient} from "../../../shared/models/stomp-client";

@Component({
  selector: 'app-driver-simulation',
  templateUrl: './driver-simulation.component.html',
  styleUrls: ['./driver-simulation.component.css'],
  providers: [MessageService]
})
export class DriverSimulationComponent implements OnInit {

  isButtonStartRejectDisabled = true
  private stompClient: StompClient;
  drivingId: number
  reason: string = "";

  isButtonEndDisabled = true

  drivingState :DrivingStateDto;


  constructor(private drivingService: DrivingService,
              private messageService: MessageService,
              private authService : AuthService) {
  }

  ngOnInit(): void {
    this.initializeWebSocketConnection();
    this.drivingService.getStateOfLastDriving().subscribe(res=>{
      this.drivingState = res;
      console.log(" ovo je stanje poslednjeg drivinga ako postoji")
      console.log(res);
      this.setButtonFlags();
    });
  }

  setButtonFlags():void{
    this.drivingId = this.drivingState.drivingId;
    if(this.drivingState.start){
      this.isButtonStartRejectDisabled = false;

    }else if(this.drivingState.finish){
      this.isButtonEndDisabled = false;
    }
  }

  initializeWebSocketConnection():void {
    const ws = new SockJS('https://localhost:8080/uber/socket');
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    this.stompClient.connect({}, () => {
      this.openGlobalSocket();
    });
  }

  openGlobalSocket():void {
    this.stompClient.subscribe('/topic/map-updates/enable-start-driving/'+this.authService.getUsername(), (message: { body: string }) => {
      console.log("radi socket")
      console.log(message.body)
      this.drivingId = Number(message.body)
      this.isButtonStartRejectDisabled = false


    });

    this.stompClient.subscribe('/topic/map-updates/enable-end-driving/'+this.authService.getUsername(), (message: { body: string }) => {
      console.log(message.body)
      this.drivingId = Number(message.body)
      this.isButtonEndDisabled = false
    });
  }

  startDriving():void {
    this.drivingService.confirmDriving(this.drivingId).subscribe(res => {
      console.log("response start")
      console.log(res)
      this.showStartResponse(res)
      this.isButtonStartRejectDisabled = true
    })

  }

  rejectDriving():void {
    const rejectDto: RejectDrivingDTO = {
      reason: "Nema putnika",
      drivingId: this.drivingId
    }
    this.drivingService.rejectDriving(rejectDto).subscribe(res => {
      console.log("response reject")
      console.log(res)
      this.showRejectResponse(res)
      this.isButtonStartRejectDisabled = true
      this.isButtonEndDisabled = true
    })
  }

  endDriving():void{
    this.drivingService.endDriving(this.drivingId).subscribe(res=>{
      console.log("response end")
      console.log(res)
      this.showEndResponse(res)
      this.isButtonEndDisabled = true
    })

  }

  showStartResponse(res: StatusResponseDto):void {

    if (res.successful) {
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'success',
        summary: 'Uspešno',
        detail: 'Započeli ste vožnju.'
      })
    } else if (res.exceptionCode === -100) {
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Nismo uspeli da započnemo vožnju'
      })
    }
  }

  showRejectResponse(res: StatusResponseDto):void {
    if (!res.successful) {
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'success',
        summary: 'Uspešno',
        detail: 'Odbili ste vožnju.'
      })
    } else if (res.exceptionCode === -100) {
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Nismo uspeli da odbijemo vožnju'
      })
    }
  }

  private showEndResponse(res: StatusResponseDto):void {
    if(res.successful){
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'success',
        summary: 'Uspešno',
        detail: 'Završili ste vožnju.'
      })
    }
    else if(res.exceptionCode==1){
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Vozač nije pronađen'
      })
    }
    else if (res.exceptionCode == 7){
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Ne možete završiti tuđu vožnju'
      })
    }
    else if(res.exceptionCode == 13){
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Vožnja je već završena.'
      })
    }
    else if(res.exceptionCode == 12){
      // should not happen exception
      this.messageService.add({
        key: 'driver-start-reject-driving',
        severity: 'warn',
        summary: 'Neuspešno',
        detail: 'Došlo je do nepredviđenog problema.'
      })
    }

  }
}
