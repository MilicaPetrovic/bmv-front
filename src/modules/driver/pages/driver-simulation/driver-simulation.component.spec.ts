import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverSimulationComponent } from './driver-simulation.component';

describe('TestSumilationComponent', () => {
  let component: DriverSimulationComponent;
  let fixture: ComponentFixture<DriverSimulationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverSimulationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DriverSimulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
