import {Component, OnInit} from '@angular/core';
import {TreeNodeSearch} from "../../../shared/models/tree-node-search";
import {MatDialog} from "@angular/material/dialog";
import {LazyLoadEvent} from "primeng/api";
import {DrivingService} from "../../services/driving/driving.service";
import {DriverDrivingHistoryDto} from "../../model/driver-driving-history-dto";
import {
  DriverDrivingDetailedComponent
} from "../../components/driver-driving-detailed/driver-driving-detailed.component";

@Component({
  selector: 'app-driver-driving-history',
  templateUrl: './driver-driving-history.component.html',
  styleUrls: ['./driver-driving-history.component.css']
})
export class DriverDrivingHistoryComponent implements OnInit {
  drivings: DriverDrivingHistoryDto[]
  loading = false
  totalRecords = 0
  rowCount = 5
  nodes: TreeNodeSearch[]
  selectedNode: TreeNodeSearch
  lastSortedBy: TreeNodeSearch | null
  sortBy: string = ''

  constructor(
    private drivingService: DrivingService,
    public dialog: MatDialog) {
    this.nodes = [
      new TreeNodeSearch('Polazište', 'pi pi-clock', ''),
      new TreeNodeSearch('Odredište', 'pi pi-clock', ''),

      new TreeNodeSearch('Cena', 'pi pi-wallet', ''),

      new TreeNodeSearch('Početak vožnja', 'pi pi-compass', ''),
      new TreeNodeSearch('Kraj vožnja', 'pi pi-compass', '')
    ]
  }

  ngOnInit(): void {
    this.loading = true
    this.getDrivings(0, this.rowCount)
  }

  nextPage(event: LazyLoadEvent): void {
    // this.loading = true;
    // event.first = 0
    // event.rows = 3
    // event.sortField ='' ;
    // event.sortOrder = -1;
    // filters:{}

    setTimeout(() => {
      this.loading = true
      if (event.first === undefined) {
        event.first = 0
      }
      if (event.rows === undefined) {
        event.rows = 5
      }
      this.rowCount = event.rows;
      this.getDrivings(event.first / event.rows, event.rows)
    }, 500)
  }


  openDialog(id: number): void {
    const dialogRef = this.dialog.open(DriverDrivingDetailedComponent, {
      width: '70%',
      height: '95%',
      data: id
    })
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed')
    })
  }

  private getDrivings(number: number, rowCount: number) {
    this.drivingService.getDrivings(number, rowCount, this.sortBy).subscribe(data => {
      this.totalRecords = data.totalCount
      this.drivings = data.resultList
      this.loading = false
    })
  }

  sortElements(): void {
    console.log("u funkciji");
    if (this.lastSortedBy !== this.selectedNode) {
      switch (this.selectedNode.label) {
        case 'Polazište': {
          this.sortBy = 'polaziste'
          this.getDrivings(0, this.rowCount);
          break;
        }
        case 'Odredište': {
          this.sortBy = 'odrediste'
          this.getDrivings(0, this.rowCount);
          break;
        }
        case 'Cena': {
          this.sortBy = 'cena'
          this.getDrivings(0, this.rowCount);
          break;
        }
        case 'Početak vožnja': {
          this.sortBy = 'start'
          this.getDrivings(0, this.rowCount);
          break;
        }
        case 'Kraj vožnja': {
          this.sortBy = 'end'
          this.getDrivings(0, this.rowCount);
          break;
        }
      }

      //this.getDrivings()
    }

  }

}
