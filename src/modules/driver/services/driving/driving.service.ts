import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {PaginationDTO} from "../../../shared/models/pagination-dto";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {DriverDrivingHistoryDto} from "../../model/driver-driving-history-dto";
import {DetailedDrivingDto} from "../../../admin/models/detailed-driving-dto";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {RejectDrivingDTO} from "../../../registered-user/model/reject-driving-dto";
import {DrivingStateDto} from "../../model/driving-state-dto";

@Injectable({
  providedIn: 'root'
})
export class DrivingService {
  private readonly API_PATH = environment.api_path + ''

  constructor(private http: HttpClient){}

  getDrivings (from: number, until: number, sortBy: string): Observable<PaginationDTO<DriverDrivingHistoryDto>> {
    return this.http.get<PaginationDTO<DriverDrivingHistoryDto>>(`${this.API_PATH}driving/history/driver/all/${from}/${until}/${sortBy}`,
    )
  }

  getDriving (drivingId: number): Observable<DetailedDrivingDto> {
    return this.http.get<DetailedDrivingDto>(`${this.API_PATH}driving/history/driver/detail/${drivingId}`)
  }

  endDriving(drivingId: number):Observable<StatusResponseDto>{
    return this.http.get<StatusResponseDto>(`${this.API_PATH}driving/flow/end/${drivingId}`)
  }

  rejectDriving(dto: RejectDrivingDTO):Observable<StatusResponseDto> {
    return this.http.post<StatusResponseDto>(`${this.API_PATH}reject/driving`, dto)
  }

  confirmDriving(drivingId: number):Observable<StatusResponseDto> {
    return this.http.post<StatusResponseDto>(`${this.API_PATH}confirm/driving`, drivingId)
  }

  getStateOfLastDriving():Observable<DrivingStateDto> {
    return this.http.get<DrivingStateDto>(`${this.API_PATH}simulation/state/last/driving`)
  }

}
