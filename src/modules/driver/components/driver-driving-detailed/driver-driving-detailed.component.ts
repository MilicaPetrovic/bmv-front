import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DetailedDrivingDto} from "../../../admin/models/detailed-driving-dto";
import {DrivingService} from "../../services/driving/driving.service";

@Component({
  selector: 'app-driver-driving-detailed',
  templateUrl: './driver-driving-detailed.component.html',
  styleUrls: ['./driver-driving-detailed.component.css']
})
export class DriverDrivingDetailedComponent implements OnInit {

  driving: DetailedDrivingDto

  constructor(
    private readonly drivingService: DrivingService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DriverDrivingDetailedComponent>,
    @Inject(MAT_DIALOG_DATA) public drivingId: number) {
  }

  ngOnInit(): void {
    this.setDriving();
  }

  private setDriving() {
    this.drivingService.getDriving(this.drivingId).subscribe(
      res => {
        this.driving = res
      },
      err => {
        console.log('FAIL')
        console.log(err)
      }
    )
  }

}
