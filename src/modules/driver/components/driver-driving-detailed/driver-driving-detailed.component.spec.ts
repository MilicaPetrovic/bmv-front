import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverDrivingDetailedComponent } from './driver-driving-detailed.component';

describe('DriverDrivingDetailedComponent', () => {
  let component: DriverDrivingDetailedComponent;
  let fixture: ComponentFixture<DriverDrivingDetailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverDrivingDetailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DriverDrivingDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
