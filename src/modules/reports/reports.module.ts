import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDrivingReportsComponent } from './pages/user-driving-reports/user-driving-reports.component';
import {RouterModule} from "@angular/router";
import {ReportsRoutes} from "./roports.routes";
import {ChartModule} from "primeng/chart";
import {MatNativeDateModule} from "@angular/material/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {ToastModule} from "primeng/toast";
import { AdminDrivingReportsComponent } from './components/admin-driving-reports/admin-driving-reports.component';
import {MatRadioModule} from "@angular/material/radio";
import {AutoCompleteModule} from "primeng/autocomplete";
import { AdminReportsComponent } from './pages/admin-reports/admin-reports.component';
import {TabViewModule} from "primeng/tabview";
import { AdminGlobalReportsComponent } from './components/admin-global-reports/admin-global-reports.component';
import { ComplexReportComponent } from './components/complex-report/complex-report.component';



@NgModule({
  declarations: [
    UserDrivingReportsComponent,
    AdminDrivingReportsComponent,
    AdminReportsComponent,
    AdminGlobalReportsComponent,
    ComplexReportComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ReportsRoutes),
    ChartModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    ToastModule,
    MatRadioModule,
    AutoCompleteModule,
    FormsModule,
    TabViewModule,
  ]
})
export class ReportsModule { }
