import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ReportDto} from "../../model/report-dto";
import {PeriodDto} from "../../model/period-dto";
import {MessageService} from "primeng/api";
import {ReportService} from "../../services/report.service";
import {MatRadioChange} from '@angular/material/radio';
import _ from 'lodash';
import {UserDto} from "../../model/user-dto";
import {UserService} from "../../services/user.service";
import {ChartData} from "chart.js";

@Component({
  selector: 'app-admin-driving-reports',
  templateUrl: './admin-driving-reports.component.html',
  styleUrls: ['../admin-reports.component.css'],
  providers: [MessageService]
})
export class AdminDrivingReportsComponent implements OnInit {
  selectedUser: UserDto
  users: UserDto[]
  basicData: ChartData
  clientChosen: boolean = true;
  lastSelectedUser: UserDto;
  lastPeriod: PeriodDto;
  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  reportDto: ReportDto = {
    moneyForDays: [],
    distancesForDays: [],
    numberOfDrivings: [],
    labels: [],
    sumOfDrivings: 0,
    averageOfDrivings: 0,
    sumOfDistances: 0,
    averageOfDistances: 0,
    sumOfMoney: 0,
    averageOfMoney: 0
  }

  constructor(
    private readonly messageService: MessageService,
    private reportService: ReportService,
    private userService: UserService) {}

  ngOnInit(): void {
    this.setUpAutocomplete()
  }

  submited(): void {
    const start = this.range.controls.start.value
    const end = this.range.controls.end.value
    if (start == null || end == null) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval za izveštavanje'
      })
      return
    }
    if (start > new Date() || end > new Date()) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval u poršlosti za izveštavanje'
      })
      return
    }
    const period: PeriodDto = {
      startString: start,
      endString: end
    }

    if (_.isEqual(this.lastSelectedUser, this.selectedUser) && _.isEqual(this.lastPeriod, period))
      return;

    if (this.clientChosen)
      this.reportService.adminReportForClient(period, this.selectedUser.username).subscribe(
        (res) => {
          this.handleReportResponse(period, res);
        });
    else
      this.reportService.adminReportForDriver(period, this.selectedUser.username).subscribe(
        (res) => {
          this.handleReportResponse(period, res);
        });
  }


  handleReportResponse(period: PeriodDto, report: ReportDto): void {
    this.lastPeriod = period
    this.lastSelectedUser = this.selectedUser
    this.reportDto = report
  }

  optionChanged($event: MatRadioChange): void {
    let new_value = this.clientChosen
    if ($event.value === '1')
      new_value = true
    else
      new_value = false
    if (new_value != this.clientChosen) {
      this.clientChosen = new_value
      this.setUpAutocomplete()
    }
  }

  filterValues(event: any): void {
    const new_users: UserDto[] = [];
    for (const user of this.users) {
      if (user.username.includes(event.query))
        new_users.push(user);
    }
    this.users = new_users;
  }

  setUpAutocomplete(): void {
    if (this.clientChosen)
      this.userService.getClients().subscribe(
        (res) => {
          this.users = res
        })
    else
      this.userService.getDrivers().subscribe(
        (res) => {
          this.users = res
        })
  }
}

