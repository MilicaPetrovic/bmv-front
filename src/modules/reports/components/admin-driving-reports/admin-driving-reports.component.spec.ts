import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDrivingReportsComponent } from './admin-driving-reports.component';

describe('AdminDrivingReportsComponent', () => {
  let component: AdminDrivingReportsComponent;
  let fixture: ComponentFixture<AdminDrivingReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDrivingReportsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminDrivingReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
