import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {UserDto} from "../../model/user-dto";
import {ReportDto} from "../../model/report-dto";
import {ChartData} from "chart.js";
import {ReportService} from "../../services/report.service";

@Component({
  selector: 'app-complex-report',
  templateUrl: './complex-report.component.html',
  styleUrls: ['../admin-reports.component.css']
})
export class ComplexReportComponent implements OnChanges {
  @Input() basicData: ChartData;
  @Input() reportDto: ReportDto
  @Input() lastSelectedUser: UserDto


  constructor(private reportService: ReportService) { }

  ngOnChanges (changes: SimpleChanges) : void {
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    console.log("change")
    if (changes['reportDto']) {
      this.createBasicData()
    }
  }

  private createBasicData() {
    this.basicData = {
      labels: this.reportService.convertDateArrayToString(this.reportDto.labels),
      datasets: [
        {
          label: 'Broj vožnji',
          data: this.reportDto.numberOfDrivings,
          fill: false,
          borderColor: '#FFA726',
          tension: .4
        },
        {
          label: 'Broj pređenih kilometara',
          data: this.reportDto.distancesForDays,
          fill: false,
          borderColor: '#42A5F5',
          tension: .4
        },
        {
          label: 'Troškovi',
          data: this.reportDto.moneyForDays,
          fill: false,
          borderColor: '#EC407A',
          tension: .4
        }
      ]
    };
  }

}
