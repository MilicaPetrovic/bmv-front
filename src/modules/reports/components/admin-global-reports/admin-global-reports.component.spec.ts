import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGlobalReportsComponent } from './admin-global-reports.component';

describe('AdminGlobalReportsComponent', () => {
  let component: AdminGlobalReportsComponent;
  let fixture: ComponentFixture<AdminGlobalReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminGlobalReportsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminGlobalReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
