import {Component} from '@angular/core';
import {UserDto} from "../../model/user-dto";
import {PeriodDto} from "../../model/period-dto";
import {FormControl, FormGroup} from "@angular/forms";
import {ReportDto} from "../../model/report-dto";
import {MessageService} from "primeng/api";
import {ReportService} from "../../services/report.service";
import _ from "lodash";
import {ChartData} from "chart.js";

@Component({
  selector: 'app-admin-global-reports',
  templateUrl: './admin-global-reports.component.html',
  styleUrls: ['../admin-reports.component.css'],
  providers: [MessageService]
})
export class AdminGlobalReportsComponent {
  reportFor: UserDto = {
    fullname: "Korisnici", profile: "assets/icons/group.png", username: ""
  }
  basicData: ChartData
  lastPeriod: PeriodDto;
  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  reportDto: ReportDto = {
    moneyForDays: [],
    distancesForDays: [],
    numberOfDrivings: [],
    labels: [],
    sumOfDrivings: 0,
    averageOfDrivings: 0,
    sumOfDistances: 0,
    averageOfDistances: 0,
    sumOfMoney: 0,
    averageOfMoney: 0
  }

  constructor(
    private readonly messageService: MessageService,
    private reportService: ReportService) {
  }

  submited(): void {
    const start = this.range.controls.start.value
    const end = this.range.controls.end.value
    if (start == null || end == null) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval za izveštavanje'
      })
      return
    }
    if (start > new Date() || end > new Date()) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval u poršlosti za izveštavanje'
      })
      return
    }
    const period: PeriodDto = {
      startString: start,
      endString: end
    }

    if (_.isEqual(this.lastPeriod, period))
      return;

    this.reportService.adminReport(period).subscribe(
      (res) => {
        this.handleReportResponse(period, res);
      });
  }


  handleReportResponse(period: PeriodDto, report: ReportDto): void {
    this.lastPeriod = period
    this.reportDto = report
  }


}
