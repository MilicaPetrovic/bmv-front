import {Routes} from "@angular/router";
import {RoleGuard} from "../auth/guards/role/role.guard";
import {UserDrivingReportsComponent} from "./pages/user-driving-reports/user-driving-reports.component";
import {AdminReportsComponent} from "./pages/admin-reports/admin-reports.component";

export const ReportsRoutes: Routes = [

  {
    path: 'client',
    component: UserDrivingReportsComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'registeredUser'}
  },
  {
    path: 'driver',
    component: UserDrivingReportsComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'driver'}
  },
  {
    path: 'admin',
    component: AdminReportsComponent,
    canActivate: [RoleGuard],
    data: {expectedRoles: 'admin'}
  },
]
