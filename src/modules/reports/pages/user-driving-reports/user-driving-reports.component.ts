import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {DateAdapter} from '@angular/material/core';
import {PeriodDto} from "../../model/period-dto";
import {ReportService} from "../../services/report.service";
import {ReportDto} from "../../model/report-dto";
import {MessageService} from "primeng/api";
import {ActivatedRoute} from '@angular/router';
import {ChartData} from "chart.js";

@Component({
  selector: 'app-user-driving-reports',
  templateUrl: './user-driving-reports.component.html',
  styleUrls: ['./user-driving-reports.component.css'],
  providers: [MessageService]
})
export class UserDrivingReportsComponent implements OnInit {

  range = new FormGroup({
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });
  reportDto: ReportDto = {
    moneyForDays: [],
    distancesForDays: [],
    numberOfDrivings: [],
    labels: [],
    sumOfDrivings: 0,
    averageOfDrivings: 0,
    sumOfDistances: 0,
    averageOfDistances: 0,
    sumOfMoney: 0,
    averageOfMoney: 0
  }

  basicDataDrivingNumber: ChartData
  basicDataDistance: ChartData
  basicDataMoney: ChartData
  client: boolean = true

  constructor(private reportService: ReportService, private router: ActivatedRoute,
              private readonly messageService: MessageService,
              private dateAdapter: DateAdapter<Date>) {
    this.dateAdapter.setLocale('sr-Latn');
  }

  ngOnInit(): void {
    this.client = this.router.snapshot.data['expectedRoles'] === 'registeredUser';
  }

  setDataOn(labels: string[], data1: number[], color: string, name: string): ChartData {
    return {
      labels: labels,
      datasets: [
        {
          label: name,
          data: data1,
          fill: false,
          borderColor: color,// '#42A5F5',
          tension: .4
        }
      ]
    }

  }

  submited(): void {
    const start = this.range.controls.start.value
    const end = this.range.controls.end.value
    if (start == null || end == null) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval za izveštavanje'
      })
      return
    }
    if (start > new Date() || end > new Date()) {
      this.messageService.add({
        key: 'report-message',
        severity: 'error',
        summary: 'Neuspešno',
        detail: 'Izaberite interval u poršlosti za izveštavanje'
      })
      return
    }
    const period: PeriodDto = {
      startString: start,
      endString: end
    }

    if (this.client) {
      this.reportService.clientReport(period).subscribe(
        (res) => {
          this.handleReportResponse(res);
        });
    } else {
      this.reportService.driverReport(period).subscribe(
        (res) => {
          this.handleReportResponse(res);
        });
    }
  }

  handleReportResponse(res: ReportDto): void {
    console.log(res)
    this.reportDto = res
    this.basicDataDrivingNumber = this.setDataOn(this.reportService.convertDateArrayToString(res.labels), res.numberOfDrivings, '#FFA726', 'Broj vožnji');
    this.basicDataDistance = this.setDataOn(this.reportService.convertDateArrayToString(res.labels), res.distancesForDays, '#42A5F5', 'Broj pređenih kilometara');
    this.basicDataMoney = this.setDataOn(this.reportService.convertDateArrayToString(res.labels), res.moneyForDays, '#EC407A', 'Troškovi za vožnje');
  }

}
