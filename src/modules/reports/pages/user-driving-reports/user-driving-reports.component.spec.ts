import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDrivingReportsComponent } from './user-driving-reports.component';

describe('ClientDrivingReportsComponent', () => {
  let component: UserDrivingReportsComponent;
  let fixture: ComponentFixture<UserDrivingReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDrivingReportsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserDrivingReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
