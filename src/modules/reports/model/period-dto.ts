export interface PeriodDto {
  startString: Date
  endString: Date
}
