export interface ReportDto {
  moneyForDays: number[]
  distancesForDays: number[]
  numberOfDrivings: number[]
  labels: number[][]
  sumOfDrivings: number
  averageOfDrivings: number
  sumOfDistances: number
  averageOfDistances: number
  sumOfMoney: number
  averageOfMoney: number
}
