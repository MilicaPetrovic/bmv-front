export interface UserDto {
  username: string
  fullname: string
  profile: string
}
