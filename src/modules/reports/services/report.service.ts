import { Injectable } from '@angular/core';
import {PeriodDto} from "../model/period-dto";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {ReportDto} from "../model/report-dto";
import {HttpClient} from "@angular/common/http";
import {InconsistentReportDto} from "../../shared/models/inconsistent-report-dto";
import {StatusResponseDto} from "../../shared/models/status-response-dto";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private readonly API_PATH = environment.api_path + 'report'

  constructor(private httpClient: HttpClient) { }

  public clientReport(period : PeriodDto): Observable<ReportDto> {
    return this.httpClient.post<ReportDto>(this.API_PATH + '/client', period)
  }

  public driverReport(period : PeriodDto): Observable<ReportDto> {
    return this.httpClient.post<ReportDto>(this.API_PATH + '/driver', period)
  }

  public adminReportForClient(period : PeriodDto, username: string): Observable<ReportDto> {
    return this.httpClient.post<ReportDto>(this.API_PATH + '/admin/client/' + username, period)
  }

  public adminReportForDriver(period : PeriodDto, username: string): Observable<ReportDto> {
    return this.httpClient.post<ReportDto>(this.API_PATH + '/admin/driver/' + username, period)
  }

  public adminReport(period : PeriodDto): Observable<ReportDto> {
    return this.httpClient.post<ReportDto>(this.API_PATH + '/admin', period)
  }

  convertDateArrayToString(labels: number[][]): string[] {
    const res: string[] = []
    for (const l of labels) {
      res.push(l[2] + '.' + l[1] + '.' + l[0])
    }
    return res
  }

  public saveInconsistencyReport(inconsistentReportDto : InconsistentReportDto): Observable<StatusResponseDto> {
    console.log("objekat")
    console.log(inconsistentReportDto);
    return this.httpClient.post<StatusResponseDto>(this.API_PATH + '/inconsistency', inconsistentReportDto);
  }
}
