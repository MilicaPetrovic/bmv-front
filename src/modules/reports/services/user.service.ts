import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {UserDto} from "../model/user-dto";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  api_path = environment.api_path;
  constructor(private http: HttpClient) { }

  getClients() : Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.api_path + "admin/clients")
  }

  getDrivers() : Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.api_path + "admin/drivers")
  }
}
