import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { NominatimResponse } from '../models/nominatim-response.model'
import { map } from 'rxjs/operators'
import { BASE_NOMINATIM_URL, DEFAULT_VIEW_BOX } from '../app.constants'
import {NominatimLatLon} from "../models/nominatim-lat-lon";

@Injectable()
export class NominatimService {
  constructor (private readonly http: HttpClient) {
  }

  addressLookup (req?: string): Observable<NominatimResponse[]> {
    //const url = `https://${BASE_NOMINATIM_URL}/search?format=json&q=${req}&${DEFAULT_VIEW_BOX}&bounded=0`
    const url = `https://${BASE_NOMINATIM_URL}/search?format=json&q=${req}&${DEFAULT_VIEW_BOX}&bounded=0&addressdetails=1`
    console.log(this.http.get(url))

    return this.http
      .get<NominatimLatLon[]>(url).pipe(
      map(
        (data: NominatimLatLon[]) => data.map((item: NominatimLatLon) => new NominatimResponse(item.lat, item.lon, this.makeNewDisplayName(item) ))
      ))
  }

  makeNewDisplayName(item: any): string{
    console.log(item)
    let address = '';
    if (item.address.road !== undefined)
      address += item.address.road + ', ';
    if (item.address.house_number !== undefined)
      address += item.address.house_number + ', ';
    if (item.address.quarter !== undefined)
      address += item.address.quarter + ', ';
    if (item.address.city_district !== undefined)
      address += item.address.city_district + ', ';
    if (item.address.state !== undefined)
      address += item.address.state + ', ';
    if (item.address.country !== undefined)
      address += item.address.country + ', ';
    console.log("ADDRESS ******************************")
    console.log(address)
    return address;
  }
}
