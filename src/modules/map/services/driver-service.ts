import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { MapPoint } from '../../shared/models/map-point'
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class DriverService {
  private readonly baseURL = environment.api_path + 'driver'

  constructor (private readonly http: HttpClient) {
  }

  getActiveDrivers (): Observable<MapPoint[]> {
    return this.http.get<MapPoint[]>(`${this.baseURL}` + '/active')
  }

  getInactiveDrivers (): Observable<MapPoint[]> {
    return this.http.get<MapPoint[]>(`${this.baseURL}` + '/inactive')
  }

  getDrivingDrivers (): Observable<MapPoint[]> {
    return this.http.get<MapPoint[]>(`${this.baseURL}` + '/driving')
  }
}
