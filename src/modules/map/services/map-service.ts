import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { MapPoint } from '../../shared/models/map-point'
import { LatLng } from 'leaflet'
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class MapService {
  private readonly baseURL = environment.api_path + 'map'

  constructor (private readonly http: HttpClient) {
  }

  saveSelectedRoute (stations: MapPoint[], coordinates: LatLng[]): Observable<string> {
    const body = {
      stations,
      coordinates
    }
    console.log('body')
    console.log(body)
    return this.http.post<string>(`${this.baseURL}` + '/directions', body)
  }
}
