import { Injectable } from '@angular/core'
import { RouteData } from '../../../shared/models/route-data'
import { AlternativeRoute } from '../../models/alternative-route'
import * as _ from 'lodash'

@Injectable({
  providedIn: 'root'
})
export class RouteService {
  getSelectedRoute (foundRoutes: RouteData[] = [], selectedColor: string): RouteData {
    for (const route of foundRoutes) {
      if (route.color === selectedColor) {
        return route
      }
    }
    return new RouteData()
  }

  getOptimalTimeRoute (foundRoutes: RouteData[]):RouteData {
    let optimal = foundRoutes[0]
    for (const r of foundRoutes) {
      if (r.totalTime < optimal.totalTime) {
        optimal = r
      }
    }
    return optimal
  }

  getOptimalDistanceRoute (foundRoutes: RouteData[]):RouteData {
    let optimal = foundRoutes[0]
    for (const r of foundRoutes) {
      if (r.totalDistance < optimal.totalDistance) {
        optimal = r
      }
    }
    return optimal
  }

  getOptimalMoneyRoute (foundRoutes: RouteData[]):RouteData {
    let optimal = foundRoutes[0]
    for (const r of foundRoutes) {
      if (r.price < optimal.price) {
        optimal = r
      }
    }
    return optimal
  }

  public routeDataIsNotEmpty (optimal: RouteData):boolean {
    console.log('provera optimala')
    console.log(optimal)
    if (optimal.totalDistance !== 0 && optimal.totalTime !== 0) {
      console.log('vrati true')
      return true
    }
    console.log('vrati false')
    return false
  }

  public permutations (alternativeRoutes: AlternativeRoute[], k: number):RouteData[] {
    let list_combinations: any = []
    for (let i = 0; i < (Math.pow(alternativeRoutes.length, k)); i++) {
      // Convert i to len th base
      const res: AlternativeRoute[] = this.createPermutation(i, alternativeRoutes, alternativeRoutes.length, k)
      list_combinations.push(_.cloneDeep(res))
    }
    console.log(list_combinations.length)
    list_combinations = this.filterAllPossiblePermutations(list_combinations)

    return this.formAlternativeRoutes(list_combinations)
  }

  private createPermutation (n: number, alternativeRoutes: AlternativeRoute[], length: number, k: number):AlternativeRoute[]  {
    const list: AlternativeRoute[] = []
    for (let i = 0; i < k; i++) {
      list.push(_.cloneDeep(alternativeRoutes[n % length]))
      n = Number((n / length).toFixed(0))
    }
    return list
  }

  private filterAllPossiblePermutations (list_combinations: any) {
    const candidatesForDelete: any = []
    for (const a of list_combinations) {
      for (let i = 0; i < a.length - 1; i++) {
        if (a[i].id > a[i + 1].id || a[i].id === a[i + 1].id) {
          candidatesForDelete.push(a)
        }
      }
    }
    for (const candidate of candidatesForDelete) {
      const index = list_combinations.indexOf(candidate, 0)
      if (index > -1) {
        list_combinations.splice(index, 1)
      }
    }
    console.log('uspesne rute')
    for (const a of list_combinations) {
      console.log(a)
    }
    return list_combinations
  }

  private formAlternativeRoutes (list_combinations: any[]):RouteData[] {
    const foundRoutes: RouteData[] = []
    for (const alternativeRoute of list_combinations) {
      const routeData = new RouteData(alternativeRoute)
      foundRoutes.push(routeData)
      console.log(routeData)
    }
    return foundRoutes
  }
}
