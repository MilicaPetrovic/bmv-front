import { Routes } from '@angular/router'
import { MapComponent } from './pages/map/map.component'

export const MapRoutes: Routes = [
  {
    path: 'home',
    pathMatch: 'full',
    component: MapComponent
  }
]
