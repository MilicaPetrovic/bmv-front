import { RouteData } from '../../shared/models/route-data'

export class AlternativeRoute {
  id: number
  route: RouteData

  constructor (id: number, route: RouteData) {
    this.id = id
    this.route = route
  }
}
