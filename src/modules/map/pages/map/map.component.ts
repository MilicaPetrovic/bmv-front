import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core'
import * as L from 'leaflet'
import {icon, latLng, Map, MapOptions, marker, tileLayer} from 'leaflet'
import {DEFAULT_LATITUDE, DEFAULT_LONGITUDE, COLORS} from '../../app.constants'
import 'leaflet-routing-machine'
import {NominatimResponse} from '../../models/nominatim-response.model'
import {MapPoint} from '../../../shared/models/map-point'
import {MapService} from '../../services/map-service'
import {DriverService} from '../../services/driver-service'
import {RouteData} from '../../../shared/models/route-data'
import {DrivingDto} from '../../../shared/models/driving-dto'
import {TreeNodeSearch} from '../../../shared/models/tree-node-search'

import {DrivingService} from '../../../registered-user/services/driving/driving.service'
import {RouteService} from '../../services/route-service/route.service'
import {AlternativeRoute} from '../../models/alternative-route'
import {MessageService} from "primeng/api";
import {RouteViewDto} from "../../../shared/models/route-view-dto";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [MessageService]
})
export class MapComponent implements OnInit, OnChanges {
  @Input() selectedOptimal: TreeNodeSearch
  @Input() favouriteRoute : RouteViewDto
  @Output() optimalRoute = new EventEmitter()
  @Output() resetEvent = new EventEmitter()

  map: Map
  mapPointStations: MapPoint[] = []
  options: MapOptions
  lastLayer: L.Layer[] = []

  results: NominatimResponse[]
  activeDrivers: MapPoint[] = []
  drivingDrivers: MapPoint[] = []
  polylines: L.Polyline[] = []
  foundRoutes: RouteData[] = []
  selectedRoute: RouteData
  selectedColor = ''
  alternativeRoutes: AlternativeRoute[] = []

  constructor(private readonly mapService: MapService,
              private readonly driverService: DriverService,
              private readonly drivingService: DrivingService,
              private messageService: MessageService,
              private readonly routeService: RouteService) {
  }

  ngOnInit():void {
    this.initializeMapOptions()
  }

  initializeMap(map: Map):void {
    console.log('initialize map')
    console.log(this.activeDrivers)
    this.map = map
    this.createMarker()
    this.initializeActiveDrivingDrivers()
  }

  getAddress(result: NominatimResponse):void {
    if (!this.existInMapPointStations(result.displayName)) {
      this.addMapPoint(result.latitude, result.longitude, result.displayName)
      this.createMarker()
    } else {
      this.messageService.add({
        key: 'map-point-message',
        severity: 'error',
        summary: 'Stanica je već dodata',
        detail: 'Izabrana stanica je već deo putanje.',
        life: 5000
      })
    }
  }

  existInMapPointStations(name: string):boolean {
    for (const mapPoint of this.mapPointStations) {
      if (mapPoint.name === name) {
        return true
      }
    }
    return false;
  }

  refreshSearchList(results: NominatimResponse[]):void {
    this.results = results
  }

  private initializeMapOptions():void {
    this.options = {
      zoom: 13,
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18, attribution: 'OSM'})
      ]
    }
    this.lastLayer.push(tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: 'OSM'
    }))
  }

  // za servis
  private addMapPoint(latitude: number, longitude: number, name: string):void {
    this.mapPointStations.push(new MapPoint(name, latitude, longitude))
  }

  private createMarker():void {
    //this.clearMap()
    this.map.setView([DEFAULT_LATITUDE, DEFAULT_LONGITUDE], this.map.getZoom())

    const mapIcon = this.getDefaultIcon()
    for (const station of this.mapPointStations) {
      const coordinates2 = latLng([station.lat ? station.lat : 45.00,
        station.lng ? station.lng : 19.2331])

      this.lastLayer.push(marker(coordinates2).setIcon(mapIcon).addTo(this.map))
      this.map.setView(coordinates2, this.map.getZoom())
    }
  }

  private initializeActiveDrivingDrivers():void {
    this.driverService.getActiveDrivers().subscribe(data => {
      this.activeDrivers = data
      console.log('data')
      console.log(data)
      console.log('nakon data drivers')
      console.log(this.activeDrivers)
      this.addActiveDrivers(data)
    })

    this.driverService.getDrivingDrivers().subscribe(data => {
      this.drivingDrivers = data
      this.addDrivingDrivers(data)
    })

    console.log('active drivers')
    console.log(this.activeDrivers)

    console.log('driving drivers')
    console.log(this.drivingDrivers)
  }

  private addActiveDrivers(data: MapPoint[]):void {
    const mapIcon = this.getFreeCarIcon()
    for (const driver of data) {
      const coordinates2 = latLng([driver.lat, driver.lng])
      this.lastLayer.push(marker(coordinates2).setIcon(mapIcon).addTo(this.map))
      this.map.setView(coordinates2, this.map.getZoom())
    }
  }

  private addDrivingDrivers(data: MapPoint[]):void {
    const mapIcon = this.getBookedCarIcon()
    for (const driver of data) {
      const coordinates2 = latLng([driver.lat, driver.lng])
      this.lastLayer.push(marker(coordinates2).setIcon(mapIcon).addTo(this.map))
      this.map.setView(coordinates2, this.map.getZoom())
    }
  }

  public refreshMap(updatedStations: MapPoint[]):void {
    this.mapPointStations = updatedStations
    this.createMarker()
    this.getDirections()
  }

  private getDefaultIcon() {
    return icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/marker/marker-icon.png'
    })
  }

  private getFreeCarIcon() {
    return icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/marker/unbooked.png'
    })
  }

  private getBookedCarIcon() {
    return icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'assets/marker/booked.png'
    })
  }

  public resetAll():void {
    this.mapPointStations = []
    this.foundRoutes = []
    this.alternativeRoutes = []
    this.polylines.forEach((polyline) => {
      this.map.removeLayer(polyline)
    })
    this.clearMap()
    this.resetEvent.emit();

  }

  public clearMap():void {
    const numOfLayers = this.lastLayer.length
    for (let i = 1; i < numOfLayers; i++) {
      if (this.map.hasLayer(this.lastLayer[i])) {
        this.map.removeLayer(this.lastLayer[i])
      }
    }
    this.polylines.forEach((polyline) => {
      this.map.removeLayer(polyline)
    })
    this.foundRoutes = []
    this.alternativeRoutes = []
    this.addDrivingDrivers(this.drivingDrivers)
    this.addActiveDrivers(this.activeDrivers)
    this.createMarker()
  }

  // servis
  getDirections():void {

   this.clearMap()

    if (this.mapPointStations.length > 2) {
      this.getAlternativeDirection(true)
    } else {
      this.getAlternativeDirection(false)
    }

  }

  ngOnChanges(changes: SimpleChanges):void {
    console.log('on changes')
    console.log(changes)
    console.log(changes['favouriteRoute'])
    let drivingDto: DrivingDto | null
    let optimal: RouteData = new RouteData()

    if (changes['selectedOptimal']) {
      console.log('skonta koji se menja')

      if (this.selectedOptimal.label === 'po vremenu') {
        console.log('po vremenu')
        optimal = this.routeService.getOptimalTimeRoute(this.foundRoutes)
      } else if (this.selectedOptimal.label === 'po razdaljini') {
        optimal = this.routeService.getOptimalDistanceRoute(this.foundRoutes)
      } else if (this.selectedOptimal.label === 'ručno') {
        console.log('****rucnoooo*****')
        optimal = this.routeService.getSelectedRoute(this.foundRoutes, this.selectedColor)
      } else if (this.selectedOptimal.label === 'po ceni') {
        optimal = this.routeService.getOptimalMoneyRoute(this.foundRoutes)
      }
      if (this.routeService.routeDataIsNotEmpty(optimal)) {
        drivingDto = new DrivingDto(this.mapPointStations, optimal, false)
        console.log('pre emita')
        console.log(drivingDto)
        this.optimalRoute.emit(drivingDto)
      }
    }
    console.log("stigne ovde")
    console.log(this.favouriteRoute)
    if(this.favouriteRoute !== undefined){
      console.log(" changes favouriteeeeeeeee")
      this.drawFavourite()
    }
  }

  private handClickOnRoute():void {
    const optimal = this.routeService.getSelectedRoute(this.foundRoutes, this.selectedColor)
    if (this.routeService.routeDataIsNotEmpty(optimal)) {
      const drivingDto = new DrivingDto(this.mapPointStations, optimal, false)
      console.log('pre emita')
      console.log(drivingDto)
      this.optimalRoute.emit(drivingDto)
    }
  }

  private getAlternativeDirection(isAlternative: boolean) :void{
    console.log('alternativna putanja')
    for (let i = 0; i < this.mapPointStations.length - 1; i++) {
      const routes = L.Routing.control({
        waypoints: this.getStationsForRoutingStartEnd(this.mapPointStations[i], this.mapPointStations[i + 1]),
        showAlternatives: true
      }).addTo(this.map)

      routes.on('routesfound', (e) => {
        console.log('rute ' + i)
        const routes = e.routes
        console.log(routes)
        for (const r of routes) {
          if (isAlternative) {
            this.alternativeRoutes.push(new AlternativeRoute(i, new RouteData(r.coordinates, r.summary.totalTime, r.summary.totalDistance)))
          } else {
            this.foundRoutes.push(new RouteData(r.coordinates, r.summary.totalTime, r.summary.totalDistance))
          }
        }
        if (i === this.mapPointStations.length - 2) {
          if (isAlternative) {
            this.foundRoutes = this.routeService.permutations(this.alternativeRoutes, this.mapPointStations.length - 1)
          }
          this.drawAlternative()
        }
      })
      if (routes !== undefined) {
        routes.remove()
      }
    }
    console.log('alternativne pre spajanja')
    console.log(this.alternativeRoutes)
  }

  private drawAlternative() :void{
    for (let i = 0; i < this.foundRoutes.length; i++) {
      this.foundRoutes[i].color = COLORS[i]
      const polyline = L.polyline((this.foundRoutes)[i].coordinates, {color: (this.foundRoutes)[i].color}).addTo(this.map)
      this.polylines.push(polyline)
      polyline.on('click', (e) => {
        console.log('eeeeeeeeeeeeeeeee')
        console.log(e)
        this.selectedColor = e.target.options.color
        console.log('bojaaa')
        console.log(e.target.options.color)
        this.handClickOnRoute()

      })
    }
  }

  private drawFavourite():void{
    this.mapPointStations = []
    this.foundRoutes = []
    this.alternativeRoutes = []
    this.polylines.forEach((polyline) => {
      this.map.removeLayer(polyline)
    })
    this.clearMap()
    this.mapPointStations = this.favouriteRoute.stations
    this.createMarker()
    const polyline = L.polyline(this.favouriteRoute.coordinates, { color: 'red' }).addTo(this.map)
    this.polylines.push(polyline)
  }


  private getStationsForRoutingStartEnd(start: MapPoint, end: MapPoint) : L.LatLng[] {
    console.log('map point 2')
    const allStations = []
    allStations.push(L.latLng(start.lat, start.lng))
    allStations.push(L.latLng(end.lat, end.lng))
    return allStations
  }
}
