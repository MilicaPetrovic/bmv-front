import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ChosenStationsComponent } from './components/chosen-stations/chosen-stations.component'
import { GeocodingComponent } from './components/geocoding/geocoding.component'
import { MapPointFormComponent } from './components/map-point-form/map-point-form.component'
import { ResultsListComponent } from './components/results-list/results-list.component'
import { MapComponent } from './pages/map/map.component'
import { DriverService } from './services/driver-service'
import { MapService } from './services/map-service'
import { NominatimService } from './services/nominatim-service'
import { LeafletModule } from '@asymmetrik/ngx-leaflet'
import { PanelModule } from 'primeng/panel'
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { MapRoutes } from './map.routes'
import { ButtonModule } from 'primeng/button'
import { MatButtonModule } from '@angular/material/button'
import {ToastModule} from "primeng/toast";

@NgModule({
  declarations: [
    ChosenStationsComponent,
    GeocodingComponent,
    MapPointFormComponent,
    ResultsListComponent,
    MapComponent
  ],
  imports: [
    CommonModule,
    LeafletModule,
    PanelModule,
    ReactiveFormsModule,
    RouterModule.forChild(MapRoutes),
    ButtonModule,
    MatButtonModule,
    ToastModule
  ],
  providers: [
    DriverService,
    MapService,
    NominatimService
  ],
  exports: [
    MapComponent
  ]
})
export class MapModule { }
