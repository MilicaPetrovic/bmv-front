export const BASE_NOMINATIM_URL = 'nominatim.openstreetmap.org'
export const DEFAULT_VIEW_BOX = 'viewbox=-25.0000%2C70.0000%2C50.0000%2C40.0000'
export const DEFAULT_LATITUDE = 45.24545
export const DEFAULT_LONGITUDE = 19.83223
export const DEFAULT_NAME = 'Novi Sad'
export const DEFAULT_PRICE_PER_KM = 120
export const  COLORS = ['red', 'green', 'blue', 'orange', 'purple']
export const COLORS_SERBIAN = ['crvena', 'zelena', 'plava', 'natandžasta', 'ljubičasta']
