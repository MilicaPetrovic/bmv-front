import { Component, EventEmitter, Output } from '@angular/core'
import { NominatimResponse } from '../../models/nominatim-response.model'
import { NominatimService } from '../../services/nominatim-service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { distinctUntilChanged, debounceTime } from 'rxjs/operators'

@Component({
  selector: 'app-geocoding',
  templateUrl: './geocoding.component.html',
  styleUrls: ['./geocoding.component.css']
})
export class GeocodingComponent {
  @Output() search = new EventEmitter()
  @Output() locationSelect = new EventEmitter()
  searchResults: NominatimResponse[]

  value = ''
  searchForm: FormGroup
  searchSubscription!: Subscription

  constructor (private readonly fb: FormBuilder, private readonly nominatimService: NominatimService) {
    this.searchForm = this.fb.group({
      search: ['', [Validators.required]]
    })

    // @ts-ignore
    this.searchSubscription = this.searchForm.get('search')
      .valueChanges.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(() => {
        this.findAddress(this.searchForm.value.search)
      })
  }

  findAddress (address: string) :void {
    console.log('USLOOOOOOO.....')
    console.log(address)
    this.nominatimService.addressLookup(address).subscribe(results => {
      console.log("RESULTATI -++++++++++++++++++++++++++++++++++++++++++++++++")
      console.log(results);
      this.searchResults = results
      this.search.emit(this.searchResults)
    })
  }
}
