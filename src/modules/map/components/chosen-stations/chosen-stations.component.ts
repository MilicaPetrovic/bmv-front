import { Component, EventEmitter, Input, Output } from '@angular/core'
import { MapPoint } from '../../../shared/models/map-point'

@Component({
  selector: 'app-chosen-stations',
  templateUrl: './chosen-stations.component.html',
  styleUrls: ['./chosen-stations.component.css']
})
export class ChosenStationsComponent{
  @Input() chosenStations: MapPoint[]

  @Output() locationDelete = new EventEmitter()


  draggingIndex: number

  onDragStart (fromIndex: number): void {
    this.draggingIndex = fromIndex
  }

  onDragEnter (toIndex: number): void {
    if (this.draggingIndex !== toIndex) {
      this._reorderItem(this.draggingIndex, toIndex)
    }
  }

  onDragEnd (): void {
  //  this.draggingIndex ;
  }

  private _reorderItem (fromIndex: number, toIndex: number): void {
    const itemToBeReordered = this.chosenStations.splice(fromIndex, 1)[0]
    this.chosenStations.splice(toIndex, 0, itemToBeReordered)
    this.draggingIndex = toIndex
  }

  deleteCurrent (index: number) :void{
    this.chosenStations.splice(index, 1)
    this.locationDelete.emit(this.chosenStations)
  }
}
