import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ChosenStationsComponent } from './chosen-stations.component'

describe('ChosenStationsComponent', () => {
  let component: ChosenStationsComponent
  let fixture: ComponentFixture<ChosenStationsComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChosenStationsComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(ChosenStationsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
