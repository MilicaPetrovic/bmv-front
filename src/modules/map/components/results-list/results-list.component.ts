
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { NominatimResponse } from '../../models/nominatim-response.model'

@Component({
  selector: 'app-results-list',
  templateUrl: './results-list.component.html',
  styleUrls: ['./results-list.component.css']
})
export class ResultsListComponent {
  @Input()
    results: NominatimResponse[] | undefined

  @Output()
    locationSelected = new EventEmitter()


  selectResult (result: NominatimResponse):void {
    this.locationSelected.emit(result)
  }
}
