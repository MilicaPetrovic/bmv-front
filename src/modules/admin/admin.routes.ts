import { Routes } from '@angular/router'
import { AdminUsersComponent } from './pages/admin-users/admin-users.component'
import { RoleGuard } from '../auth/guards/role/role.guard'
import { AdminDriversComponent } from './pages/admin-drivers/admin-drivers.component'
import { AddNewDriverComponent } from './pages/add-new-driver/add-new-driver.component'

export const AdminRoutes: Routes = [
  {
    path: 'users',
    pathMatch: 'full',
    component: AdminUsersComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'admin' }
  },
  {
    path: 'driver-requests',
    pathMatch: 'full',
    component: AdminDriversComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'admin' }
  },
  {
    path: 'add-driver',
    pathMatch: 'full',
    component: AddNewDriverComponent,
    canActivate: [RoleGuard],
    data: { expectedRoles: 'admin' }
  }
]
