import { Component, OnInit } from '@angular/core';
import {
  AbstractControl, FormBuilder,
  FormControl, FormGroup,
  FormGroupDirective,
  NgForm,
  ValidatorFn,
  Validators
} from '@angular/forms'
import { ErrorStateMatcher } from '@angular/material/core'
import { RegisterService } from 'src/modules/auth/services/register/register.service';
import { MessageService } from 'primeng/api'

@Component({
  selector: 'app-add-new-driver',
  templateUrl: './add-new-driver.component.html',
  styleUrls: ['./add-new-driver.component.css'],
  providers: [MessageService]
})
export class AddNewDriverComponent implements OnInit {

  emailFormControl = new FormControl('', [Validators.required, Validators.email])
  passwordFormControl = new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')])
  nameFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]*'), Validators.minLength(3), Validators.maxLength(20)])
  surnameFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]*'), Validators.minLength(3), Validators.maxLength(20)])
  phoneFormControl = new FormControl('', [Validators.required, Validators.pattern('[\+][0-9]{6,13}'), Validators.minLength(6), Validators.maxLength(13)])
  usernameFormControl = new FormControl('', [Validators.required, Validators.pattern('^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$')])
  cityFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Za-z ]*'), Validators.minLength(2), Validators.maxLength(30)])
  postNumberFormControl = new FormControl('', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{4}')])
  vehicleNameFormControl = new FormControl('', [Validators.required])
  colorFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+')])
  labelFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+')])
  petFriendlyFormControl = new FormControl(false)
  babyFriendlyFormControl = new FormControl(false)
  yearOfProductionFormControl = new FormControl('', [Validators.required, Validators.pattern('[1-2]{1}[0-9]{3}')])
  numberOfSeatsFormControl = new FormControl('', [Validators.required, Validators.pattern('[0-9]+')])
  priceFormControl = new FormControl('', [Validators.required, Validators.pattern('[0-9]+')])
  vehicleTypeFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-Z][a-z]+')])
  password2FormControl: FormControl
  registerFormGroup: FormGroup

  matcher = new MyErrorStateMatcher()
  error: string | null

  constructor(private readonly registerService: RegisterService,
              private readonly messageService: MessageService,
              private readonly fb: FormBuilder)
  {
    this.registerFormGroup = fb.group({
      email: this.emailFormControl,
      password: this.passwordFormControl,
      name: this.nameFormControl,
      surname: this.surnameFormControl,
      phoneNumber: this.phoneFormControl,
      username: this.usernameFormControl,
      password2: this.password2FormControl,
      city: this.cityFormControl,
      postNumber: this.postNumberFormControl,
      vehicleName: this.vehicleNameFormControl,
      color: this.colorFormControl,
      label: this.labelFormControl,
      petFriendly: this.petFriendlyFormControl,
      babyFriendly: this.babyFriendlyFormControl,
      yearOfProduction: this.yearOfProductionFormControl,
      numberOfSeats: this.numberOfSeatsFormControl,
      price: this.priceFormControl,
      vehicleType: this.vehicleTypeFormControl
    })
  }

  ngOnInit(): void {
    this.password2FormControl = new FormControl('', [Validators.required, this.createPasswordMatchingValidator(this.passwordFormControl), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')])
  }

  submit() : void{
    if (!this.registerFormGroup.valid)
      return;
    console.log(this.registerFormGroup.value)
    this.registerService.registerDriver(this.registerFormGroup.value).subscribe(
      (result) => {
        if (result.successful) {
          this.messageService.add({ key: 'registration-message', severity: 'success', summary: 'Uspešna registracija', detail: 'Uspešno ste registrovali novog vozača.' })
          this.registerFormGroup.reset()
          this.password2FormControl.reset()
        } else {
          if (result.exceptionCode == 4) { this.messageService.add({ key: 'registration-message', severity: 'error', summary: 'Neuspešna registracija', detail: 'Korisničko ime ili email adresa su već zauzeti.', life: 5000 }) } else if (result.exceptionCode == 7) { this.messageService.add({ key: 'registration-message', severity: 'error', summary: 'Neuspešna registracija', detail: 'Nismo uspeli da Vas registrujemo.\nMolimo Vas popunite formu validnim podacima.', life: 5000 }) }
        }
      }
    )
  }

  private createPasswordMatchingValidator (passwordFormControl: FormControl): ValidatorFn {
    {
      return (control: AbstractControl): Record<string, string> | null =>
        control.value === passwordFormControl.value
          ? null
          : { wrongValue: control.value }
    }
  }


}

class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState (control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = (form != null) && form.submitted
    return !!((control != null) && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}
