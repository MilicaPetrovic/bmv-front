import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { statusDTO } from '../../../shared/models/status-dto'
import { environment } from '../../../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { WarningNote } from '../../models/warning-note'
import { DetailedDrivingDto } from '../../models/detailed-driving-dto'

@Injectable({
  providedIn: 'root'
})
export class AdminUsersService {
  private readonly API_PATH = environment.api_path + 'admin'

  constructor (private readonly httpClient: HttpClient) {
  }

  toggleBlockOfUser (username: string): Observable<statusDTO> {
    return this.httpClient.get<statusDTO>(`${this.API_PATH}/toggle/block/${username}`)
  }

  addWarning (username: string, text: string): Observable<WarningNote[]> {
    const textDto = {
      text
    }
    return this.httpClient.post<WarningNote[]>(`${this.API_PATH}/add/warning/${username}`, textDto)
  }

  getWarningsByUser (clientId: string): Observable<WarningNote[]> {
    return this.httpClient.get<WarningNote[]>(`${this.API_PATH}/get/user/warnings/${clientId}`)
  }

  getDriving (drivingId: number): Observable<DetailedDrivingDto> {
    return this.httpClient.get<DetailedDrivingDto>(`${this.API_PATH}/driving/${drivingId}`)
  }

}
