import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { PaginationDTO } from '../../../shared/models/pagination-dto'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { AdminDriverDriving } from '../../models/admin-driver-driving'
import { VehicleDto } from '../../models/vehicle-dto'
import { ProfileChangeRequest } from 'src/modules/admin/models/profile-change-request'
import { StatusResponseDto } from 'src/modules/shared/models/status-response-dto'

@Injectable({
  providedIn: 'root'
})
export class AdminDriversService {
  private readonly API_PATH = environment.api_path + 'admin'
  authHeader: HttpHeaders

  constructor (private readonly httpClient: HttpClient, private readonly userService: AuthService) {
    this.authHeader = this.userService.getAuthHeader()
  }

  getUsers (from: number | undefined, until: number | undefined): Observable<PaginationDTO<AdminUserBasic>> {
    return this.httpClient.get<PaginationDTO<AdminUserBasic>>(`${this.API_PATH}/get/drivers/${from}/${until}`,
      { headers: this.authHeader }
    )
  }

  getDrivingsByUser (username: string, page: number, row: number): Observable<PaginationDTO<AdminDriverDriving>> {
    return this.httpClient.get<PaginationDTO<AdminDriverDriving>>(`${this.API_PATH}/get/driver/drivings/${username}/${page}/${row}`, {
      headers: this.authHeader
    })
  }

  getVehicle (username: string) : Observable<VehicleDto>{
    return this.httpClient.get<VehicleDto>(`${this.API_PATH}/driver/vehicle/${username}`, {
      headers: this.authHeader
    })
  }

  getProfileChangeRequests(from: number | undefined, until: number | undefined): Observable<PaginationDTO<ProfileChangeRequest>> {
    return this.httpClient.get<PaginationDTO<ProfileChangeRequest>>(`${this.API_PATH}/get/profile/change/requests/${from}/${until}`, {
      headers: this.authHeader
    })
  }

  updateDriver(profileChangeRequest: ProfileChangeRequest): Observable<StatusResponseDto> {
    return this.httpClient.put<StatusResponseDto>(`${this.API_PATH}/update/driver`, profileChangeRequest, {
      headers: this.authHeader
    })
  }

}
