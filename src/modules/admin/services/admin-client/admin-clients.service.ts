import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { environment } from '../../../../environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AdminClientDriving } from '../../models/admin-client-driving'
import { AuthService } from '../../../auth/services/auth/auth.service'
import { PaginationDTO } from '../../../shared/models/pagination-dto'

@Injectable({
  providedIn: 'root'
})
export class AdminClientsService {
  private readonly API_PATH = environment.api_path + 'admin'
  authHeader: HttpHeaders

  constructor (private readonly httpClient: HttpClient, private readonly userService: AuthService) {
    this.authHeader = this.userService.getAuthHeader()
  }

  getUsers (from: number | undefined, until: number | undefined): Observable<PaginationDTO<AdminUserBasic>> {
    return this.httpClient.get<PaginationDTO<AdminUserBasic>>(`${this.API_PATH}/get/clients/${from}/${until}`,
      { headers: this.authHeader }
    )
  }

  getDrivingsByUser (clientId: string, from: number | undefined, until: number | undefined): Observable<PaginationDTO<AdminClientDriving>> {
    return this.httpClient.get<PaginationDTO<AdminClientDriving>>(`${this.API_PATH}/get/client/drivings/${clientId}/${from}/${until}`, {
      headers: this.authHeader
    })
  }
}
