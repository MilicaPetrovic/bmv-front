import { Component, Inject, OnInit } from '@angular/core'
import { WarningNote } from '../../models/warning-note'
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { LazyLoadEvent } from 'primeng/api'
import { AdminDriversService } from '../../services/admin-driver/admin-drivers.service'
import { AdminDriverDriving } from '../../models/admin-driver-driving'
import {AdminDrivingDetailedComponent} from "../admin-driving-detailed/admin-driving-detailed.component";

@Component({
  selector: 'app-admin-driver-detailed',
  templateUrl: './admin-driver-detailed.component.html',
  styleUrls: ['./admin-driver-detailed.component.css']
})
export class AdminDriverDetailedComponent implements OnInit {
  warnings: WarningNote[]
  drivings: AdminDriverDriving[]
  drivingLoading = false
  drivingTotalRecords = 1
  drivingRow = 1

  constructor (
    private readonly adminDriversService: AdminDriversService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AdminDriverDetailedComponent>,
    @Inject(MAT_DIALOG_DATA) public client: AdminUserBasic
  ) { }

  ngOnInit (): void {
    this.getDrivings(0, this.drivingRow)
  }

  nextDrivingPage (event: LazyLoadEvent) : void {
    setTimeout(() => {
      this.drivingLoading = true
      if (event.first === undefined) { event.first = 0 }
      if (event.rows === undefined) { event.rows = 5 }
      this.getDrivings(event.first / event.rows, event.rows)
    }, 500)
  }

  private getDrivings (number: number, drivingRow: number) {
    this.drivingLoading = true
    this.adminDriversService.getDrivingsByUser(this.client.username, number, drivingRow).subscribe(data => {
      this.drivingTotalRecords = data.totalCount
      this.drivings = data.resultList
      this.drivingLoading = false
    })
  }

  openDialogForDriving (id: number) : void {
    const dialogRef = this.dialog.open(AdminDrivingDetailedComponent, {
      width: '70%',
      height: '95%',
      data: id
    })

    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed')
    })
  }
}
