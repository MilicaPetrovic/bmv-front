import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminDriverDetailedComponent } from './admin-driver-detailed.component'

describe('AdminDriverDetailedComponent', () => {
  let component: AdminDriverDetailedComponent
  let fixture: ComponentFixture<AdminDriverDetailedComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminDriverDetailedComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(AdminDriverDetailedComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
