import { Component, Input, OnInit } from '@angular/core'
import { VehicleDto } from '../../models/vehicle-dto'
import { AdminDriversService } from '../../services/admin-driver/admin-drivers.service'

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  @Input() driverUsername = ''
  vehicle: VehicleDto

  constructor (private readonly adminDriversService: AdminDriversService) {
  }

  ngOnInit (): void {
    this.adminDriversService.getVehicle(this.driverUsername).subscribe(
      res => {
        this.vehicle = res
        console.log('RES')
        console.log(res)
      }
    )
  }
}
