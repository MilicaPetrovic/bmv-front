import { Component, Input, OnInit } from '@angular/core'
import { AdminClientsService } from '../../services/admin-client/admin-clients.service'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { LazyLoadEvent } from 'primeng/api'
import { AdminUsersService } from '../../services/admin-users/admin-users.service'
import { AdminDetailedUserComponent } from '../admin-client-detailed/admin-detailed-user.component'
import { MatDialog } from '@angular/material/dialog'
import { AdminDriversService } from '../../services/admin-driver/admin-drivers.service'
import { AdminDriverDetailedComponent } from '../admin-driver-detailed/admin-driver-detailed.component'

@Component({
  selector: 'app-admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.css']
})
export class AdminUsersListComponent implements OnInit {
  clients: AdminUserBasic[]
  loading = false
  totalRecords = 0
  rowCount = 1
  @Input() isClients: boolean

  constructor (
    private readonly adminClientService: AdminClientsService,
    private readonly adminUsersService: AdminUsersService,
    public dialog: MatDialog,
    private readonly adminDriversService: AdminDriversService) { }

  ngOnInit (): void {
    this.loading = true
    this.getUsers(0, this.rowCount)
  }

  nextPage (event: LazyLoadEvent) : void {
    // this.loading = true;
    // event.first = 0
    // event.rows = 3
    // event.sortField ='' ;
    // event.sortOrder = -1;
    // filters:{}

    setTimeout(() => {
      this.loading = true
      if (event.first === undefined) { event.first = 0 }
      if (event.rows === undefined) { event.rows = 5 }
      this.getUsers(event.first / event.rows, event.rows)
    }, 500)
  }

  blockUser (event: MouseEvent, username: string) : void {
    event.stopPropagation()
    this.adminUsersService.toggleBlockOfUser(username).subscribe(data => {
      if (data.successful) {
        for (const cl of this.clients) {
          if (cl.username == username) {
            cl.blocked = !cl.blocked
          }
        }
      } else {
        console.log('SOMETHING HAPPENED')
      }
    })
  }

  openDialog (client: AdminUserBasic) : void {
    let dialogRef
    if (this.isClients) {
      dialogRef = this.dialog.open(AdminDetailedUserComponent, {
        width: '90%',
        height: '90%',
        data: client
      })
    } else {
      dialogRef = this.dialog.open(AdminDriverDetailedComponent, {
        width: '90%',
        height: '90%',
        data: client
      })
    }

    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed')
    })
  }

  private getUsers (number: number, rowCount: number) {
    if (this.isClients) {
      this.adminClientService.getUsers(number, rowCount).subscribe(data => {
        this.totalRecords = data.totalCount
        this.clients = data.resultList
        this.loading = false
      })
    } else {
      this.adminDriversService.getUsers(number, rowCount).subscribe(data => {
        this.totalRecords = data.totalCount
        this.clients = data.resultList
        this.loading = false
      })
    }
  }
}
