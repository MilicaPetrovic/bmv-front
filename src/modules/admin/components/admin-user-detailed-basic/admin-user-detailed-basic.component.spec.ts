import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminUserDetailedBasicComponent } from './admin-user-detailed-basic.component'

describe('AdminUserDetailedBasicComponent', () => {
  let component: AdminUserDetailedBasicComponent
  let fixture: ComponentFixture<AdminUserDetailedBasicComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminUserDetailedBasicComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(AdminUserDetailedBasicComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
