import { Component, Input } from '@angular/core'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { AdminUsersService } from '../../services/admin-users/admin-users.service'

@Component({
  selector: 'app-admin-user-detailed-basic',
  templateUrl: './admin-user-detailed-basic.component.html',
  styleUrls: ['./admin-user-detailed-basic.component.css']
})
export class AdminUserDetailedBasicComponent {
  @Input()
    user: AdminUserBasic

  constructor (private readonly adminUserService: AdminUsersService) { }

  blockUser (username: string) : void {
    this.adminUserService.toggleBlockOfUser(username).subscribe(data => {
      if (data.successful) {
        this.user.blocked = !this.user.blocked
      } else {
        console.log('SOMETHING HAPPENED')
      }
    })
  }
}
