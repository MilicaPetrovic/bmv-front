import {Component, Inject, OnInit} from '@angular/core';
import {DetailedDrivingDto} from "../../models/detailed-driving-dto";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AdminUsersService} from "../../services/admin-users/admin-users.service";

@Component({
  selector: 'app-admin-driving-detailed',
  templateUrl: './admin-driving-detailed.component.html',
  styleUrls: ['./admin-driving-detailed.component.css']
})
export class AdminDrivingDetailedComponent implements OnInit {

  driving: DetailedDrivingDto

  constructor(
    private readonly drivingService: AdminUsersService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AdminDrivingDetailedComponent>,
    @Inject(MAT_DIALOG_DATA) public drivingId: number) { }

  ngOnInit(): void {
    this.drivingService.getDriving(this.drivingId).subscribe(
      res => {
        this.driving = res
      },
      err => {
        console.log('FAIL')
        console.log(err)
      }
    )
  }

}
