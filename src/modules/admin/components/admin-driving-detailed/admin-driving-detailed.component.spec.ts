import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDrivingDetailedComponent } from './admin-driving-detailed.component';

describe('AdminDrivingDetailedComponent', () => {
  let component: AdminDrivingDetailedComponent;
  let fixture: ComponentFixture<AdminDrivingDetailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDrivingDetailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminDrivingDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
