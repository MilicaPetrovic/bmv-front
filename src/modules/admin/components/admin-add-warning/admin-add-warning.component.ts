import { Component } from '@angular/core'
import { MatDialogRef } from '@angular/material/dialog'

@Component({
  selector: 'app-admin-add-warning',
  templateUrl: './admin-add-warning.component.html',
  styleUrls: ['./admin-add-warning.component.css']
})
export class AdminAddWarningComponent {
  warning = ''

  constructor (
    public dialogRef: MatDialogRef<AdminAddWarningComponent>
  ) {}
}
