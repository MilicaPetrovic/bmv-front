import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminAddWarningComponent } from './admin-add-warning.component'

describe('AdminAddWarningComponent', () => {
  let component: AdminAddWarningComponent
  let fixture: ComponentFixture<AdminAddWarningComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminAddWarningComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(AdminAddWarningComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
