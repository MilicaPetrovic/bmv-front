import { Component, Inject, OnInit } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { AdminClientsService } from '../../services/admin-client/admin-clients.service'
import { WarningNote } from '../../models/warning-note'
import { LazyLoadEvent } from 'primeng/api'
import { AdminClientDriving } from '../../models/admin-client-driving'
import {AdminDrivingDetailedComponent} from "../admin-driving-detailed/admin-driving-detailed.component";

@Component({
  selector: 'app-admin-client-client-detailed',
  templateUrl: './admin-detailed-user.component.html',
  styleUrls: ['./admin-detailed-user.component.css']
})
export class AdminDetailedUserComponent implements OnInit {
  warnings: WarningNote[]
  drivings: AdminClientDriving[]
  drivingLoading = false
  drivingTotalRecords = 1
  drivingRow = 5

  constructor (
    private readonly adminClientService: AdminClientsService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AdminDetailedUserComponent>,
    @Inject(MAT_DIALOG_DATA) public client: AdminUserBasic
  ) { }

  ngOnInit (): void {
    this.drivingLoading = true
    this.adminClientService.getDrivingsByUser(this.client.username, 0, this.drivingRow).subscribe(data => {
      this.drivingTotalRecords = data.totalCount
      this.drivings = data.resultList
      this.drivingLoading = false
    })
  }

  nextDrivingPage (event: LazyLoadEvent): void {
    setTimeout(() => {
      this.drivingLoading = true
      if (event.first === undefined) { event.first = 0 }
      if (event.rows === undefined) { event.rows = 5 }
      let until = event.first + event.rows
      if (until > this.drivingTotalRecords) until = this.drivingTotalRecords
      this.adminClientService.getDrivingsByUser(this.client.username, event.first / event.rows, event.rows).subscribe(data => {
        this.drivingTotalRecords = data.totalCount
        this.drivings = data.resultList
        this.drivingLoading = false
      })
    }, 500)
  }

  openDialogForDriving (id: number): void {
    console.log(id)
    const dialogRef = this.dialog.open(AdminDrivingDetailedComponent, {
      width: '70%',
      height: '95%',
      data: id
    })

    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed')
    })
  }
}
