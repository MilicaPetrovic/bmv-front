import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminDetailedUserComponent } from './admin-detailed-user.component'

describe('AdminDetailedUserComponent', () => {
  let component: AdminDetailedUserComponent
  let fixture: ComponentFixture<AdminDetailedUserComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminDetailedUserComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(AdminDetailedUserComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
