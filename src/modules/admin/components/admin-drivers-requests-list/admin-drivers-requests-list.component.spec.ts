import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDriversRequestsListComponent } from './admin-drivers-requests-list.component';

describe('AdminDriversRequestsListComponent', () => {
  let component: AdminDriversRequestsListComponent;
  let fixture: ComponentFixture<AdminDriversRequestsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDriversRequestsListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminDriversRequestsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
