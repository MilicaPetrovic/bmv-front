import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LazyLoadEvent } from 'primeng/api';
import { ProfileChangeRequest } from 'src/modules/admin/models/profile-change-request';
import { AdminDriversService } from '../../services/admin-driver/admin-drivers.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-admin-drivers-requests-list',
  templateUrl: './admin-drivers-requests-list.component.html',
  styleUrls: ['./admin-drivers-requests-list.component.css'],
  providers: [MessageService]
})
export class AdminDriversRequestsListComponent implements OnInit {

  requests: ProfileChangeRequest[]
  loading = false
  totalRecords = 0
  rowCount = 1

  constructor(public dialog: MatDialog,
              private readonly adminDriversService: AdminDriversService,
              private readonly messageService: MessageService) { }

  ngOnInit(): void {
    this.loading = true
    this.getRequests(0, this.rowCount)
  }

  nextPage (event: LazyLoadEvent) :void{
    // this.loading = true;
    // event.first = 0
    // event.rows = 3
    // event.sortField ='' ;
    // event.sortOrder = -1;
    // filters:{}

    setTimeout(() => {
      this.loading = true
      if (event.first === undefined) { event.first = 0 }
      if (event.rows === undefined) { event.rows = 5 }
      this.getRequests(event.first / event.rows, event.rows)
    }, 500)
  }


  private getRequests (number: number, rowCount: number) {
    this.adminDriversService.getProfileChangeRequests(number, rowCount).subscribe(data => {
      this.totalRecords = data.totalCount
      this.requests = data.resultList
      this.loading = false
    })
  }

  accept(event: MouseEvent, request: ProfileChangeRequest): void {
    event.stopPropagation();
    request.approved = true;
    request.done = true;
    this.updateDriver(request);
  }

  decline(event: MouseEvent, request: ProfileChangeRequest): void {
    event.stopPropagation()
    request.approved = false;
    request.done = true;
    this.updateDriver(request);
  }

  private updateDriver(request: ProfileChangeRequest) {
    this.adminDriversService.updateDriver(request).subscribe(
      data => {
        if (data.successful) {
          console.log('USPJESNO')
          this.requests = this.requests.filter((x) => !x.done)
          this.messageService.add({ key: 'update-profile-message', severity: 'success', summary: 'Uspešna promena podataka', detail: data.exceptionMessage });
        }
        else {
          console.log('NEUSPJESNO')
          this.messageService.add({ key: 'update-profile-message', severity: 'error', summary: 'Neuspešna promena podataka', detail: data.exceptionMessage, life: 5000 });
        }
      }
    )
  }


}
