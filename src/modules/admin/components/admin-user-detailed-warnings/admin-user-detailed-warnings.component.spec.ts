import { ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminUserDetailedWarningsComponent } from './admin-user-detailed-warnings.component'

describe('AdminUserDetailedWarningsComponent', () => {
  let component: AdminUserDetailedWarningsComponent
  let fixture: ComponentFixture<AdminUserDetailedWarningsComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdminUserDetailedWarningsComponent]
    })
      .compileComponents()

    fixture = TestBed.createComponent(AdminUserDetailedWarningsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
