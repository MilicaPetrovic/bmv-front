import { Component, Input, OnInit } from '@angular/core'
import { AdminAddWarningComponent } from '../admin-add-warning/admin-add-warning.component'
import { WarningNote } from '../../models/warning-note'
import { AdminUserBasic } from '../../models/admin-user-basic'
import { AdminUsersService } from '../../services/admin-users/admin-users.service'
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-admin-user-detailed-warnings',
  templateUrl: './admin-user-detailed-warnings.component.html',
  styleUrls: ['./admin-user-detailed-warnings.component.css']
})
export class AdminUserDetailedWarningsComponent implements OnInit {
  warnings: WarningNote[]
  @Input()
    user: AdminUserBasic

  constructor (private readonly adminUserService: AdminUsersService,
    public dialog: MatDialog) { }

  ngOnInit (): void {
    this.adminUserService.getWarningsByUser(this.user.username).subscribe(data => {
      this.warnings = data
    })
  }

  addWarning () : void {
    const dialogRef = this.dialog.open(AdminAddWarningComponent, {
      width: '40%',
      height: '60%'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      if (result !== '') {
        this.adminUserService.addWarning(this.user.username, result).subscribe(data => {
          this.warnings = data
          console.log(this.warnings)
        })
      }
    })
  }
}
