import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AdminAddWarningComponent } from './components/admin-add-warning/admin-add-warning.component'
import { AdminDetailedUserComponent } from './components/admin-client-detailed/admin-detailed-user.component'
import { AdminUsersListComponent } from './components/admin-users-list/admin-users-list.component'
import { AdminUsersComponent } from './pages/admin-users/admin-users.component'
import { AdminClientsService } from './services/admin-client/admin-clients.service'
import { AdminUsersService } from './services/admin-users/admin-users.service'
import { TabViewModule } from 'primeng/tabview'
import { TableModule } from 'primeng/table'
import { MatDialogModule } from '@angular/material/dialog'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { AdminRoutes } from './admin.routes'
import { MatButtonModule } from '@angular/material/button'
import { ButtonModule } from 'primeng/button'
import { SharedModule } from '../shared/shared.module'
import { AdminDriverDetailedComponent } from './components/admin-driver-detailed/admin-driver-detailed.component'
import { AdminUserDetailedBasicComponent } from './components/admin-user-detailed-basic/admin-user-detailed-basic.component'
import { AdminUserDetailedWarningsComponent } from './components/admin-user-detailed-warnings/admin-user-detailed-warnings.component'
import { VehicleComponent } from './components/vehicle/vehicle.component'
import {ProfileModule} from "../profile/profile.module";
import { AdminDrivingDetailedComponent } from './components/admin-driving-detailed/admin-driving-detailed.component'
import { BadgeModule } from 'primeng/badge'
import { AdminDriversComponent } from './pages/admin-drivers/admin-drivers.component';
import { AdminDriversRequestsListComponent } from './components/admin-drivers-requests-list/admin-drivers-requests-list.component';
import { AddNewDriverComponent } from './pages/add-new-driver/add-new-driver.component';
import { MatGridListModule } from '@angular/material/grid-list'
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { ToastModule } from 'primeng/toast'
import { MatCardModule } from '@angular/material/card'
import { MatIconModule } from '@angular/material/icon'
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AdminAddWarningComponent,
    AdminDetailedUserComponent,
    AdminUsersComponent,
    AdminDriverDetailedComponent,
    AdminUserDetailedBasicComponent,
    AdminUserDetailedWarningsComponent,
    AdminUsersListComponent,
    AdminDrivingDetailedComponent,
    VehicleComponent,
    AdminDriversComponent,
    AdminDriversRequestsListComponent,
    AddNewDriverComponent
  ],
  imports: [
    CommonModule,
    TabViewModule,
    TableModule,
    MatDialogModule,
    FormsModule,
    RouterModule.forChild(AdminRoutes),
    MatButtonModule,
    ButtonModule,
    SharedModule,
    BadgeModule,
    MatGridListModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    ToastModule,
    MatIconModule,
    ProfileModule,
    MatDividerModule,
    MatCheckboxModule,
    MatSelectModule
    ],
  providers: [
    AdminClientsService,
    AdminUsersService
  ]
})
export class AdminModule { }
