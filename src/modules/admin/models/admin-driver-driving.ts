export interface AdminDriverDriving {
  id: number
  state: string
  orderTime: number[]
  departure: string
  arrival: string
  price: number
  rejectionText: string
  reported: boolean
}
