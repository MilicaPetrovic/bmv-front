export interface DetailedDrivingDto {
  price: number
  state: string
  rejected: boolean
  rejectionText: string
  orderTime: number[]
  finished: boolean
  startTime: number[]
  endTime: number[]
  petFriendly: boolean
  babyFriendly: boolean
  vehicleType: string
  driver: DriverDto
  passengers: PassengerDto[]
}

export interface DriverDto {
  username: string
  fullName: string
  driverRating: number
  vehicleRating: number
  rated: boolean
}

export interface PassengerDto {
  username: string
  fullName: string
  driverRating: number
  vehicleRating: number
  text: string
  price: number
  reported: boolean
  paid: boolean
  rated: boolean
}
