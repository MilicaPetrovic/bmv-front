export interface VehicleDto {
  name: string
  vehicleType: string
  vehiclePrice: number
  color: string
  yearOfProduction: number
  label: string
  babyFriendly: boolean
  petFriendly: boolean
  numberOfSeats: number
}
