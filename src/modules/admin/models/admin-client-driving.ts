export interface AdminClientDriving {
  id: number
  price: number
  orderTime: number[]
  departure: string
  arrival: string
  driverName: string
  drivingState: string
  reported: boolean
}
