export interface AdminUserBasic {
  name: string
  surname: string
  email: string
  username: string
  phone: string
  blocked: boolean
}
