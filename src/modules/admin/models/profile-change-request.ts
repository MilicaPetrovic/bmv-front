import { City } from "../../shared/models/city"

export interface ProfileChangeRequest {
    id: number
    username: string
    name: string
    surname: string
    phone: string
    city: City
    approved: boolean
    changes: string[]
    done: boolean
  }
