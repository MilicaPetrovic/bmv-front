import { Routes } from '@angular/router'
import { LoginComponent } from './pages/login/login.component'
import { LoginGuard } from './guards/login/login.guard'
import { RegisterComponent } from './pages/register/register.component'

export const AuthRoutes: Routes = [
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginComponent,
    canActivate: [LoginGuard],
    title: 'Ulogovanje',
    data: {title: 'Ulogovanje'}
  },
  {
    path: 'register',
    pathMatch: 'full',
    component: RegisterComponent,
    canActivate: [LoginGuard],
    data: {title: 'Registracija'}
  }

]
