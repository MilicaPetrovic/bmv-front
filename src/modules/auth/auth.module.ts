import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { LoginComponent } from './pages/login/login.component'
import { RegisterComponent } from './pages/register/register.component'
import { RouterModule } from '@angular/router'
import { AuthRoutes } from './auth.routes'
import { AuthService } from './services/auth/auth.service'
import { RegisterService } from './services/register/register.service'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { ToastModule } from 'primeng/toast'
import { MatCardModule } from '@angular/material/card'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { GoogleSigninButtonDirective } from '../root/directives/google-signin-button.directive'


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    GoogleSigninButtonDirective
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AuthRoutes),
    MatGridListModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    ToastModule,
    MatIconModule
  ],
  providers: [
    AuthService,
    RegisterService
    
  ]
})
export class AuthModule { }
