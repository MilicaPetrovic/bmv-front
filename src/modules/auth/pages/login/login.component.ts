import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth/auth.service'
import { SocialUser, SocialAuthService, FacebookLoginProvider } from '@abacritt/angularx-social-login'
import { HttpErrorResponse } from '@angular/common/http'
import {JwtHelperService} from "@auth0/angular-jwt";
import { UserTokenState } from '../../model/user-token-state'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup

  error: string | null

  user: SocialUser
  loggedIn: boolean

  constructor (
    private readonly fb: FormBuilder,
    private readonly userService: AuthService,
    private readonly authService: SocialAuthService
  ) { }

  ngOnInit (): void {
    this.userService.checkIfSomeoneIsLoggedIn()

    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required
      ]],
      password: ['', [
        Validators.required
      ]]
    })

    this.loginForm.valueChanges.subscribe()

    this.authService.authState.subscribe((user: SocialUser) => {
      this.user = user
      console.log(user)
      this.loggedIn = (user != null)
      if (user.provider === 'GOOGLE') {
        this.userService.socialLogin(this.user).subscribe({
          next: (res: UserTokenState) => {
            this.userService.setUser(res.accessToken)
            this.userService.setExpiration(res.expiresIn)
            window.location.href = 'https://localhost:4200/registered/step'
          }
        })
      }
    })
  }

  login (): void {
    if (!this.loginForm.valid) {
      this.error = 'Neispravan email ili lozinka.'
      return
    }
    console.log('LOGINNN')
    this.error = ''
    this.userService.login(this.loginForm.value).subscribe({
      next: (res: UserTokenState) => {
        this.userService.setUser(res.accessToken)
        this.userService.setExpiration(res.expiresIn)
        const role = this.checkRole()
        if(role ==='driver'){
          window.location.href = 'https://localhost:4200/driver/simulation'
        }
        else if(role === 'admin'){
          window.location.href = 'https://localhost:4200/admin/users'
        }
        else if(role === 'registeredUser'){
          window.location.href = 'https://localhost:4200/registered/step'
        }
      },
      error: (err: HttpErrorResponse) => {
        if (err.status === 401) { this.error = 'Neispravan email ili lozinka.' } else if (err.status === 403) { this.error = 'Vaš nalog još nije aktiviran!' }
      }
    })
  }

  loginWithFB (): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then(() => {
        this.userService.socialLogin(this.user).subscribe({
          next: (res: UserTokenState) => {
            this.userService.setUser(res.accessToken)
            this.userService.setExpiration(res.expiresIn)
            window.location.href = 'https://localhost:4200/registered/step'
          }
        })
      })
  }

  // loginWithGoogle(): void {
  //   console.log("UDJE")
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
  //     .then(res => {
  //       this.userService.socialLogin(this.user).subscribe({
  //         next: (res: any) => {
  //           this.userService.setUser(res.accessToken);
  //           this.userService.setExpiration(res.expiresIn);
  //           this.router.navigate(['home'])
  //         }
  //       })
  //     })
  // }


  checkRole (): string  {
    const item = localStorage.getItem('token')
    if (item) {
      const jwt: JwtHelperService = new JwtHelperService()
      const info = jwt.decodeToken(item)

      for (const key in info.role[0]) {
        if(key === "name") {
          continue
        }

        if (info.role[0][key]) {
          console.log('IMAM ROLU: ' + key)
          return key
        }
      }
    } else {
      return ''
    }
    return''
  }
}
