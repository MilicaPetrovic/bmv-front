import { SocialAuthService } from '@abacritt/angularx-social-login'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing'
import { ReactiveFormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { of } from 'rxjs'
import { AuthService } from '../../services/auth/auth.service'
import { RouterTestingModule } from '@angular/router/testing'

import { LoginComponent } from './login.component'
import { HttpHeaders } from '@angular/common/http'
import { By } from '@angular/platform-browser'

describe('LoginComponent', () => {
  let component: LoginComponent
  let fixture: ComponentFixture<LoginComponent>
  let authService: any;
  let socialAuthService: any;

  beforeEach(async () => {
    const authenticationServiceMock = {
      login: jasmine.createSpy('login').and.returnValue(of({
        token: 'eyJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoiUk9MRV9SRUdJU1RFUkVEX1VTRVIiLCJpc3MiOiJzcHJpbmctc2VjdXJpdHktZXhhbXBsZSIsInN1YiI6ImF0QGdtYWlsLmNvbSIsImF1ZCI6IndlYiIsImlhdCI6MTYxMTM1Njk5NiwiZXhwIjoxNjExMzU4Nzk2fQ.zfS9kgvCNirTMIXdQRW3cKkdrDyN6sPZNGWB8kO7Z0GxFMV2BDK-uPDqyPyvTX2tskBa3ug3-nCWoHk-LHbzCg',

        expiration: 500000
      })),

      loginWithFB: jasmine.createSpy('loginWithFB').and.returnValue(of(true)),

      getAuthHeader: jasmine.createSpy('getAuthHeader').and.returnValue(of(
        new HttpHeaders({ Authorization: 'Bearer ' +  'eyJhbGciOiJIUzUxMiJ9.eyJyb2xlIjoiUk9MRV9SRUdJU1RFUkVEX1VTRVIiLCJpc3MiOiJzcHJpbmctc2VjdXJpdHktZXhhbXBsZSIsInN1YiI6ImF0QGdtYWlsLmNvbSIsImF1ZCI6IndlYiIsImlhdCI6MTYxMTM1Njk5NiwiZXhwIjoxNjExMzU4Nzk2fQ.zfS9kgvCNirTMIXdQRW3cKkdrDyN6sPZNGWB8kO7Z0GxFMV2BDK-uPDqyPyvTX2tskBa3ug3-nCWoHk-LHbzCg'})
      )),

      checkIfSomeoneIsLoggedIn: jasmine.createSpy('checkIfSomeoneIsLoggedIn').and.returnValue(of(false)),

      setUser: jasmine.createSpy('setUser'),
      setExpiration: jasmine.createSpy('setExpiration')
    };

    const socialAuthServiceMock = {
      signIn: jasmine.createSpy('signIn').and.returnValue(of(true))
      // const spy = spyOnProperty(myObj, 'myGetterName', 'get');

    };

    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [ LoginComponent ],
      providers: [
        { provide: AuthService, useValue: authenticationServiceMock },
        { provide: SocialAuthService, useValue: socialAuthServiceMock },
     ]
    })
      .compileComponents()
  })

  beforeEach(()=>{
    fixture = TestBed.createComponent(LoginComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    authService = TestBed.inject(AuthService);
    socialAuthService = TestBed.inject(SocialAuthService);
  })


  it('should create', () => {
    // socialAuthService.authState.and.returnValue(of(true))
    expect(component).toBeTruthy()
  })

  it('should be initialized', () => {
    // socialAuthService.authState.and.returnValue(of(true))
    component.ngOnInit();
    expect(component.loginForm).toBeDefined();
    expect(component.loginForm.invalid).toBeTruthy();
  });

  it('should set input in reactive form', fakeAsync(() => {
    // socialAuthService.authState.and.returnValue(of(true))
    fixture.detectChanges();  // ngOnInit will be called
    fixture.whenStable().then(() => {
        const email = fixture.debugElement.query(By.css('#email')).nativeElement;
        const password = fixture.debugElement.query(By.css('#pass')).nativeElement;

        expect(email.value).toEqual('');
        expect(password.value).toEqual('');

        email.value = 'admin@gmail.com';
        password.value = 'asdf';

        email.dispatchEvent(new Event('input'));
        password.dispatchEvent(new Event('input'));

        const controlEmail = component.loginForm.controls['email'];
        const controlPassword = component.loginForm.controls['password'];

        expect(controlEmail.value).toEqual('admin@gmail.com');
        expect(controlPassword.value).toEqual('asdf');

      });

  }));

  it('should sign in the user', fakeAsync(() => {
    // socialAuthService.authState.and.returnValue(of(true))
    expect(component.loginForm.valid).toBeFalsy();
    component.loginForm.controls['email'].setValue('admin@gmail.com');
    component.loginForm.controls['password'].setValue('asdf');

    expect(component.loginForm.valid).toBeTruthy();
    const loginButton = fixture.debugElement.nativeElement.querySelector('.button');
    loginButton.click();

    expect(authService.login).toHaveBeenCalledTimes(1);
    flush();
  }));

  it('should be invalid form when email is empty', () => {
    component.loginForm.controls['email'].setValue('');
    component.loginForm.controls['password'].setValue('asdf');

    expect(component.loginForm.invalid).toBeTruthy();
    fixture.detectChanges();
    const loginButton = fixture.debugElement.nativeElement.querySelector('.button');
    loginButton.click();

    expect(component.error).toEqual('Neispravan email ili lozinka.');
  });

  it('should be invalid form when password is empty', () => {
    component.loginForm.controls['email'].setValue('admin@gmail.com');
    component.loginForm.controls['password'].setValue('');

    expect(component.loginForm.invalid).toBeTruthy();
    fixture.detectChanges();
    const loginButton = fixture.debugElement.nativeElement.querySelector('.button');
    loginButton.click();

    expect(component.error).toEqual('Neispravan email ili lozinka.');
  });

  it('should be invalid form when password and username are empty', () => {
    component.loginForm.controls['email'].setValue('');
    component.loginForm.controls['password'].setValue('');

    expect(component.loginForm.invalid).toBeTruthy();
    fixture.detectChanges();
  });

  it('should call facebookSignIn on facebook button click', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    const facebookBtn = fixture.debugElement.nativeElement.querySelector('.fb-btn');
    facebookBtn.click();
    expect(socialAuthService.signIn).toHaveBeenCalled();
  });

})
