import {ComponentFixture, fakeAsync, flush, TestBed} from '@angular/core/testing'

import { RegisterComponent } from './register.component'
import {BrowserModule, By} from "@angular/platform-browser";
import {FormControl, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {ToastModule} from "primeng/toast";
import {RegisterService} from "../../services/register/register.service";
import {of} from "rxjs";
import {RegisterRequest} from "../../model/register-request";

describe('RegisterComponent', () => {
  let component: RegisterComponent
  let fixture: ComponentFixture<RegisterComponent>
  //let de: DebugElement
  //let el: HTMLElement
  let registerService: RegisterService

  const emailControlName = 'email'
  const nameControlName = 'name'
  const surnameControlName = 'surname'
  const phoneControlName = 'phone'
  const usernameControlName = 'username'
  const passwordControlName = 'password'
  const passwordControlControlName = 'password2'
  const cityControlName = 'city'
  const postalNumberControlName = 'postalNumber'

  const emailCorrect = 'rs@rs.com'
  const nameCorrect = 'Konstrukcija'
  const surnameCorrect = 'Testiranje'
  const phoneCorrect = '+381589659'
  const usernameCorrect = 'konstrakta'
  const passwordCorrect = 'aA1*aaaa'
  const cityCorrect = 'Novi Sad'
  const postalNumberCorrect = '21000'

  const registerRequest : RegisterRequest = {
    city: cityCorrect,
    email: emailCorrect,
    name: nameCorrect,
    password: passwordCorrect,
    phoneNumber: phoneCorrect,
    postNumber: postalNumberCorrect,
    surname: surnameCorrect,
    username: usernameCorrect
  }

  const setCorrectValues = ()=>{

    component.registerFormGroup.controls[emailControlName].setValue(emailCorrect);
    component.registerFormGroup.controls[passwordControlName].setValue(passwordCorrect);
    component.registerFormGroup.controls[nameControlName].setValue(nameCorrect);
    component.registerFormGroup.controls[surnameControlName].setValue(surnameCorrect);
    component.registerFormGroup.controls[phoneControlName].setValue(phoneCorrect);
    component.registerFormGroup.controls[usernameControlName].setValue(usernameCorrect);
    component.registerFormGroup.controls[passwordControlControlName].setValue(passwordCorrect);
    component.registerFormGroup.controls[cityControlName].setValue(cityCorrect);
    component.registerFormGroup.controls[postalNumberControlName].setValue(postalNumberCorrect);
  }

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [
        HttpClientModule,
        BrowserAnimationsModule,
        BrowserModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatGridListModule,
        MatCardModule,
        ToastModule
      ],
    })
      .compileComponents()

  })

  beforeEach(()=>{
    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    //de = fixture.debugElement.query(By.css(".register-form"))
    //el = de.nativeElement
    registerService = TestBed.inject(RegisterService);
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it(`should have as text 'Uber za tebe'`, () => {
    const a = fixture.debugElement.query(By.css(".title"));
    console.log(a)
    console.log(a.nativeElement)
    //console.log(a.text);

    expect(a.nativeNode.innerText).toEqual('Uber za tebe');
  });

  it('should set input in reactive form', fakeAsync(() => {
    fixture.detectChanges();  // ngOnInit will be called
    fixture.whenStable().then(() => {
      const email = fixture.debugElement.query(By.css(`#${emailControlName}`)).nativeElement;
      const name = fixture.debugElement.query(By.css(`#${nameControlName}`)).nativeElement;
      const city = fixture.debugElement.query(By.css(`#${cityControlName}`)).nativeElement
      const password = fixture.debugElement.query(By.css(`#${passwordControlName}`)).nativeElement
      const phoneNumber = fixture.debugElement.query(By.css(`#${phoneControlName}`)).nativeElement
      const surname = fixture.debugElement.query(By.css(`#${surnameControlName}`)).nativeElement
      const username = fixture.debugElement.query(By.css(`#${usernameControlName}`)).nativeElement
      const postNumber = fixture.debugElement.query(By.css(`#${postalNumberControlName}`)).nativeElement
      const password2 = fixture.debugElement.query(By.css(`#${passwordControlControlName}`)).nativeElement

      expect(email.value).toEqual('');
      expect(name.value).toEqual('');
      expect(city.value).toEqual('');
      expect(password.value).toEqual('');
      expect(phoneNumber.value).toEqual('');
      expect(surname.value).toEqual('');
      expect(username.value).toEqual('');
      expect(postNumber.value).toEqual('');
      expect(password2.value).toEqual('');

      email.value = emailCorrect;
      name.value = nameCorrect;
      city.value = cityCorrect
      password.value = passwordCorrect
      phoneNumber.value = phoneCorrect
      surname.value = surnameCorrect
      username.value = usernameCorrect
      postNumber.value = postalNumberCorrect
      password2.value = passwordCorrect


      email.dispatchEvent(new Event('input'));
      name.dispatchEvent(new Event('input'));
      city.dispatchEvent(new Event('input'));
      password.dispatchEvent(new Event('input'));
      phoneNumber.dispatchEvent(new Event('input'));
      surname.dispatchEvent(new Event('input'));
      username.dispatchEvent(new Event('input'));
      postNumber.dispatchEvent(new Event('input'));
      password2.dispatchEvent(new Event('input'));

      const controlEmail = component.registerFormGroup.controls[emailControlName];
      const controlName = component.registerFormGroup.controls[nameControlName];
      const controlSurname = component.registerFormGroup.controls[surnameControlName];
      const controlUsername = component.registerFormGroup.controls[usernameControlName];
      const controlPassword = component.registerFormGroup.controls[passwordControlName];
      const controlPhone = component.registerFormGroup.controls[phoneControlName];
      const controlPostNumber = component.registerFormGroup.controls[postalNumberControlName];
      const controlPassword2 = component.registerFormGroup.controls[passwordControlControlName];
      const controlCity = component.registerFormGroup.controls[cityControlName];

      expect(controlEmail.value).toEqual(emailCorrect);
      expect(controlName.value).toEqual(nameCorrect);
      expect(controlCity.value).toEqual(cityCorrect);
      expect(controlSurname.value).toEqual(surnameCorrect);
      expect(controlUsername.value).toEqual(usernameCorrect);
      expect(controlPassword.value).toEqual(passwordCorrect);
      expect(controlPhone.value).toEqual(phoneCorrect);
      expect(controlPostNumber.value).toEqual(postalNumberCorrect);
      expect(controlPassword2.value).toEqual(passwordCorrect);

    });

  }));

  it('should register the user', fakeAsync(() => {
    expect(component.registerFormGroup.valid).toBeFalsy();
    setCorrectValues()
    fixture.detectChanges();
    expect(component.registerFormGroup.valid).toBeTruthy();
    const registerButton = fixture.debugElement.nativeElement.querySelector('button');

    registerService =fixture.debugElement.injector.get(RegisterService)
    spyOn(registerService, "registerUser")
      .and.returnValue(of({successful: true, exceptionMessage: '', exceptionCode: 0}))

    registerButton.click();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const toast = fixture.debugElement.query(By.css("#register"));
      const toastSummary = fixture.debugElement.query(By.css(".p-toast-summary"))
      expect(toast.nativeNode.hidden).toBeFalsy()
      expect(toastSummary.nativeElement.innerHTML).toEqual("Uspešna registracija")
    });
    flush();
  }));

  it('should be already registered user with same data', fakeAsync(() => {
    expect(component.registerFormGroup.valid).toBeFalsy();
    setCorrectValues()
    fixture.detectChanges();
    expect(component.registerFormGroup.valid).toBeTruthy();
    component.registerRequest = registerRequest

    const registerButton = fixture.debugElement.nativeElement.querySelector('button');

    registerButton.click();

    //expect(registerService.registerUser).toHaveBeenCalledTimes(0)

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const toast = fixture.debugElement.query(By.css("#register"));
      const toastSummary = fixture.debugElement.query(By.css(".p-toast-summary"))
      expect(toast.nativeNode.hidden).toBeFalsy()
      expect(toastSummary.nativeElement.innerHTML).toEqual("Poslali ste već zahtev")
    });
    flush();
  }));

  it('should not register the user, credentials in use', fakeAsync(() => {
    expect(component.registerFormGroup.valid).toBeFalsy();
    setCorrectValues()
    fixture.detectChanges();
    expect(component.registerFormGroup.valid).toBeTruthy();
    const registerButton = fixture.debugElement.nativeElement.querySelector('button');

    registerService =fixture.debugElement.injector.get(RegisterService)
    spyOn(registerService, "registerUser")
      .and.returnValue(of({successful: false, exceptionMessage: '', exceptionCode: 4}))

    registerButton.click();

    //expect(spy).toHaveBeenCalled()

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const toast = fixture.debugElement.query(By.css("#register"));
      const toastSummary = fixture.debugElement.query(By.css(".p-toast-detail"))
      expect(toast.nativeNode.hidden).toBeFalsy()
      expect(toastSummary.nativeElement.innerHTML).toEqual("Nismo uspeli da Vas registrujemo.\nKorisničko ime ili email adresa već zauzet.")
    });
    flush();
  }));

  it('should not register the user, invalid data', fakeAsync(() => {
    expect(component.registerFormGroup.valid).toBeFalsy();
    setCorrectValues()
    fixture.detectChanges();
    expect(component.registerFormGroup.valid).toBeTruthy();
    const registerButton = fixture.debugElement.nativeElement.querySelector('button');

    registerService =fixture.debugElement.injector.get(RegisterService)
    spyOn(registerService, "registerUser")
      .and.returnValue(of({successful: false, exceptionMessage: '', exceptionCode: 7}))

    registerButton.click();

    //expect(spy).toHaveBeenCalled()

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const toast = fixture.debugElement.query(By.css("#register"));
      const toastSummary = fixture.debugElement.query(By.css(".p-toast-detail"))
      expect(toast.nativeNode.hidden).toBeFalsy()
      expect(toastSummary.nativeElement.innerHTML).toEqual('Nismo uspeli da Vas registrujemo.\nMolimo Vas popunite formu validnim podacima.')
    });
    flush();
  }));

  it('should not register the user, unexpected error', fakeAsync(() => {
    expect(component.registerFormGroup.valid).toBeFalsy();
    setCorrectValues()
    fixture.detectChanges();
    expect(component.registerFormGroup.valid).toBeTruthy();
    const registerButton = fixture.debugElement.nativeElement.querySelector('button');

    registerService =fixture.debugElement.injector.get(RegisterService)
    spyOn(registerService, "registerUser")
      .and.returnValue(of({successful: false, exceptionMessage: '', exceptionCode: -1}))

    registerButton.click();

    //expect(spy).toHaveBeenCalled()

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const toast = fixture.debugElement.query(By.css("#register"));
      const toastSummary = fixture.debugElement.query(By.css(".p-toast-detail"))
      expect(toast.nativeNode.hidden).toBeFalsy()
      expect(toastSummary.nativeElement.innerHTML).toEqual('Nismo uspeli da Vas registrujemo.')
    });
    flush();
  }));

  it('should be valid password 2', () => {
    const sameValue = 'ABc'
    const fc = new FormControl('', []);
    fc.setValue(sameValue)
    const returnedValue = component.createPasswordMatchingValidator(fc)

    const fc2 = new FormControl('', []);
    fc2.setValue('no')
    const decision = returnedValue(fc2)
    expect(decision).not.toEqual(null)

    fc2.setValue(sameValue)
    const decision2 = returnedValue(fc2)
    expect(decision2).toEqual(null)
    //expect(decision).toEqual('wrongValue')
  })

  it(`form should be valid`, () => {
    setCorrectValues()
    expect(component.registerFormGroup.valid).toBeTruthy();
  });

  it(`form should be invalid on email`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[emailControlName].setValue('emailIncorrect');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[emailControlName].setValue('email@');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[emailControlName].setValue('email@rs.');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on password`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[passwordControlName].setValue('emailCorrect');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[passwordControlName].setValue('emailCorrect');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[passwordControlName].setValue('emailCorrect');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on name`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[nameControlName].setValue('mojaaa');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[nameControlName].setValue('45378');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on surname`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[surnameControlName].setValue('prezimee');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[surnameControlName].setValue('24245');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on phone`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[phoneControlName].setValue('65468');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[phoneControlName].setValue('telefon');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on username`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[usernameControlName].setValue('sjv jsnf ?');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on password2`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[passwordControlControlName].setValue('emailCorrect');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on city`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[passwordControlName].setValue('city');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it(`form should be invalid on postal number`, () => {
    setCorrectValues()
    component.registerFormGroup.controls[passwordControlName].setValue('knkj');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[passwordControlName].setValue('23');
    expect(component.registerFormGroup.valid).toBeFalsy();
    component.registerFormGroup.controls[passwordControlName].setValue('1265989');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });
})
