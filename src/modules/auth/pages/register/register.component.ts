import {Component} from '@angular/core'
import {
  AbstractControl, FormBuilder,
  FormControl, FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms'
import {ErrorStateMatcher} from '@angular/material/core'
import {RegisterRequest} from '../../model/register-request'
import {RegisterService} from '../../services/register/register.service'
import {MessageService} from 'primeng/api'
import * as _ from 'lodash'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [MessageService]
})

export class RegisterComponent  {
  registerRequest: RegisterRequest;
  emailFormControl = new FormControl('', [Validators.required, Validators.email])
  passwordFormControl = new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')])
  nameFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-ZŠĆČĐŽ][a-zšđčćž]*'), Validators.minLength(3), Validators.maxLength(20)])
  surnameFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-ZŠĆČĐŽ][a-zšđčćž]*'), Validators.minLength(3), Validators.maxLength(20)])
  phoneFormControl = new FormControl('', [Validators.required, Validators.pattern('[\+][0-9]{6,13}'), Validators.minLength(6), Validators.maxLength(13)])
  usernameFormControl = new FormControl('', [Validators.required, Validators.pattern('^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$')])
  password2FormControl: FormControl
  cityFormControl = new FormControl('', [Validators.required, Validators.pattern('[A-ZŠĆČĐŽ][a-zšđčćžA-ZŠĆČĐŽ\\s]*'), Validators.minLength(3), Validators.maxLength(20)])
  cityNumberFormControl = new FormControl('', [Validators.required, Validators.pattern('[0-9]{5}')])

  registerFormGroup: FormGroup

  matcher = new ErrorStateMatcher();

  constructor(
    private readonly registerSerive: RegisterService,
    private readonly messageService: MessageService,
    private readonly fb: FormBuilder) {
    this.registerFormGroup = fb.group({
      email: this.emailFormControl,
      password: this.passwordFormControl,
      name: this.nameFormControl,
      surname: this.surnameFormControl,
      phone: this.phoneFormControl,
      username: this.usernameFormControl,
      password2: this.passwordFormControl,
      city: this.cityFormControl,
      postalNumber: this.cityNumberFormControl
    })
    this.password2FormControl = new FormControl('', [Validators.required, this.createPasswordMatchingValidator(this.passwordFormControl), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')])
  }

  submit(): void {
    if (this.registerFormGroup.valid) {
      const registerRequestLoc: RegisterRequest = {
        city: this.cityFormControl.value,
        email: this.emailFormControl.value,
        name: this.nameFormControl.value,
        password: this.passwordFormControl.value,
        phoneNumber: this.phoneFormControl.value,
        surname: this.surnameFormControl.value,
        username: this.usernameFormControl.value,
        postNumber: this.cityNumberFormControl.value
      }
      if (_.isEqual(this.registerRequest, registerRequestLoc)) {
        this.messageService.add({
          key: 'registration-message',
          severity: 'info',
          summary: 'Poslali ste već zahtev',
          detail: 'Molimo Vas sačekajte malo i verifikujte vaš nalog preko mejla koji ste dobili'
        })
        return;
      }
      this.registerRequest = registerRequestLoc
      this.registerSerive.registerUser(this.registerRequest).subscribe(
        result => {
          console.log('REGISTER')
          console.log(result)
          if (result.successful) {
            console.log("USPESNA REGISTRACIJA")
            this.messageService.add({
              key: 'registration-message',
              severity: 'success',
              summary: 'Uspešna registracija',
              detail: 'Molimo Vas verifikujte vašu email adresu'
            })
          } else {
            if (result.exceptionCode == 4) {
              console.log("Nooooo REGISTRACIJA  -  4")
              this.messageService.add({
                key: 'registration-message',
                severity: 'error',
                summary: 'Neuspešna registracija',
                detail: 'Nismo uspeli da Vas registrujemo.\nKorisničko ime ili email adresa već zauzet.',
                life: 5000
              })
            } else if (result.exceptionCode == 7) {
              console.log("Nooooo REGISTRACIJA  -  7")
              this.messageService.add({
                key: 'registration-message',
                severity: 'error',
                summary: 'Neuspešna registracija',
                detail: 'Nismo uspeli da Vas registrujemo.\nMolimo Vas popunite formu validnim podacima.',
                life: 5000
              })
            }else{
              this.messageService.add({
                key: 'registration-message',
                severity: 'error',
                summary: 'Neuspešna registracija',
                detail: 'Nismo uspeli da Vas registrujemo.',
                life: 5000
              })

              console.log("Nooooo REGISTRACIJA  -  ELSE")
            }
          }
        },

        () =>{
          this.messageService.add({
            key: 'registration-message',
            severity: 'error',
            summary: 'Neuspešna registracija',
            detail: 'Nismo uspeli da Vas registrujemo.',
          })
        }
      )
      console.log('Zovem baaaack')
    }
  }

  createPasswordMatchingValidator(passwordFormControl: FormControl): ValidatorFn {
    {
      return (control: AbstractControl): Record<string, string> | null =>
        control.value === passwordFormControl.value
          ? null
          : {wrongValue: control.value}
    }
  }
}

/*
class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = (form != null) && form.submitted
    return !!((control != null) && control.invalid && (control.dirty || control.touched || isSubmitted))
  }
}
*/
