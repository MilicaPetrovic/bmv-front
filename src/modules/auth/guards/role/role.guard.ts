import { Injectable } from '@angular/core'
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router'
import { JwtHelperService } from '@auth0/angular-jwt'
import { AuthService } from '../../services/auth/auth.service'

@Injectable({
  providedIn: 'root'
})
// admin
// registeredUser
// driver
export class RoleGuard implements CanActivate {
  constructor (public auth: AuthService, public router: Router) {}

  canActivate (route: ActivatedRouteSnapshot): boolean {
    const expectedRoles: string = route.data['expectedRoles']
    const token = this.auth.getUser()
    const jwt: JwtHelperService = new JwtHelperService()

    if (!token) {
      return false
    }
    const info = jwt.decodeToken(token)


    const roles: string[] = expectedRoles.split('|', 3)
    for (const r of roles) {
      if (info.role[0][r])
      { return true }
    }
    this.router.navigate(['/home'])
    return false
  }
}
