import { HttpClient } from '@angular/common/http';
import { fakeAsync, getTestBed, TestBed, tick } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthService } from './auth.service'
import { SocialUser } from '@abacritt/angularx-social-login';

describe('AuthService', () => {
  let authService: AuthService
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers:    [AuthService, HttpClient]
    })
    authService = TestBed.inject(AuthService)
    
    httpMock = TestBed.inject(HttpTestingController);
  })

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(authService).toBeTruthy()
  })

  it('login should get a jwt token', fakeAsync(() => {
    const token = 'tokenvalue';
    let receivedToken = '';
    const auth: any = { username: 'email@gmail.com', password: 'asdf'};

    authService.login(auth).subscribe({
      next(res: any) {
        receivedToken = res
      }
    });
    const req = httpMock.expectOne('https://localhost:8080/uber/auth/login');
    expect(req.request.method).toBe('POST');
    req.flush(token);
    tick();

    expect(receivedToken).toBeDefined();
    expect(receivedToken).toEqual('tokenvalue');

  }));

  it('login should throw error 401', fakeAsync(() => {
    const auth: any = { username: 'email@gmail.com', password: 'asdfghsdj'};
    let error: any;
    authService.login(auth).subscribe({error: (e) => error = e});
    const req = httpMock.expectOne('https://localhost:8080/uber/auth/login');
    expect(req.request.method).toBe('POST');
    req.flush('Unauthorized', {
      status: 401,
      statusText: 'Unauthorized'
    });
    tick();

    expect(error).toBeDefined();
    expect(error.status).toEqual(401);
    expect(error.statusText).toEqual('Unauthorized');
  }));

  it('login should throw error 403', fakeAsync(() => {
    const auth: any = { username: 'email@gmail.com', password: 'asdfghsdj'};
    let error: any;
    authService.login(auth).subscribe({error: (e) => error = e});
    const req = httpMock.expectOne('https://localhost:8080/uber/auth/login');
    expect(req.request.method).toBe('POST');
    req.flush('Forbidden', {
      status: 403,
      statusText: 'Forbidden'
    });
    tick();

    expect(error).toBeDefined();
    expect(error.status).toEqual(403);
    expect(error.statusText).toEqual('Forbidden');
  }));

  it('social login should get a jwt token', fakeAsync(() => {
    const socialUser: SocialUser = {
      provider: 'FACEBOOK',
      id: '1',
      email: 'user@use.com',
      name: 'user user',
      photoUrl: 'asdasda',
      firstName: 'user',
      lastName: 'user',
      authToken: '',
      idToken: 'idtoken',
      authorizationCode: 'asd',
      response: null
    }
    const token = 'tokenvalue';
    expect(socialUser.authToken).toEqual('');

    authService.socialLogin(socialUser).subscribe({
      next(res: any) {
        socialUser.authToken = res
      }
    });
    const req = httpMock.expectOne('https://localhost:8080/uber/auth/socialLogin');
    expect(req.request.method).toBe('POST');
    req.flush(token);
    tick();

    expect(socialUser.authToken).toBeDefined();
    expect(socialUser.authToken).toEqual('tokenvalue');

  }));
})
