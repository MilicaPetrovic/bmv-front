import { SocialUser } from '@abacritt/angularx-social-login'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { JwtHelperService } from '@auth0/angular-jwt'
import {Observable} from "rxjs";
import {StatusResponseDto} from "../../../shared/models/status-response-dto";
import {environment} from "../../../../environments/environment";
import { UserTokenState } from '../../model/user-token-state'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly API_PATH = environment.api_path + 'auth'

  constructor (
    private readonly httpClient: HttpClient,
    private readonly router: Router
  ) { }

  public setUser (token: string): void {
    localStorage.setItem('token', token)
  }

  public getUser (): string | null {
    return localStorage.getItem('token')
  }

  public setExpiration (expiration: string): void {
    localStorage.setItem('expiration', expiration)
  }

  public getExpiration (): string | null {
    return localStorage.getItem('expiration')
  }

  public logout (): void {
    this.httpClient.get(this.API_PATH + '/logout').subscribe(
      () =>{
        this.removeCredentials()
      },
      () => {
        this.removeCredentials()
      }
    )
  }

  public driverLogout (): Observable<StatusResponseDto> {
    return this.httpClient.get<StatusResponseDto>(this.API_PATH + '/logout')
  }

  public login (loginData: JSON): Observable<UserTokenState>  {
    return this.httpClient.post<UserTokenState>(this.API_PATH + '/login', loginData)
  }

  checkIfSomeoneIsLoggedIn (): boolean {
    if (this.getUser() !== null) {
      return true
      // this.router.navigate(["home"]);
    }
    return false
  }

  public socialLogin (socialUser: SocialUser): Observable<UserTokenState> {
    return this.httpClient.post<UserTokenState>(this.API_PATH + '/socialLogin', socialUser)
  }

  getAuthHeader (): HttpHeaders {
    console.log(localStorage.getItem('token'))
    if (localStorage.getItem('token')) {
      return new HttpHeaders({ Authorization: 'Bearer ' + localStorage.getItem('token') })
    } else {
      return new HttpHeaders()
    }
  }

  public getUsername (): string {
    const jwt: JwtHelperService = new JwtHelperService()
    const user = this.getUser()
    if (user == null) { return '' }
    const info = jwt.decodeToken(user)
    return info.username
  }

  getRole(): string {
    const jwt: JwtHelperService = new JwtHelperService()
    const token = this.getUser()
    if (!token) {
      return ''
    }
    const info = jwt.decodeToken(token)
    if (info.role[0].registeredUser)// ['registeredUser'])
      return 'RU';
    else if (info.role[0].driver)//['driver'])
      return 'D';
    else if (info.role[0].admin)//['admin'])
      return 'A';
    else
      return '';
  }

  removeCredentials() : void {
    localStorage.removeItem('token')
    localStorage.removeItem('expiration')
    window.location.href = 'https://localhost:4200/auth/login'
  }
}
