import { TestBed } from '@angular/core/testing'

import { RegisterService } from './register.service'
import {environment} from "../../../../environments/environment";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {RegisterRequest} from "../../model/register-request";
import {statusDTO} from "../../../shared/models/status-dto";

describe('RegisterService', () => {
  const API_PATH = environment.api_path + 'auth'
  let httpController:HttpTestingController
  let service: RegisterService

  const status: statusDTO = {
    exceptionCode: 0, exceptionMessage: "", successful: true
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegisterService]
    })
    service = TestBed.inject(RegisterService)
    httpController =TestBed.inject(HttpTestingController)
  })

  afterEach(() => {
    httpController.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should call register and return status', () => {


    const registerRequest: RegisterRequest = {
      city: 'Novi Sad',
      email: 'erdelji.sw32.2019@uns.ac.rs',
      name: 'Viol',
      password: 'bB1*bbbbb',
      phoneNumber: '+84584848',
      surname: 'Erde',
      username: 'vihihihi',
      postNumber: '21000',
    }
    // act
    service.registerUser(registerRequest).subscribe((res) => {

      // assert
      expect(res).toEqual(status);
    });

    const req = httpController.expectOne({
      method: 'POST',
      url: `${API_PATH}/register`,
    });

    //const httpMock = httpController.expectOne(`${API_PATH}/register`)
    //expect(httpMock.request.method).toBe('POST');

    // sending mocked data
    req.flush(status);
  });
})
