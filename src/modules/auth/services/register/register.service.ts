import { Injectable } from '@angular/core'
import { environment } from '../../../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { RegisterRequest } from '../../model/register-request'
import { statusDTO } from '../../../shared/models/status-dto'
import {Observable} from "rxjs";
import { DriverRegisterRequest } from 'src/modules/shared/models/driver-register-request'

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private readonly API_PATH = environment.api_path + 'auth'

  constructor (private readonly httpClient: HttpClient) { }

  public registerUser (registerData: RegisterRequest) : Observable<statusDTO> {
    const url = this.API_PATH + '/register'
    return this.httpClient.post<statusDTO>(`${url}`, (registerData))
  }

  public registerDriver (registerData: DriverRegisterRequest): Observable<statusDTO> {
    const url = this.API_PATH + '/register/driver'
    return this.httpClient.post<statusDTO>(`${url}`, (registerData));
  }
}
