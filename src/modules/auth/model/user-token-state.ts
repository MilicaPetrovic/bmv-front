export interface UserTokenState {
  expiresIn: string;
  accessToken: string;
}
