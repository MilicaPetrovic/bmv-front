export interface RegisterRequest {
  username: string | null
  name: string | null
  surname: string | null
  email: string | null
  password: string | null
  phoneNumber: string | null
  city: string | null
  postNumber: string | null
}
