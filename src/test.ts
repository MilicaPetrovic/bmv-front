// This file is required by karma.conf.js and loads recursively all the .spec and framework files

import 'zone.js/testing'
import { getTestBed } from '@angular/core/testing'
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing'

declare const require: {
  context: (path: string, deep?: boolean, filter?: RegExp) => {
    <T>(id: string): T
    keys: () => string[]
  }
}

// First, initialize the Angular testing environment.
getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
)


// inital
//const context = require.context('./', true, /\.spec\.ts$/)
//context.keys().forEach(context)

//for auth
const context = require.context('./modules/auth/', true, /\.spec\.ts$/);
//const context = require.context('./modules/auth/pages/register', true, /\.spec\.ts$/);

// And load the modules.
context.keys().forEach(context);
